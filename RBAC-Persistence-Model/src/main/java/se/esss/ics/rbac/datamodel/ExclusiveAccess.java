/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.EntityDescriptor;
import se.esss.ics.rbac.Util;

/**
 * <code>ExcelusiveAccess</code> is defined for those permissions for which a user currently has exclusive access. For
 * some permissions user can request exclusive access, which grants him exclusive for that permission for a certain
 * period of time. When one user has exclusive access for a permission, all other users are denied access to whatever
 * that permission protects until the exclusive access expires or the user releases the exclusive access (either
 * explicitly or by logging out). The exclusive access overrides all rules that are bound to the permission. This means
 * if we have a permission for action which is always granted if requested from the main control room, access will be
 * denied if someone else has exclusive access to that permission.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "exclusive_access")
@Table(name = "exclusive_access",
    uniqueConstraints = {
        @UniqueConstraint(name = "UK_EXCLUSIVE_ACCESS_permission", columnNames = { "permission" }) },
    indexes = {
        @Index(name = "INDEX_EXCLUSIVE_ACCESS_user_id", columnList = ("user_id")),
        @Index(name = "INDEX_EXCLUSIVE_ACCESS_end_time", columnList = ("end_time")),
        @Index(name = "INDEX_EXCLUSIVE_ACCESS_permission", columnList = ("permission")) })

@NamedQueries({
        @NamedQuery(
                name = "ExclusiveAccess.selectAll",
                query = "SELECT ea FROM se.esss.ics.rbac.datamodel.ExclusiveAccess ea"
                        + " WHERE ea.endTime > NOW() ORDER BY ea.permission"),
        @NamedQuery(
                name = "ExclusiveAccess.selectPermissions",
                query = "SELECT ea.permission FROM se.esss.ics.rbac.datamodel.ExclusiveAccess ea"
                        + " WHERE ea.endTime > NOW()"),
        @NamedQuery(
                name = "ExclusiveAccess.findByUserId",
                query = "SELECT ea FROM se.esss.ics.rbac.datamodel.ExclusiveAccess ea"
                        + " WHERE ea.userId = :userId AND ea.endTime > NOW()"),
        @NamedQuery(name = "ExclusiveAccess.deleteExpired",
                query = "DELETE FROM se.esss.ics.rbac.datamodel.ExclusiveAccess ea WHERE ea.endTime < NOW()") })
public final class ExclusiveAccess implements Serializable, EntityDescriptor {

    private static final long serialVersionUID = -4805790102202650139L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_EXCLUSIVE_ACCESS_permission"), name = "permission",
            nullable = false)
    private Permission permission;
    @Column(name = "user_id", nullable = false)
    private String userId;
    @Column(name = "start_time")
    private Timestamp startTime;
    @Column(name = "end_time")
    private Timestamp endTime;

    /**
     * @return the primary key of this entity
     */
    public int getId() {
        return id;
    }

    /**
     * @return the permission for which this exclusive access is set
     */
    public Permission getPermission() {
        return permission;
    }

    /**
     * @param permission the permission to set for which this exclusive access is set
     */
    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    /**
     * @return the id of the user that has exclusive access
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId of the user that requested exclusive access
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the time when exclusive access begin
     */
    public Timestamp getStartTime() {
        return startTime != null ? new Timestamp(startTime.getTime()) : null;
    }

    /**
     * @param startTime the time when the exclusive access begin
     */
    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime != null ? new Timestamp(startTime.getTime()) : null;
    }

    /**
     * @return the time when exclusive access expires
     */
    public Timestamp getEndTime() {
        return endTime != null ? new Timestamp(endTime.getTime()) : null;
    }

    /**
     * @param endTime the expiry time of the exclusive access
     */
    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime != null ? new Timestamp(endTime.getTime()) : null;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, userId, startTime, endTime);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        ExclusiveAccess other = (ExclusiveAccess) obj;
        return id == other.id && Objects.equals(endTime, other.endTime) && Objects.equals(startTime, other.startTime)
                && Objects.equals(permission, other.permission) && Objects.equals(userId, other.userId);
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        String perm = permission == null ? "null" : permission.toString(level + 1);
        return new StringBuilder(perm.length() + 200)
                .append("EXCLUSIVE ACCESS [").append(tab)
                .append("Permission: ").append(perm).append(tab)
                .append("Start Time: ").append(startTime).append(tab)
                .append("End Time: ").append(endTime).append(tab)
                .append("User: ").append(userId).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }
}
