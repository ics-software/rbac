/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.datamodel;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import se.esss.ics.rbac.NamedEntity;
import se.esss.ics.rbac.Util;

/**
 * <code>Permission</code> defines a single action for which a role can be authorised for. It is uniquely defined by its
 * name and always belongs to a resource.
 * <p>
 * The application will make a request to the authorisation service to check if the role X has permission Y. If the
 * permission Y exists in this table and if it is associated with the role X, the service should respond to the caller
 * that the role has the requested permission, therefore granting the caller access to the specific action.
 * </p>
 * <p>
 * The above is only true, when the permission is not associated with a {@link Rule}. If the permission contains a rule,
 * that rule has to be evaluated prior to authorising a role for the actions. The rule might overrule the role access;
 * for example permission might always be granted when the request comes from a specific IP (e.g. main control room).
 * </p>
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Entity(name = "permission")
@Table(name = "permission", 
    uniqueConstraints = { 
        @UniqueConstraint(name = "UK_PERMISSION_name_resource", columnNames = { "name", "resource" }) }, 
    indexes = {
        @Index(name = "INDEX_PERMISSION_name", columnList = ("name")),
        @Index(name = "INDEX_PERMISSION_exclusive_access_allowed", columnList = ("exclusive_access_allowed")),
        @Index(name = "INDEX_PERMISSION_resource", columnList = ("resource")) })
@NamedQueries({
        @NamedQuery(name = "Permission.selectAll",
                query = "SELECT p FROM se.esss.ics.rbac.datamodel.Permission p ORDER BY p.resource.name, p.name"),
        @NamedQuery(name = "Permission.selectNamesByResource",
                query = "SELECT p.name FROM se.esss.ics.rbac.datamodel.Permission p WHERE p.resource = :resource"),
        @NamedQuery(name = "Permission.findByName",
                query = "SELECT p FROM se.esss.ics.rbac.datamodel.Permission p"
                        + " WHERE p.name = :name AND p.resource = :resource"),
        @NamedQuery(name = "Permission.findByNames",
                query = "SELECT p FROM se.esss.ics.rbac.datamodel.Permission p"
                        + " WHERE p.name IN :names AND p.resource = :resource"),
        @NamedQuery(name = "Permission.findByResource",
                query = "SELECT p FROM se.esss.ics.rbac.datamodel.Permission p"
                        + " WHERE p.resource.id = :resourceId ORDER BY p.name"),
        @NamedQuery(name = "Permission.findByResourceAndUser",
                query = "SELECT DISTINCT p FROM se.esss.ics.rbac.datamodel.Permission p,"
                        + " se.esss.ics.rbac.datamodel.Role r, se.esss.ics.rbac.datamodel.UserRole ur"
                        + " JOIN r.permissions rp"
                        + " WHERE ur.userId = :userId AND p.resource = :resource"
                        + " AND p.exclusiveAccessAllowed = :eaAllowed AND ur.role = r AND rp.id = p.id"),
        @NamedQuery(name = "Permission.findByWildcard",
                query = "SELECT p FROM se.esss.ics.rbac.datamodel.Permission p WHERE p.name LIKE :wildcard"),
        @NamedQuery(name = "Permission.findByRule",
                query = "SELECT p FROM se.esss.ics.rbac.datamodel.Permission p"
                        + " WHERE p.rule = :rule ORDER BY p.name"),
        @NamedQuery(name = "Permission.filterByResource",
                query = "SELECT p FROM se.esss.ics.rbac.datamodel.Permission p"
                        + " WHERE p.name NOT IN :permissionNames ORDER BY p.name"),
        @NamedQuery(name = "Permission.filter",
                query = "SELECT p FROM se.esss.ics.rbac.datamodel.Permission p"
                        + " WHERE p.resource IN :resources") })
public final class Permission implements Serializable, NamedEntity, Comparable<Permission> {

    private static final long serialVersionUID = -204201146680580295L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", length = 1000)
    private String description;
    @Column(name = "exclusive_access_allowed", nullable = false)
    private Boolean exclusiveAccessAllowed = Boolean.FALSE;
    @ManyToOne(optional = false)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_PERMISSION_resource"), name = "resource", nullable = false,
            referencedColumnName = "id")
    private Resource resource;
    @ManyToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_PERMISSION_rule"), name = "rule", referencedColumnName = "id")
    private Rule rule;
    @ManyToMany(mappedBy = "permissions", fetch = FetchType.LAZY)
    private Set<Role> role;
    @OneToOne(mappedBy = "permission", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private ExclusiveAccess exclusiveAccess;

    /**
     * @return the primary key of this entity
     */
    public int getId() {
        return id;
    }

    /*
     * @see se.esss.ics.rbac.NamedEntity#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the name of this permission
     */
    public void setName(String name) {
        if (name != null && name.indexOf(',') > -1) {
            throw new IllegalArgumentException("Permission name contains an invaid character ','.");
        }
        this.name = name != null ? name.trim() : null;
    }

    /**
     * @return the detailed description of this permission
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the detailed description of this permission
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the resource, which this permission belongs to. Permission can only be checked if requested for this
     *         particular resource.
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @param resource the resource, which the permission belongs to
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }

    /**
     * @return the rule governing this permission.
     */
    public Rule getRule() {
        return rule;
    }

    /**
     * @param rule the rule governing this permission.
     */
    public void setRule(Rule rule) {
        this.rule = rule;
    }

    /**
     * @return the roles that have this permission
     */
    public Set<Role> getRole() {
        return role;
    }

    /**
     * @param role the roles associated with the permission
     */
    public void setRole(Set<Role> role) {
        this.role = role;
    }

    /**
     * @param exclusiveAccessAllowed true if it is allowed to request exclusive access for this permission or false
     *            otherwise
     */
    public void setExclusiveAccessAllowed(Boolean exclusiveAccessAllowed) {
        this.exclusiveAccessAllowed = exclusiveAccessAllowed;
    }

    /**
     * @return true if it is allowed to request exclusive access for this permission or false otherwise
     */
    public Boolean isExclusiveAccessAllowed() {
        return exclusiveAccessAllowed;
    }

    /**
     * @return the exclusive access that is currently active for this permission
     */
    public ExclusiveAccess getExclusiveAccess() {
        return exclusiveAccess;
    }

    /**
     * @param exclusiveAccess the exclusive access associated with this permission
     */
    public void setExclusiveAccess(ExclusiveAccess exclusiveAccess) {
        this.exclusiveAccess = exclusiveAccess;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, resource, exclusiveAccessAllowed, rule);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        Permission other = (Permission) obj;
        return id == other.id && Objects.equals(name, other.name) && Objects.equals(resource, other.resource)
                && Objects.equals(description, other.description)
                && Objects.equals(exclusiveAccessAllowed, other.exclusiveAccessAllowed)
                && Objects.equals(rule, other.rule);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.EntityDescriptor#toString(int)
     */
    @Override
    public String toString(int level) {
        String endTab = '\n' + Util.getIndent(level);
        String tab = endTab + Util.INDENT;
        String res = resource == null ? "null" : resource.toString(level + 1);
        String rul = rule == null ? "null" : rule.toString(level + 1);
        int desclength = description == null ? 4 : description.length();
        return new StringBuilder(res.length() + rul.length() + desclength + 100)
                .append("PERMISSION [").append(tab)
                .append("Name: ").append(name).append(tab)
                .append("Resource: ").append(res).append(tab)
                .append("Description: ").append(description).append(tab)
                .append("Rule: ").append(rul).append(endTab)
                .append(']').toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Permission o) {
        int i = resource.getName().compareTo(o.resource.getName());
        if (i != 0) {
            return i;
        }
        i = name.compareTo(o.getName());
        if (i != 0) {
            return i;
        }
        return id - o.id;
    }

    /**
     * Returns a copy of this permission, with all fields set except the ID.
     * 
     * @param resource the resource of the created permission
     * @return a copy of this permission with all fields set except the ID
     */
    public Permission deriveWith(Resource resource) {
        Permission p = new Permission();
        p.setResource(resource);
        p.setDescription(description);
        p.setExclusiveAccessAllowed(isExclusiveAccessAllowed());
        p.setName(name);
        p.setRole(role);
        p.setRule(rule);
        return p;
    }
}
