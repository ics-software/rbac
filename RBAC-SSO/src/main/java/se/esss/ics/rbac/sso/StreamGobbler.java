/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.sso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * <code>StreamGobbler</code> reads the data from the input stream and fires error events for each read line.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class StreamGobbler extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamGobbler.class);

    /**
     * <code>StreamListener</code> receives notifications from the StreamGobbler when an error occurs
     */
    public static interface StreamListener {
        /**
         * Reports an error message. This call indicates that there was an error during the operation. Description
         * should be given in the message.
         * 
         * @param message the message
         */
        void error(String message);
    }

    private InputStream is;
    private String type;
    private StreamListener listener;

    /**
     * Constructs a new Stream Gobbler.
     * 
     * @param is the input stream that will be read
     * @param type the tag printed in front of each line
     * @param listener the listener to forward all the lines to
     */
    public StreamGobbler(InputStream is, String type, StreamListener listener) {
        this.is = is;
        this.type = type;
        this.listener = listener;
    }

    /**
     * Creates readers to handle the text created by the external program.
     */
    @Override
    public void run() {
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while (br.ready() && (line = br.readLine()) != null) {
                listener.error(type + " > " + line);
            }
        } catch (IOException ioe) {
            LOGGER.error("Exception occurs while reading text from input stream.", ioe);
        }
    }
}
