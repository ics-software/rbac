/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.sso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.grizzly.http.server.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Request filter aborts all requests which comes from addresses which are not defined in allowed remote addresses file.
 * Requests are aborted with BAD REQUEST response status.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public class RequestFilter implements ContainerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestFilter.class);
    private static final String CREATE_FILE_ERROR = "Error while creating file which defines accepted remote "
            + "addresses.";
    private static final String FILE_NOT_FOUND_ERROR = "File which defines accepted remote addresses could not be "
            + "found.";
    private static final String FILE_READING_ERROR = "Eror while reading file which defines accepted remote addresses.";

    private static final String LOCALHOST_IP_ADDRESS = "127.0.0.1";
    private static final String LOCALHOST_STRING = "0:0:0:0:0:0:0:1";

    @Inject
    private Provider<Request> request;
    private List<String> validRemoteAddresses;

    /*
     * (non-Javadoc)
     * 
     * @see javax.ws.rs.container.ContainerRequestFilter#filter(javax.ws.rs.container.ContainerRequestContext)
     */
    @Override
    public synchronized void filter(ContainerRequestContext requestContext) throws IOException {
        if (validRemoteAddresses == null) {
            validRemoteAddresses = getValidRemoteAddresses();
        }
        String ip = request.get().getRemoteAddr();
        String host = request.get().getRemoteHost();
        boolean isValid = false;
        if (LOCALHOST_IP_ADDRESS.equals(ip) || LOCALHOST_STRING.equals(ip)) {
            isValid = true;
        } else {
            isValid = validRemoteAddresses.contains(ip) || validRemoteAddresses.contains(host);
        }
        if (!isValid) {
            LOGGER.info("Access denied to " + ip + " (host)");
            requestContext.abortWith(Response.status(Status.BAD_REQUEST).build());
        }
    }

    /**
     * Reads allowed remote addresses from file into list. File path is defined in local services properties file.
     * 
     * @return list of remote addresses which are allowed.
     */
    private static List<String> getValidRemoteAddresses() {
        List<String> validRemoteAddresses = new ArrayList<String>();
        String filePath = SSOProperties.getInstance().getLocalServicesAcceptedIPsFilePath();
        if (filePath == null || filePath.isEmpty()) {
            return new ArrayList<String>();
        }
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                LOGGER.error(CREATE_FILE_ERROR, e);
            }
        } else {
            try (BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))) {
                String line;
                while ((line = br.readLine()) != null) {
                    validRemoteAddresses.add(line);
                }
                br.close();
            } catch (FileNotFoundException e) {
                LOGGER.error(FILE_NOT_FOUND_ERROR, e);
            } catch (IOException e) {
                LOGGER.error(FILE_READING_ERROR, e);
            }
        }
        return validRemoteAddresses;
    }
}
