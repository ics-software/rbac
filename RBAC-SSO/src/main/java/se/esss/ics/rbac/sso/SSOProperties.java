/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.sso;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * <code>SSOProperties</code> provides access to properties that guide the connection to the local authentication
 * services. It provides the URL to the service.
 * <p>
 * Properties are loaded from <code>localservices.properties</code> file, that has to be placed somewhere on the
 * classpath. All values can be overridden by setting the system properties, but that has to be done before this class
 * is loaded. Convenience methods are provided for accessing the properties.
 * </p>
 * <p>
 * Local services properties are implemented as a singleton. Use {@link #getInstance()} to get an instance of the
 * properties.
 * </p>
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public class SSOProperties {

    /** Local services host */
    public static final String KEY_LOCAL_SERVICES_HOST = "localservices.host";
    /** Local services port */
    public static final String KEY_LOCAL_SERVICES_PORT = "localservices.port";
    /** Local services file */
    public static final String KEY_LOCAL_SERVICES_FILE = "localservices.accepted_ips_file";

    private static final String FILE_LOCAL_SERVICES_PROPERTIES = "localservices.properties";

    private static final SSOProperties INSTANCE = new SSOProperties();

    private final Properties properties;

    private SSOProperties() {
        properties = new Properties();
        try (InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(FILE_LOCAL_SERVICES_PROPERTIES)) {
            properties.load(stream);
        } catch (IOException e) {
            properties.setProperty(KEY_LOCAL_SERVICES_HOST, "0.0.0.0");
            properties.setProperty(KEY_LOCAL_SERVICES_PORT, "9421");
            properties.setProperty(KEY_LOCAL_SERVICES_FILE, "rbac.allowed");
        }
        properties.putAll(System.getProperties());
    }

    /**
     * Returns the singleton instance of {@link SSOProperties} properties. Properties are read from the properties file
     * when the instance is first created. If properties could not be read, default values are put in their place.
     * Properties are always overridden by the system properties. The default values are:
     * <ul>
     * <li>{@value #KEY_LOCAL_SERVICES_HOST} -&gt; 0.0.0.0</li>
     * <li>{@value #KEY_LOCAL_SERVICES_PORT} -&gt; 9638</li>
     * <li>{@value #KEY_LOCAL_SERVICES_FILE} -&gt; rbaac.allowed</li>
     * </ul>
     *
     * @return singleton instance of local services properties.
     */
    public static SSOProperties getInstance() {
        return INSTANCE;
    }

    /**
     * Searches for the property with the specified key amongst the local services properties. The method returns null
     * if the property is not found.
     *
     * @param key
     *            the property key.
     *
     * @return the value of the local services property with the specified key value.
     */
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    /**
     * Returns the value of the {@value #KEY_LOCAL_SERVICES_HOST} property.
     *
     * @return the host name of the local authentication services
     */
    public String getLocalServicesHostname() {
        return properties.getProperty(KEY_LOCAL_SERVICES_HOST);
    }

    /**
     * Returns the value of the {@value #KEY_LOCAL_SERVICES_PORT} property as an integer.
     *
     * @return the port on which the local authentication services are accessible
     */
    public int getLocalServicesPort() {
        return Integer.parseInt(properties.getProperty(KEY_LOCAL_SERVICES_PORT));
    }

    /**
     * Returns the value of the {@value #KEY_LOCAL_SERVICES_FILE} property.
     *
     * @return the file in which allowed remote addresses are defined
     */
    public String getLocalServicesAcceptedIPsFilePath() {
        return properties.getProperty(KEY_LOCAL_SERVICES_FILE);
    }

    /**
     * Sets the hostname of the SSO server.
     *
     * @param host the host
     */
    public void setLocalServicesHostname(String host) {
        properties.setProperty(KEY_LOCAL_SERVICES_HOST,host);
    }

    /**
     * Sets the port of the SSO server.
     *
     * @param port the port
     */
    public void setLocalServicesPort(int port) {
        properties.setProperty(KEY_LOCAL_SERVICES_PORT,String.valueOf(port));
    }

    /**
     * Sets the accepted IPs file path.
     *
     * @param path the path to set
     */
    public void setLocalServicesAcceptedIPsFilePath(String path) {
        properties.setProperty(KEY_LOCAL_SERVICES_FILE,path);
    }
}
