/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.demo;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.SecurityFacadeListener;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.localservice.LocalAuthServiceDetails;
import se.esss.ics.rbac.access.swing.SwingSecurityCallback;

/**
 *
 * <code>Demo</code> demonstrates the usage of the {@link SecurityFacade} in a swing application.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class Demo extends JFrame {

    private static final Logger LOGGER = LoggerFactory.getLogger(Demo.class);

    /**
     * Start the program.
     *
     * @param args the program arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Demo().setVisible(true);
            }
        });
    }

    private static final long serialVersionUID = 1L;

    private static final String ADD = "Add";
    private static final String REMOVE = "Remove";
    private static final String RESOURCE_NAME = "RBACDemo";

    private static final String IP = "127.0.0.1";

    private JButton loginButton;
    private JButton addButton;
    private JButton removeButton;
    private JTextArea tokenArea;

    public Demo() {
        super();
        createGUI();
        initFacade();
        setSize(400, 300);
        setLocationRelativeTo(null);
        setTitle("Demo");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initFacade() {
        SecurityFacade.getDefaultInstance().addSecurityFacadeListener(new SecurityFacadeListener() {
            @Override
            public void loggedIn(Token token) {
                loginButton.setText("Logout");
                tokenArea.setText(token.toString());
                SecurityFacade.getDefaultInstance().hasPermissions(
                        (SecurityCallback) null, IP, RESOURCE_NAME, ADD, REMOVE);
            }

            @Override
            public void loggedOut(Token token) {
                loginButton.setText("Login");
                tokenArea.setText(null);
                initButtonAccess(new HashMap<String, Boolean>());
            }
        });
        SecurityFacade.getDefaultInstance().setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public void authorisationCompleted(Token token, Map<String, Boolean> permissions) {
                initButtonAccess(permissions);
            }

            @Override
            public void authorisationFailed(String reason, String[] permissions, Token token, Throwable e) {
                super.authorisationFailed(reason, permissions, token, e);
                initButtonAccess(new HashMap<String, Boolean>());
            }

            @Override
            public LocalAuthServiceDetails getLocalAuthServiceDetails() {
                return new LocalAuthServiceDetails();
            }

        });
        SecurityFacade.getDefaultInstance().initLocalServiceUsage();
        try {
            Token token = SecurityFacade.getDefaultInstance().getToken(IP);
            if (token != null) {
                loginButton.setText("Logout");
                tokenArea.setText(token.toString());
                SecurityFacade.getDefaultInstance().hasPermissions(
                        (SecurityCallback) null, IP, RESOURCE_NAME, ADD, REMOVE);
            } else {
                initButtonAccess(new HashMap<String, Boolean>());
            }
        } catch (Exception e) {
            LOGGER.error("Cannot retrieve token.",e);
        }
    }

    private void createGUI() {
        loginButton = new JButton("Login");
        loginButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (SecurityFacade.getDefaultInstance().getToken(IP) == null) {
                        SecurityFacade.getDefaultInstance().authenticate(null);
                    } else {
                        SecurityFacade.getDefaultInstance().logout(null);
                    }
                } catch (SecurityFacadeException e1) {
                    LOGGER.error("Cannot authenticate user.",e);
                }
            }
        });
        addButton = new JButton("Add");
        addButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                SecurityFacade.getDefaultInstance().hasPermissions(new MySwingSecurityCallback() {
                    @Override
                    public void authorisationCompleted(Token token, Map<String, Boolean> permissions) {
                        if (permissions.get(ADD)) {
                            JOptionPane.showMessageDialog(Demo.this, "Permission 'ADD' granted", "Granted",
                                    JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(Demo.this, "You do not have permission 'ADD'.",
                                    "No permission", JOptionPane.WARNING_MESSAGE);
                        }

                    }
                }, IP, RESOURCE_NAME, ADD);
            }
        });
        removeButton = new JButton(REMOVE);
        removeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                SecurityFacade.getDefaultInstance().hasPermissions(new MySwingSecurityCallback() {
                    @Override
                    public void authorisationCompleted(Token token, Map<String, Boolean> permissions) {
                        if (permissions.get(REMOVE)) {
                            JOptionPane.showMessageDialog(Demo.this, "Permission 'REMOVE' granted", "Granted",
                                    JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(Demo.this, "You do not have permission 'REMOVE'.",
                                    "No permission", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                }, IP, RESOURCE_NAME, REMOVE);
            }
        });

        tokenArea = new JTextArea();
        tokenArea.setEditable(false);
        tokenArea.setLineWrap(false);

        JPanel contentPanel = new JPanel(new GridBagLayout());
        contentPanel.add(loginButton, new GridBagConstraints(0, 0, 2, 1, 1, 0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
        contentPanel.add(new JScrollPane(tokenArea), new GridBagConstraints(0, 1, 2, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 0, 5), 0, 0));
        contentPanel.add(addButton, new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
        contentPanel.add(removeButton, new GridBagConstraints(1, 2, 1, 1, 1, 0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

        setContentPane(contentPanel);
    }

    private void initButtonAccess(Map<String, Boolean> permissions) {
        Boolean b = permissions.get(ADD);
        if (b == null)
            b = false;
        addButton.setEnabled(b);
        b = permissions.get(REMOVE);
        if (b == null)
            b = false;
        removeButton.setEnabled(b);
    }

    private static class MySwingSecurityCallback extends SwingSecurityCallback {
        @Override
        public void authorisationFailed(String reason, String[] permissions, Token token, Throwable e) {
            super.authorisationFailed(reason, permissions, token, e);
            try {
                SecurityFacade.getDefaultInstance().logout(IP);
            } catch (SecurityFacadeException ex) {
                LOGGER.error("Cannot logout.",e);
            }
        }
    }
}
