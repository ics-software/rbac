/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.dsaccess.ldap;

import java.text.Normalizer;
import java.util.Objects;
import java.util.regex.Pattern;

import se.esss.ics.rbac.dsaccess.UserInfo;

/**
 *
 * <code>LDAPUserInfo</code> is a simple implementation of {@link UserInfo} interface, used by
 * {@link LDAPDirectoryServiceAccess}. It requires all fields to be initialised at construction.
 * <p>
 * This implementation returns slightly different results for compareTo and equals. {@link #compareTo(UserInfo)} will
 * return 0 for two different implementations of {@link UserInfo} as long as their values are the same, while the
 * {@link #equals(Object)} method requires that the implementation classes are the same.
 * </p>
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public class LDAPUserInfo implements UserInfo, Comparable<UserInfo> {

    private static final long serialVersionUID = 1769167990502224838L;
    private final String username;
    private final String lastName;
    private final String firstName;
    private final String middleName;
    private final String group;
    private final String eMail;
    private final String phoneNumber;
    private final String location;

    /**
     * Constructs a new {@link LDAPUserInfo} and sets all the field to the provided values.
     *
     * @param username username of the user
     * @param lastName last name of the user
     * @param firstName first name of the user
     * @param middleName middle name of the user.
     * @param group the name of the group this user belongs to
     * @param eMail e-mail address
     * @param phoneNumber phone number
     * @param mobilePhone the mobile number, which replaces the phone number if that one is null
     * @param location physical location
     *
     * @throws IllegalArgumentException if username, first, or last name are null
     */
    LDAPUserInfo(String username, String lastName, String firstName, String middleName, String group, String eMail,
            String phoneNumber, String mobilePhone, String location) {
        if (username == null || lastName == null || firstName == null) {
            throw new IllegalArgumentException("Username, last name, and first name must not be null.");
        }
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.group = group;
        this.eMail = eMail;
        this.phoneNumber = phoneNumber == null ? mobilePhone : phoneNumber;
        this.location = location;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getUsername()
     */
    @Override
    public String getUsername() {
        return username;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getLastName()
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getFirstName()
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getMiddleName()
     */
    @Override
    public String getMiddleName() {
        return middleName;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getGroup()
     */
    @Override
    public String getGroup() {
        return group;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getEMail()
     */
    @Override
    public String getEMail() {
        return eMail;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getPhoneNumber()
     */
    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.dsaccess.UserInfo#getLocation()
     */
    @Override
    public String getLocation() {
        return location;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(UserInfo o) {
        String name = fixString(this.lastName);
        String otherName = fixString(o.getLastName());
        int i = name.compareTo(otherName);
        if (i != 0) {
            return i;
        }
        name = fixString(this.firstName);
        otherName = fixString(o.getFirstName());
        i = name.compareTo(otherName);
        if (i != 0) {
            return i;
        }
        i = this.username.compareTo(o.getUsername());
        if (i != 0) {
            return i;
        }
        if (this.eMail != null && o.getEMail() != null) {
            i = this.eMail.compareTo(o.getEMail());
            if (i != 0) {
                return i;
            }
        }
        if (this.group != null && o.getGroup() != null) {
            i = this.group.compareTo(o.getGroup());
            if (i != 0) {
                return i;
            }
        }
        if (this.phoneNumber != null && o.getPhoneNumber() != null) {
            i = this.phoneNumber.compareTo(o.getPhoneNumber());
            if (i != 0) {
                return i;
            }
        }
        if (this.location != null && o.getLocation() != null) {
            i = this.location.compareTo(o.getLocation());
            if (i != 0) {
                return i;
            }
        }
        return 0;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(200).append("Username: ").append(username).append("\n  Name: ")
                .append(firstName).append(' ');
        if (middleName != null) {
            sb.append(middleName).append(' ');
        }
        sb.append(lastName).append("\n  Group: ").append(group).append("\n  E-mail: ").append(eMail)
                .append("\n  Phone Number: ").append(phoneNumber).append("\n  Location: ").append(location);
        return sb.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return 31 * getClass().hashCode()
                + Objects.hash(eMail, firstName, group, lastName, location, middleName, phoneNumber, username);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        LDAPUserInfo other = (LDAPUserInfo) obj;
        return Objects.equals(eMail, other.eMail) && Objects.equals(firstName, other.firstName)
                && Objects.equals(group, other.group) && Objects.equals(lastName, other.lastName)
                && Objects.equals(location, other.location) && Objects.equals(middleName, other.middleName)
                && Objects.equals(phoneNumber, other.phoneNumber) && Objects.equals(username, other.username);
    }

    private static final Pattern DIACRITICS_PATTERN = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");

    /**
     * Down grade accented characters to closest ASCII representative.
     *
     * @param str the string to fix
     * @return the input string with all accents replaced by ASCII characters
     */
    private static String fixString(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        return DIACRITICS_PATTERN.matcher(nfdNormalizedString).replaceAll("");
    }
}
