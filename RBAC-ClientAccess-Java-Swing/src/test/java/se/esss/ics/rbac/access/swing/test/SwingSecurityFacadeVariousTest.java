/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.swing.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.security.cert.X509Certificate;

import se.esss.ics.rbac.access.swing.SecurityFacade;
import se.esss.ics.rbac.access.swing.SwingSecurityCallback;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.test.SecurityFacadeVariousTest;

/**
 * 
 * <code>SwingSecurityFacadeVariousTest</code> test general stuff using the swing security facade implementation.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SwingSecurityFacadeVariousTest extends SecurityFacadeVariousTest {

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.access.test.AbstractSecurityFacadeTest#createFacade()
     */
    @Override
    protected ISecurityFacade createFacade() {
        ISecurityFacade facade = new SecurityFacade(super.createFacade());
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public boolean acceptCertificate(String host, X509Certificate certificate) {
                return true;
            }
        });
        return facade;
    }

    /**
     * Override, because we are testing with a different implementation of the {@link ISecurityFacade}.
     */
    @Override
    public void testSecurityFacadeConstructor() {
        ISecurityFacade facade = se.esss.ics.rbac.access.SecurityFacade.getDefaultInstance();
        assertNotNull("Facade should not be null", facade);
        assertEquals("The facade class should be the SecurityFacade", SecurityFacade.class, facade.getClass());
        assertSame("Default instance should always return the same instance", facade,
                se.esss.ics.rbac.access.SecurityFacade.getDefaultInstance());

        facade.setDefaultSecurityCallback(null);
        SecurityCallback cb = facade.getDefaultSecurityCallback();
        assertNotNull("Default Security callback is never null", cb);

        ISecurityFacade facade2 = se.esss.ics.rbac.access.SecurityFacade.newInstance();
        assertEquals("Default and new instance should be of the same type", facade.getClass(), facade2.getClass());

    }
}
