SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: access_security_group; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE access_security_group (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255) NOT NULL
);


--
-- Name: access_security_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE access_security_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: access_security_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE access_security_group_id_seq OWNED BY access_security_group.id;


--
-- Name: access_security_input; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE access_security_input (
    id integer NOT NULL,
    input_index integer NOT NULL,
    pv_name character varying(255) NOT NULL,
    as_group integer NOT NULL
);


--
-- Name: access_security_input_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE access_security_input_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: access_security_input_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE access_security_input_id_seq OWNED BY access_security_input.id;


--
-- Name: access_security_rule; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE access_security_rule (
    id integer NOT NULL,
    calc character varying(40),
    description character varying(255),
    level boolean,
    name character varying(255) NOT NULL,
    permission integer NOT NULL,
    as_group integer NOT NULL
);


--
-- Name: access_security_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE access_security_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: access_security_rule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE access_security_rule_id_seq OWNED BY access_security_rule.id;


--
-- Name: access_security_rule_ip_group; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE access_security_rule_ip_group (
    access_security_rule_id integer NOT NULL,
    ipgroups_id integer NOT NULL
);


--
-- Name: access_security_rule_role; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE access_security_rule_role (
    rules_id integer NOT NULL,
    roles_id integer NOT NULL
);


--
-- Name: configuration_parameter; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE configuration_parameter (
    name character varying(32) NOT NULL,
    value character varying(1000)
);


--
-- Name: exclusive_access; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE exclusive_access (
    id integer NOT NULL,
    end_time timestamp without time zone,
    start_time timestamp without time zone,
    user_id character varying(255) NOT NULL,
    permission integer NOT NULL
);


--
-- Name: exclusive_access_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE exclusive_access_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: exclusive_access_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE exclusive_access_id_seq OWNED BY exclusive_access.id;


--
-- Name: expression; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE expression (
    id integer NOT NULL,
    definition character varying(1000) NOT NULL,
    description character varying(1000),
    long_name character varying(255) NOT NULL,
    name character varying(10) NOT NULL
);


--
-- Name: expression_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expression_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: expression_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expression_id_seq OWNED BY expression.id;


--
-- Name: ip_group; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE ip_group (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255) NOT NULL
);


--
-- Name: ip_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ip_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ip_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ip_group_id_seq OWNED BY ip_group.id;


--
-- Name: ip_group_ip; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE ip_group_ip (
    ip_group_id integer NOT NULL,
    ip character varying(255)
);


--
-- Name: management_studio_log; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE management_studio_log (
    id integer NOT NULL,
    content character varying(1000),
    severity character varying(255) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    user_id character varying(255) NOT NULL
);


--
-- Name: management_studio_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE management_studio_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: management_studio_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE management_studio_log_id_seq OWNED BY management_studio_log.id;


--
-- Name: permission; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE permission (
    id integer NOT NULL,
    description character varying(1000),
    exclusive_access_allowed boolean NOT NULL,
    name character varying(255) NOT NULL,
    resource integer NOT NULL,
    rule integer
);


--
-- Name: permission_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE permission_id_seq OWNED BY permission.id;


--
-- Name: rbac_log; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE rbac_log (
    id integer NOT NULL,
    action character varying(255) NOT NULL,
    content character varying(1000),
    ip character varying(255) NOT NULL,
    severity character varying(255) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    user_id character varying(255) NOT NULL
);


--
-- Name: rbac_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE rbac_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rbac_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE rbac_log_id_seq OWNED BY rbac_log.id;


--
-- Name: resource; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE resource (
    id integer NOT NULL,
    description character varying(1000),
    name character varying(255) NOT NULL
);


--
-- Name: resource_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE resource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE resource_id_seq OWNED BY resource.id;


--
-- Name: resource_managers; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE resource_managers (
    resource_id integer NOT NULL,
    managers character varying(255)
);


--
-- Name: role; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE role (
    id integer NOT NULL,
    description character varying(1000),
    name character varying(30) NOT NULL
);


--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- Name: role_managers; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE role_managers (
    role_id integer NOT NULL,
    managers character varying(255)
);


--
-- Name: role_permission; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE role_permission (
    role_id integer NOT NULL,
    permissions_id integer NOT NULL
);


--
-- Name: rule; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE rule (
    id integer NOT NULL,
    definition character varying(1000) NOT NULL,
    description character varying(1000),
    name character varying(255) NOT NULL
);


--
-- Name: rule_expression; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE rule_expression (
    rule_id integer NOT NULL,
    expression_id integer NOT NULL
);


--
-- Name: rule_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE rule_id_seq OWNED BY rule.id;


--
-- Name: token; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE token (
    id integer NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    expiration_date timestamp without time zone NOT NULL,
    ip character varying(255) NOT NULL,
    token_id character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL
);


--
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE token_id_seq OWNED BY token.id;


--
-- Name: token_role; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE token_role (
    id integer NOT NULL,
    role integer NOT NULL,
    token integer NOT NULL
);


--
-- Name: token_role_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE token_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: token_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE token_role_id_seq OWNED BY token_role.id;


--
-- Name: user_role; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE user_role (
    id integer NOT NULL,
    assignment integer NOT NULL,
    delegator character varying(255),
    end_time timestamp without time zone,
    start_time timestamp without time zone,
    user_id character varying(255) NOT NULL,
    role integer NOT NULL
);


--
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_role_id_seq OWNED BY user_role.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_security_group ALTER COLUMN id SET DEFAULT nextval('access_security_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_security_input ALTER COLUMN id SET DEFAULT nextval('access_security_input_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_security_rule ALTER COLUMN id SET DEFAULT nextval('access_security_rule_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY exclusive_access ALTER COLUMN id SET DEFAULT nextval('exclusive_access_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expression ALTER COLUMN id SET DEFAULT nextval('expression_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ip_group ALTER COLUMN id SET DEFAULT nextval('ip_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY management_studio_log ALTER COLUMN id SET DEFAULT nextval('management_studio_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY permission ALTER COLUMN id SET DEFAULT nextval('permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY rbac_log ALTER COLUMN id SET DEFAULT nextval('rbac_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY resource ALTER COLUMN id SET DEFAULT nextval('resource_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule ALTER COLUMN id SET DEFAULT nextval('rule_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY token ALTER COLUMN id SET DEFAULT nextval('token_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY token_role ALTER COLUMN id SET DEFAULT nextval('token_role_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_role ALTER COLUMN id SET DEFAULT nextval('user_role_id_seq'::regclass);


--
-- Name: access_security_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY access_security_group
    ADD CONSTRAINT access_security_group_pkey PRIMARY KEY (id);


--
-- Name: access_security_input_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY access_security_input
    ADD CONSTRAINT access_security_input_pkey PRIMARY KEY (id);


--
-- Name: access_security_rule_ip_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY access_security_rule_ip_group
    ADD CONSTRAINT access_security_rule_ip_group_pkey PRIMARY KEY (access_security_rule_id, ipgroups_id);


--
-- Name: access_security_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY access_security_rule
    ADD CONSTRAINT access_security_rule_pkey PRIMARY KEY (id);


--
-- Name: access_security_rule_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY access_security_rule_role
    ADD CONSTRAINT access_security_rule_role_pkey PRIMARY KEY (rules_id, roles_id);


--
-- Name: configuration_parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY configuration_parameter
    ADD CONSTRAINT configuration_parameter_pkey PRIMARY KEY (name);


--
-- Name: exclusive_access_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY exclusive_access
    ADD CONSTRAINT exclusive_access_pkey PRIMARY KEY (id);


--
-- Name: expression_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY expression
    ADD CONSTRAINT expression_pkey PRIMARY KEY (id);


--
-- Name: ip_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY ip_group
    ADD CONSTRAINT ip_group_pkey PRIMARY KEY (id);


--
-- Name: management_studio_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY management_studio_log
    ADD CONSTRAINT management_studio_log_pkey PRIMARY KEY (id);


--
-- Name: permission_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (id);


--
-- Name: rbac_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY rbac_log
    ADD CONSTRAINT rbac_log_pkey PRIMARY KEY (id);


--
-- Name: resource_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY resource
    ADD CONSTRAINT resource_pkey PRIMARY KEY (id);


--
-- Name: role_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY role_permission
    ADD CONSTRAINT role_permission_pkey PRIMARY KEY (role_id, permissions_id);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: rule_expression_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY rule_expression
    ADD CONSTRAINT rule_expression_pkey PRIMARY KEY (rule_id, expression_id);


--
-- Name: rule_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY rule
    ADD CONSTRAINT rule_pkey PRIMARY KEY (id);


--
-- Name: token_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY token
    ADD CONSTRAINT token_pkey PRIMARY KEY (id);


--
-- Name: token_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY token_role
    ADD CONSTRAINT token_role_pkey PRIMARY KEY (id);


--
-- Name: uk_87xh5v8bk6ynkp487mmw58182; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY ip_group
    ADD CONSTRAINT uk_87xh5v8bk6ynkp487mmw58182 UNIQUE (name);


--
-- Name: uk_access_security_group_name; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY access_security_group
    ADD CONSTRAINT uk_access_security_group_name UNIQUE (name);


--
-- Name: uk_access_security_input_input_index_group; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY access_security_input
    ADD CONSTRAINT uk_access_security_input_input_index_group UNIQUE (input_index, as_group);


--
-- Name: uk_exclusive_access_permission; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY exclusive_access
    ADD CONSTRAINT uk_exclusive_access_permission UNIQUE (permission);


--
-- Name: uk_expression_name; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY expression
    ADD CONSTRAINT uk_expression_name UNIQUE (name);


--
-- Name: uk_ip_group_name; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY ip_group
    ADD CONSTRAINT uk_ip_group_name UNIQUE (name);


--
-- Name: uk_permission_name_resource; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY permission
    ADD CONSTRAINT uk_permission_name_resource UNIQUE (name, resource);


--
-- Name: uk_resource_name; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY resource
    ADD CONSTRAINT uk_resource_name UNIQUE (name);


--
-- Name: uk_role_name; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY role
    ADD CONSTRAINT uk_role_name UNIQUE (name);


--
-- Name: uk_token_role_token_role; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY token_role
    ADD CONSTRAINT uk_token_role_token_role UNIQUE (token, role);


--
-- Name: uk_token_token_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY token
    ADD CONSTRAINT uk_token_token_id UNIQUE (token_id);


--
-- Name: uk_user_role_user_id_role; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT uk_user_role_user_id_role UNIQUE (user_id, role, start_time, end_time);


--
-- Name: user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (id);


--
-- Name: index_access_security_group_name; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_access_security_group_name ON access_security_group USING btree (name);


--
-- Name: index_access_security_input_as_group; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_access_security_input_as_group ON access_security_input USING btree (as_group);


--
-- Name: index_access_security_input_pv_name; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_access_security_input_pv_name ON access_security_input USING btree (pv_name);


--
-- Name: index_access_security_rule_as_group; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_access_security_rule_as_group ON access_security_rule USING btree (as_group);


--
-- Name: index_exclusive_access_end_time; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_exclusive_access_end_time ON exclusive_access USING btree (end_time);


--
-- Name: index_exclusive_access_permission; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_exclusive_access_permission ON exclusive_access USING btree (permission);


--
-- Name: index_exclusive_access_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_exclusive_access_user_id ON exclusive_access USING btree (user_id);


--
-- Name: index_expression_name; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_expression_name ON expression USING btree (name);


--
-- Name: index_ip_group_name; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_ip_group_name ON ip_group USING btree (name);


--
-- Name: index_management_studio_log_timestamp; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_management_studio_log_timestamp ON management_studio_log USING btree ("timestamp");


--
-- Name: index_management_studio_log_timestamp_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_management_studio_log_timestamp_user_id ON management_studio_log USING btree (user_id);


--
-- Name: index_permission_exclusive_access_allowed; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_permission_exclusive_access_allowed ON permission USING btree (exclusive_access_allowed);


--
-- Name: index_permission_name; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_permission_name ON permission USING btree (name);


--
-- Name: index_permission_resource; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_permission_resource ON permission USING btree (resource);


--
-- Name: index_rbac_log_action; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_rbac_log_action ON rbac_log USING btree (action);


--
-- Name: index_rbac_log_severity; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_rbac_log_severity ON rbac_log USING btree (severity);


--
-- Name: index_rbac_log_timestamp; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_rbac_log_timestamp ON rbac_log USING btree ("timestamp");


--
-- Name: index_rbac_log_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_rbac_log_user_id ON rbac_log USING btree (user_id);


--
-- Name: index_resource_name; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_resource_name ON resource USING btree (name);


--
-- Name: index_role_name; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_role_name ON role USING btree (name);


--
-- Name: index_rule_name; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_rule_name ON rule USING btree (name);


--
-- Name: index_token_expiration_date; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_token_expiration_date ON token USING btree (expiration_date);


--
-- Name: index_token_role_token; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_token_role_token ON token_role USING btree (token);


--
-- Name: index_token_token_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_token_token_id ON token USING btree (token_id);


--
-- Name: index_token_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_token_user_id ON token USING btree (user_id);


--
-- Name: index_user_role_assignment; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_user_role_assignment ON user_role USING btree (assignment);


--
-- Name: index_user_role_end_time; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_user_role_end_time ON user_role USING btree (end_time);


--
-- Name: index_user_role_role; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_user_role_role ON user_role USING btree (role);


--
-- Name: index_user_role_start_time; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_user_role_start_time ON user_role USING btree (start_time);


--
-- Name: index_user_role_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_user_role_user_id ON user_role USING btree (user_id);


--
-- Name: fk_access_security_input_as_group; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_security_input
    ADD CONSTRAINT fk_access_security_input_as_group FOREIGN KEY (as_group) REFERENCES access_security_group(id);


--
-- Name: fk_access_security_rule_as_group; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_security_rule
    ADD CONSTRAINT fk_access_security_rule_as_group FOREIGN KEY (as_group) REFERENCES access_security_group(id);


--
-- Name: fk_access_security_rule_ip_group_ip_groups; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_security_rule_ip_group
    ADD CONSTRAINT fk_access_security_rule_ip_group_ip_groups FOREIGN KEY (ipgroups_id) REFERENCES ip_group(id);


--
-- Name: fk_access_security_rule_ip_group_rule; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_security_rule_ip_group
    ADD CONSTRAINT fk_access_security_rule_ip_group_rule FOREIGN KEY (access_security_rule_id) REFERENCES access_security_rule(id);


--
-- Name: fk_access_security_rule_role_as_rule; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_security_rule_role
    ADD CONSTRAINT fk_access_security_rule_role_as_rule FOREIGN KEY (rules_id) REFERENCES access_security_rule(id);


--
-- Name: fk_access_security_rule_role_role; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY access_security_rule_role
    ADD CONSTRAINT fk_access_security_rule_role_role FOREIGN KEY (roles_id) REFERENCES role(id);


--
-- Name: fk_exclusive_access_permission; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY exclusive_access
    ADD CONSTRAINT fk_exclusive_access_permission FOREIGN KEY (permission) REFERENCES permission(id);


--
-- Name: fk_ip_group_ip_ip_group; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ip_group_ip
    ADD CONSTRAINT fk_ip_group_ip_ip_group FOREIGN KEY (ip_group_id) REFERENCES ip_group(id);


--
-- Name: fk_permission_resource; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY permission
    ADD CONSTRAINT fk_permission_resource FOREIGN KEY (resource) REFERENCES resource(id);


--
-- Name: fk_permission_rule; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY permission
    ADD CONSTRAINT fk_permission_rule FOREIGN KEY (rule) REFERENCES rule(id);


--
-- Name: fk_resource_managers_resource; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY resource_managers
    ADD CONSTRAINT fk_resource_managers_resource FOREIGN KEY (resource_id) REFERENCES resource(id);


--
-- Name: fk_role_managers_role; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY role_managers
    ADD CONSTRAINT fk_role_managers_role FOREIGN KEY (role_id) REFERENCES role(id);


--
-- Name: fk_role_permission_permission; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY role_permission
    ADD CONSTRAINT fk_role_permission_permission FOREIGN KEY (permissions_id) REFERENCES permission(id);


--
-- Name: fk_role_permission_role; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY role_permission
    ADD CONSTRAINT fk_role_permission_role FOREIGN KEY (role_id) REFERENCES role(id);


--
-- Name: fk_rule_expression_expression; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_expression
    ADD CONSTRAINT fk_rule_expression_expression FOREIGN KEY (expression_id) REFERENCES expression(id);


--
-- Name: fk_rule_expression_rule; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_expression
    ADD CONSTRAINT fk_rule_expression_rule FOREIGN KEY (rule_id) REFERENCES rule(id);


--
-- Name: fk_token_role_role; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY token_role
    ADD CONSTRAINT fk_token_role_role FOREIGN KEY (role) REFERENCES user_role(id);


--
-- Name: fk_token_role_token; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY token_role
    ADD CONSTRAINT fk_token_role_token FOREIGN KEY (token) REFERENCES token(id);


--
-- Name: fk_user_role_role; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT fk_user_role_role FOREIGN KEY (role) REFERENCES role(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
