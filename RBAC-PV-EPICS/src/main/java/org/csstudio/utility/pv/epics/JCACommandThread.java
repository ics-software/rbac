/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv.epics;

import gov.aps.jca.CAException;
import gov.aps.jca.Context;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JCA command pump, added for two reasons:
 * <ol>
 * <li>JCA callbacks can't directly send JCA commands without danger of a deadlock, at least not with JNI and the
 * "DirectRequestDispatcher".
 * <li>Instead of calling 'flushIO' after each command, this thread allows for a few requests to queue up, then
 * periodically pumps them out with only a final 'flush'
 * </ol>
 * 
 * @author Kay Kasemir
 */
class JCACommandThread extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(JCACommandThread.class);
    /**
     * Delay between queue inspection. Longer delay results in bigger 'batches', which is probably good, but also
     * increases the latency.
     */
    private static final long DELAY_MILLIS = 100;

    /** The JCA Context */
    private final Context jcaContext;

    /**
     * Command queue.
     * <p>
     * SYNC on access
     */
    private final List<Runnable> commandQueue = new LinkedList<>();

    /** Maximum size that command_queue reached at runtime */
    private int maxSizeReached = 0;

    /** Flag to tell thread to run or quit */
    private volatile boolean run = false;

    /**
     * Construct, but don't start the thread.
     * 
     * @param jcaContext the context to flush
     * @see #start()
     */
    public JCACommandThread(final Context jcaContext) {
        super("JCA Command Thread");
        this.jcaContext = jcaContext;
    }

    /**
     * Version of <code>start</code> that may be called multiple times.
     * <p>
     * The thread must only be started after the first PV has been created. Otherwise, if flush is called without PVs,
     * JNI JCA reports pthread errors.
     * <p>
     * NOP when already running
     */
    @Override
    public synchronized void start() {
        if (run) {
            return;
        }
        run = true;
        super.start();
    }

    /** Stop the thread and wait for it to finish. */
    void shutdown() {
        run = false;
        try {
            join();
        } catch (InterruptedException ex) {
            LOGGER.error("JCACommandThread shutdown", ex);
        }
    }

    /**
     * Add a command to the queue.
     * 
     * @param command the command to offer to the queue
     */
    void addCommand(final Runnable command) {
        synchronized (commandQueue) {
            // New maximum queue length (+1 for the one about to get added)
            if (commandQueue.size() >= maxSizeReached) {
                maxSizeReached = commandQueue.size() + 1;
            }
            commandQueue.add(command);
        }
    }

    /** @return Oldest queued command or <code>null</code> */
    private Runnable getCommand() {
        synchronized (commandQueue) {
            if (!commandQueue.isEmpty()) {
                return commandQueue.remove(0);
            }
        }
        return null;
    }

    @Override
    public void run() {
        while (run) {
            // Execute all the commands currently queued...
            Runnable command = getCommand();
            while (command != null) {
                // Execute one command
                try {
                    command.run();
                } catch (Exception ex) {
                    LOGGER.error("JCACommandThread exception", ex);
                }
                // Get next command
                command = getCommand();
            }
            // Flush.
            // Once, after executing all the accumulated commands.
            // Even when the command queue was empty,
            // there may be stuff worth flushing.
            try {
                jcaContext.flushIO();
            } catch (CAException | IllegalStateException ex) {
                LOGGER.error("JCA Flush exception", ex);
            }
            // Then wait.
            try {
                Thread.sleep(DELAY_MILLIS);
            } catch (InterruptedException ex) {
                // ignore
            }
        }
    }
}
