/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv.epics;

/**
 * 
 * <code>MonitorMask</code> specifies the event mask for epics events. When connecting to the PV the user always
 * connects with one of the masks to filter out unnecessary event changes.
 * 
 */
public enum MonitorMask {
    /** Listen to changes in value and in alarms */
    VALUE(1 | 4),
    /** Listen to changes in value beyond archive limit */
    ARCHIVE(2),
    /** Listen to changes in alarm state */
    ALARM(1 | 4),
    /** Listen to all events */
    ALL(1 | 2 | 4);

    private final int mask;

    private MonitorMask(final int mask) {
        this.mask = mask;
    }

    /** @return Mask bits used in underlying CA call */
    public int getMask() {
        return mask;
    }
}
