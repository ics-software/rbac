/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv.epics;

import org.csstudio.utility.pv.PV;
import org.csstudio.utility.pv.PVException;
import org.csstudio.utility.pv.PVFactory;

/**
 * PV Factory for EPICS V3 PVs.
 * 
 * @author Kay Kasemir
 */
public class EPICSPVFactory implements PVFactory {

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PVFactory#createPV(java.lang.String)
     */
    @Override
    public PV createPV(final String name) throws PVException {
        return new EPICSv3PV(name);
    }
}
