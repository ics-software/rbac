/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv.epics;

import gov.aps.jca.CAException;
import gov.aps.jca.Channel;
import gov.aps.jca.Channel.ConnectionState;
import gov.aps.jca.dbr.DBR;
import gov.aps.jca.dbr.DBRType;
import gov.aps.jca.event.ConnectionEvent;
import gov.aps.jca.event.ConnectionListener;
import gov.aps.jca.event.GetEvent;
import gov.aps.jca.event.GetListener;
import gov.aps.jca.event.MonitorEvent;
import gov.aps.jca.event.MonitorListener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.csstudio.data.values.IMetaData;
import org.csstudio.data.values.IValue;
import org.csstudio.utility.pv.PV;
import org.csstudio.utility.pv.PVException;
import org.csstudio.utility.pv.PVListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * EPICS ChannelAccess implementation of the PV interface.
 * 
 * @see PV
 * @author Kay Kasemir
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
public class EPICSv3PV implements PV, ConnectionListener, MonitorListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EPICSv3PV.class);

    /** Listener to a get-callback for data. */
    private class GetCallbackListener implements GetListener {
        /** The received meta data/ */
        private IMetaData meta;

        /** The received value. */
        private IValue value;

        /**
         * After updating <code>meta</code> and <code>value</code>, this flag is set, and then <code>notify</code> is
         * invoked on <code>this</code>.
         */
        private boolean gotResponse = false;

        private synchronized void reset() {
            gotResponse = false;
        }

        @Override
        public void getCompleted(final GetEvent event) {
            if (event.getStatus().isSuccessful()) {
                final DBR dbr = event.getDBR();
                meta = DBRHelper.decodeMetaData(dbr);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(name + " meta: " + meta);
                }
                try {
                    value = DBRHelper.decodeValue(meta, dbr);
                } catch (IllegalArgumentException ex) {
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error("PV " + name, ex);
                    }
                    value = null;
                }
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(name + " value: " + value);
                }
            } else {
                meta = null;
                value = null;
            }
            synchronized (this) {
                gotResponse = true;
                this.notifyAll();
            }
        }
    }

    private static enum State {
        /** Nothing happened, yet */
        IDLE,
        /** Trying to connect */
        CONNECTING,
        /** Got basic connection */
        CONNECTED,
        /** Requested MetaData */
        GETTING_META_DATA,
        /** Received MetaData */
        GOT_META_DATA,
        /** Subscribing to receive value updates */
        SUBSCRIBING,
        /**
         * Received Value Updates
         * <p>
         * This is the ultimate state!
         */
        GOT_MONITOR,
        /** Got disconnected */
        DISCONNECTED
    }

    private final String name;
    private State state = State.IDLE;
    private final List<PVListener> listeners = new CopyOnWriteArrayList<>();
    private RefCountedChannel channelRef;
    private RefCountedMonitor subscription;

    /**
     * isConnected? <code>true</code> if we are currently connected (based on the most recent connection callback).
     * <p>
     * EPICS_V3_PV also runs notifyAll() on <code>this</code> whenever the connected flag changes to <code>true</code>.
     */
    private boolean connected = false;

    /** Meta data obtained during connection cycle. */
    private IMetaData meta;

    /** Most recent live value. */
    private IValue value;

    /** Indicates if the PV has been started and not yet stopped. */
    private volatile boolean running = false;

    /** Listener to the get... for meta data */
    private final GetListener metaDataGetListener = new GetListener() {
        @Override
        public void getCompleted(final GetEvent event) {
            if (event.getStatus().isSuccessful()) {
                state = State.GOT_META_DATA;
                meta = DBRHelper.decodeMetaData(event.getDBR());
            } else {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error(name + " meta data get error: " + event.getStatus().getMessage());
                }
            }
            // Subscribe, but outside of callback (JCA deadlocks)
            PVContext.scheduleCommand(new Runnable() {
                @Override
                public void run() {
                    subscribe();
                }
            });
        }
    };

    private final GetCallbackListener getCallback = new GetCallbackListener();

    /**
     * Generate an EPICS PV.
     * 
     * @param name The PV name.
     */
    public EPICSv3PV(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable {
        try {
            if (channelRef != null) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("EPICS_V3_PV " + name + " not properly stopped");
                }
                stop();
            }
        } finally {
            super.finalize();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /*
     * @see org.csstudio.utility.pv.PV#getValue(double)
     */
    @Override
    public IValue getValue(final double timeoutSeconds) throws PVException {
        try {
            long endTime = System.currentTimeMillis() + (long) (timeoutSeconds * 1000);
            // Try to connect (NOP if already connected)
            connect();
            // Wait for connection
            while (!connected) {
                long remain = endTime - System.currentTimeMillis();
                if (remain <= 0) {
                    throw new PVException(name, "PV " + name + " connection timeout.");
                }
                synchronized (this) {
                    this.wait(remain);
                }
            }
            // Reset the callback data
            getCallback.reset();
            // Issue the 'get'
            DBRType type = DBRHelper.getCtrlType(channelRef.getChannel().getFieldType());

            channelRef.getChannel().get(type, channelRef.getChannel().getElementCount(), getCallback);
            // Wait for value callback
            synchronized (getCallback) {
                while (!getCallback.gotResponse) {
                    long remain = endTime - System.currentTimeMillis();
                    if (remain <= 0) {
                        throw new PVException(name, "PV " + name + " value timeout");
                    }
                    getCallback.wait(remain);
                }
            }
            value = getCallback.value;
            return value;
        } catch (CAException | InterruptedException e) {
            throw new PVException(name, "Could not read the value of the PV '" + name + "'.", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#getValue()
     */
    @Override
    public IValue getValue() {
        return value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#addListener(org.csstudio.utility.pv.PVListener)
     */
    @Override
    public void addListener(final PVListener listener) {
        listeners.add(listener);
        if (running && isConnected()) {
            listener.pvValueUpdate(this);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#removeListener(org.csstudio.utility.pv.PVListener)
     */
    @Override
    public void removeListener(final PVListener listener) {
        listeners.remove(listener);
    }

    /**
     * Try to connect to the PV. OK to call more than once.
     */
    private void connect() throws PVException {
        state = State.CONNECTING;
        // Already attempted a connection?
        synchronized (this) {
            if (channelRef == null) {
                try {
                    channelRef = PVContext.getChannel(name, EPICSv3PV.this);
                } catch (CAException e) {
                    state = State.DISCONNECTED;
                    throw new PVException(name,"Could not connect to channel '" + name + "'.", e);
                }
            }
        }
        if (channelRef.getChannel().getConnectionState() == ConnectionState.CONNECTED) {
            handleConnected(channelRef.getChannel());
        }
    }

    /**
     * Disconnect from the PV. OK to call more than once.
     */
    private void disconnect() {
        // Releasing the _last_ channel will close the context,
        // which waits for the JCA Command thread to exit.
        // If a connection or update for the channel happens at that time,
        // the JCA command thread will send notifications to this PV,
        // which had resulted in dead lock:
        // This code locked the PV, then tried to join the JCA Command thread.
        // JCA Command thread tried to lock the PV, so it could not exit.
        // --> Don't lock while calling into the PVContext.
        RefCountedChannel channelRefCopy;
        synchronized (this) {
            // Never attempted a connection?
            if (channelRef == null) {
                return;
            }
            channelRefCopy = channelRef;
            channelRef = null;
        }
        try {
            PVContext.releaseChannel(channelRefCopy, this);
        } catch (CAException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Could not disconnect channel " + getName() + ".", e);
            }
        }
        fireDisconnected();
    }

    /** Subscribe for value updates. */
    private void subscribe() {
        synchronized (this) {
            // Prevent multiple subscriptions.
            if (subscription != null) {
                return;
            }
            // Late callback, channel already closed?
            RefCountedChannel chRef = channelRef;
            if (chRef == null) {
                return;
            }
            Channel channel = chRef.getChannel();
            try {
                DBRType type = DBRHelper.getTimeType(channel.getFieldType());
                state = State.SUBSCRIBING;
                subscription = PVContext.getMonitor(channel, type, PVContext.MONITOR_MASK.getMask());
                subscription.addMonitorListener(this);
            } catch (CAException ex) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error(name + " subscribe error", ex);
                }
            }
        }
    }

    /** Unsubscribe from value updates. */
    private void unsubscribe() {
        RefCountedMonitor subCopy;
        // Atomic access
        synchronized (this) {
            subCopy = subscription;
            subscription = null;
        }
        if (subCopy == null) {
            return;
        }
        try {
            subCopy.removeMonitorListener(this);
            PVContext.releaseMonitor(subCopy);
        } catch (CAException ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(name + " unsubscribe error.", ex);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#start()
     */
    @Override
    public void start() throws PVException {
        if (running) {
            return;
        }
        running = true;
        connect();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#isRunning()
     */
    @Override
    public boolean isRunning() {
        return running;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#isConnected()
     */
    @Override
    public boolean isConnected() {
        return connected;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#isWriteAllowed()
     */
    @Override
    public boolean isWriteAllowed() {
        return connected && channelRef != null && channelRef.getChannel() != null
                && channelRef.getChannel().getWriteAccess();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#getStateInfo()
     */
    @Override
    public String getStateInfo() {
        return state.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#stop()
     */
    @Override
    public void stop() {
        running = false;
        unsubscribe();
        disconnect();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.utility.pv.PV#setValue(java.lang.Object)
     */
    @Override
    public void setValue(final Object newValue) throws PVException {
        if (!isConnected()) {
            throw new PVException(name,name + " is not connected.");
        }
        try {
            if (newValue instanceof String) {
                channelRef.getChannel().put((String) newValue);
            } else if (newValue instanceof String[]) {
                channelRef.getChannel().put((String[]) newValue);
            } else if (newValue instanceof Double) {
                channelRef.getChannel().put(((Double) newValue).doubleValue());
            } else if (newValue instanceof Double[]) {
                Double[] dbl = (Double[]) newValue;
                double[] val = new double[dbl.length];
                for (int i = 0; i < val.length; ++i) {
                    val[i] = dbl[i].doubleValue();
                }
                channelRef.getChannel().put(val);
            } else if (newValue instanceof Integer) {
                channelRef.getChannel().put(((Integer) newValue).intValue());
            } else if (newValue instanceof Integer[]) {
                Integer[] ival = (Integer[]) newValue;
                int[] val = new int[ival.length];
                for (int i = 0; i < val.length; ++i) {
                    val[i] = ival[i].intValue();
                }
                channelRef.getChannel().put(val);
            } else if (newValue instanceof double[]) {
                channelRef.getChannel().put((double[]) newValue);
            } else if (newValue instanceof int[]) {
                channelRef.getChannel().put((int[]) newValue);
            } else {
                throw new PVException(name,"Cannot handle value of type '" + newValue.getClass().getName() + "'.");
            }
        } catch (CAException e) {
            String strVal = String.valueOf(newValue);
            throw new PVException(name,"Cannot set value '" + strVal + "' to channel '" 
                        + name + "'.", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see gov.aps.jca.event.ConnectionListener#connectionChanged(gov.aps.jca.event .ConnectionEvent)
     */
    @Override
    public void connectionChanged(final ConnectionEvent ev) {
        // This runs in a CA thread
        if (ev.isConnected()) {
            // Transfer to JCACommandThread to avoid deadlocks
            PVContext.scheduleCommand(new Runnable() {
                @Override
                public void run() {
                    handleConnected((Channel) ev.getSource());
                }
            });
        } else {
            state = State.DISCONNECTED;
            connected = false;
            PVContext.scheduleCommand(new Runnable() {
                @Override
                public void run() {
                    unsubscribe();
                    fireDisconnected();
                }
            });
        }
    }

    /**
     * PV is connected. Get meta info, or subscribe right away.
     */
    private void handleConnected(final Channel channel) {
        if (state == State.CONNECTED) {
            return;
        }
        state = State.CONNECTED;

        // If we're "running", we need to get the meta data and
        // then subscribe.
        // Otherwise, we're done.
        if (!running) {
            meta = null;
            synchronized (this) {
                connected = true;
                this.notifyAll();
            }
            return;
        }
        // else: running, get meta data, then subscribe
        try {
            DBRType type = channel.getFieldType();
            if (!type.isSTRING()) {
                state = State.GETTING_META_DATA;
                if (type.isDOUBLE() || type.isFLOAT()) {
                    type = DBRType.CTRL_DOUBLE;
                } else if (type.isENUM()) {
                    type = DBRType.LABELS_ENUM;
                } else {
                    type = DBRType.CTRL_SHORT;
                }
                channel.get(type, 1, metaDataGetListener);
                return;
            }
        } catch (final Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(name + " connection handling error.", ex);
            }
            return;
        }

        // Meta info is not requested, not available for this type,
        // or there was an error in the get call.
        // So reset it, then just move on to the subscription.
        meta = null;
        subscribe();
    }

    /*
     * (non-Javadoc)
     * 
     * @see gov.aps.jca.event.MonitorListener#monitorChanged(gov.aps.jca.event. MonitorEvent)
     */
    @Override
    public void monitorChanged(final MonitorEvent ev) {
        // This runs in a CA thread.
        // Ignore values that arrive after stop()
        if (!running) {
            return;
        }

        synchronized (this) {
            if (subscription == null) {
                return;
            }
        }

        if (!ev.getStatus().isSuccessful()) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(name + " monitor error:" + ev.getStatus().getMessage());
            }
            return;
        }

        state = State.GOT_MONITOR;
        try {
            value = DBRHelper.decodeValue(meta, ev.getDBR());
            synchronized (this) {
                connected = true;
                this.notifyAll();
            }
            fireValueUpdate();
        } catch (IllegalArgumentException ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(name + " monitor value error.", ex);
            }
        }
    }

    /** Notify all listeners. */
    private void fireValueUpdate() {
        for (final PVListener listener : listeners) {
            listener.pvValueUpdate(this);
        }
    }

    /** Notify all listeners. */
    private void fireDisconnected() {
        for (final PVListener listener : listeners) {
            listener.pvDisconnected(this);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "EPICS_V3_PV '" + name + "'";
    }

    /*
     * @see org.csstudio.utility.pv.PV#blockUntilFirstUpdate(int)
     */
    @Override
    public void blockUntilFirstUpdate(int timeout) throws PVException {
        if (value != null) {
            return;
        }
        try {
            long endTime = System.currentTimeMillis() + timeout;
            while (value == null) {
                long remain = endTime - System.currentTimeMillis();
                if (remain <= 0) {
                    throw new PVException(name,"Could not block until first update for pv '" + name + ".");
                }
                synchronized (this) {
                    this.wait(remain);
                }
            }
        } catch (InterruptedException e) {
            throw new PVException(name,"Could not block until first update for pv '" + name + ".", e);
        }
    }
}
