/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.dsaccess.DirectoryServiceAccess;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.jaxb.TokenInfo;
import se.esss.ics.rbac.logic.exception.AuthAndAuthException;
import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.logic.exception.TokenInvalidException;
import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.datamodel.TokenRole;
import se.esss.ics.rbac.datamodel.UserRole;
import se.esss.ics.rbac.datamodel.UserRole.AssignmentType;

/**
 *
 * <code>AuthenticationEJB</code> implements the logic related to authentication, such as login, logout, token retrieval
 * etc.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
@Stateless(description = "The bean takes care of user authentication actions (login, logout, get token etc.")
public class AuthenticationEJB implements Serializable {

    private static final long serialVersionUID = 8490833901259765115L;

    @PersistenceContext(unitName = "RBAC")
    private transient EntityManager entityManager;
    @EJB
    private DirectoryServiceAccess directoryService;
    @EJB
    private SignatureEJB signatureEJB;
    @EJB
    private GeneralEJB generalEJB;

    /**
     * Verifies the input parameters and throws an exception if any of the parameters is null or empty.
     *
     * @param username the username character array
     * @param password the password character array
     * @param ip the IP address
     *
     * @throws IllegalRBACArgumentException if any of the parameters is null or empty
     */
    private static void verifyInputParameters(char[] username, char[] password, String ip)
            throws IllegalRBACArgumentException {
        if (username == null || username.length == 0) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "Username"));
        } else if (password == null || password.length == 0) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "Password"));
        } else if (ip == null || ip.isEmpty()) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "IP address"));
        }
    }

    /**
     * Tries to login the user with the directory service. If successful, user info is returned. If not successful an
     * exception is thrown.
     *
     * @param username the username used for authentication
     * @param password the password used for authentication
     *
     * @return the user info of successfully logged in user
     *
     * @throws AuthAndAuthException if login failed due to incorrect credentials
     * @throws RBACException if login failed due to directory service error
     */
    private UserInfo tryLogin(char[] username, char[] password) throws RBACException {
        try {
            UserInfo dsUserInfo = directoryService.login(username, password);
            if (dsUserInfo == null) {
                throw new AuthAndAuthException(null, null, Messages.getString(
                        Messages.AUTHENTICATION_INCORRECT_USERNAME, new String(username)));
            }
            return dsUserInfo;
        } catch (DirectoryServiceAccessException e) {
            throw new RBACException(new String(username), Messages.getString(Messages.AUTHENTICATION_FAILED,
                    new String(username)), e);
        }
    }

    /**
     * Method attempts to login the user using {@link DirectoryServiceAccess}. If the login is successful, a new
     * {@link Token} is created, populated with provided data and persisted. Token information, including the token data
     * signature is returned to the caller as {@link TokenInfo}.
     *
     * @param username character array containing login user name
     * @param password character array containing login password
     * @param ip address of the login attempt call
     * @param roleNames array of role preferred role names
     * @return token info containing all of the created token's information and a signature
     *
     * @throws IllegalRBACArgumentException if any of the required parameters is <code>null</code>
     * @throws AuthAndAuthException if the user could not be authenticated
     * @throws RBACException if there is an error while creating token signature
     */
    public TokenInfo login(char[] username, char[] password, String ip, String[] roleNames) throws RBACException {
        verifyInputParameters(username, password, ip);

        UserInfo dsUserInfo = tryLogin(username, password);

        // Determine and populate the token values (time, roles, ...).
        Token token = new Token();
        token.setUserId(String.copyValueOf(username));
        token.setIp(ip);

        long currentTime = System.currentTimeMillis();
        long expirationPeriod = generalEJB.getExpirationPeriod();
        Timestamp creationTime = new Timestamp(currentTime);
        Timestamp expirationTime = new Timestamp(currentTime + expirationPeriod);
        token.setCreationDate(creationTime);
        token.setExpirationDate(expirationTime);

        List<UserRole> roles;
        if (roleNames == null || roleNames.length == 0) {
            roles = entityManager.createNamedQuery("UserRole.findValidByUserId", UserRole.class)
                    .setParameter("userId", new String(username)).getResultList();
        } else {
            roles = entityManager.createNamedQuery("UserRole.findValidByNamesAndUserId", UserRole.class)
                    .setParameter("names", Arrays.asList(roleNames)).setParameter("userId", new String(username))
                    .getResultList();
        }
        Set<TokenRole> tokenRoles = new HashSet<>();
        Date now = new Date();
        for (UserRole r : roles) {
            if (r.getAssignment() == AssignmentType.NORMAL
                    || (r.getStartTime().before(now) && r.getEndTime().after(now))) {
                tokenRoles.add(new TokenRole(token, r));
            }
        }
        token.setRole(tokenRoles);

        TokenInfo tokenInfo = TokenUtilities.fromToken(token);
        tokenInfo.setFirstName(dsUserInfo.getFirstName());
        tokenInfo.setLastName(dsUserInfo.getLastName());

        byte[] signature = signatureEJB.generateSignature(TokenUtilities.generateTokenData(tokenInfo));
        tokenInfo.setSignature(signature);

        String tokenID = TokenUtilities.generateTokenID(tokenInfo);
        tokenInfo.setTokenID(tokenID);
        token.setTokenId(tokenID);

        entityManager.persist(token);
        for (TokenRole tr : tokenRoles) {
            entityManager.persist(tr);
        }
        return tokenInfo;
    }

    /**
     * Verifies if the token with the provided ID exists and if it is valid. This method does not do any changes to the
     * token, like extending its expiration period. It also does not fetch any data besides the token itself, to allow
     * to return as quickly as possible. The returned info contains only the info that Token contains. The first name,
     * last name, UUID and signature are not returned.
     *
     * @param tokenID the id of the token
     *
     * @return the token info if the token is valid
     *
     * @throws IllegalRBACArgumentException if the token ID is null or empty
     * @throws TokenInvalidException if the token is invalid or does not exist
     * @throws DataConsistencyException if there are more tokens with the given ID
     */
    public TokenInfo isTokenValid(String tokenID) throws IllegalRBACArgumentException, TokenInvalidException,
            DataConsistencyException {
        if (tokenID == null || tokenID.isEmpty()) {
            throw new IllegalRBACArgumentException(Messages.getString(Messages.PARAMETER_NOT_PROVIDED, "Token ID"));
        }
        Token token = generalEJB.fetchToken(tokenID);
        GeneralEJB.validateToken(token);
        return TokenUtilities.fromToken(token);
    }

    /**
     * Logs the caller out by deleting the token with provided ID. The token is only deleted, if it exists. If the token
     * does not exist, logout is considered successful, but the method will return null.
     *
     * @param tokenID id of the token to be deleted.
     *
     * @return token info if the logout has been successful, null if the token was already logged out or expired or it
     *         never existed
     *
     * @throws IllegalRBACArgumentException if any of the required parameters is <code>null</code>.
     * @throws DataConsistencyException if multiple tokens exist
     * @throws RBACException if there was an error communicating with the directory service
     */
    public TokenInfo logout(String tokenID) throws RBACException {
        GeneralEJB.verifyInputParameters(tokenID);

        try {
            Token token = generalEJB.fetchToken(tokenID);
            if (directoryService.logout(token.getUserId().toCharArray())) {
                entityManager.remove(token);
                return TokenUtilities.fromToken(token);
            } else {
                throw new RBACException(token.getUserId(), Messages.getString(Messages.DS_LOGOUT_FAILED,
                        token.getUserId()));
            }
        } catch (TokenInvalidException e) {
            return null;
        }
    }

    /**
     * Method checks if a token with provided ID exists. It fetches the token with the specified ID from the database,
     * creates a {@link TokenInfo} from it and returns it. Expired tokens may not and will not be retrieved.
     *
     * @param tokenID id of the token to be returned
     *
     * @return token with the specified tokenID
     *
     * @throws IllegalRBACArgumentException if any of the required parameters is <code>null</code>.
     * @throws RBACException if there was an error communicating with the directory service
     * @throws DataConsistencyException if multiple tokens were found
     * @throws TokenInvalidException if the token does not exist
     */
    public TokenInfo getToken(String tokenID) throws RBACException {
        return getOrRenewToken(tokenID, false);
    }

    /**
     * Method checks if a token with provided ID exists. It fetches the token with the specified ID from the database,
     * extends its expiration time, creates a {@link TokenInfo} from it and returns it. Expired tokens may not and will
     * not be renewed. Exception will be thrown if an expired token is being renewed.
     *
     * @param tokenID id of the token to be renewed.
     *
     * @return token with the specified tokenID and extended expiration time.
     *
     * @throws IllegalRBACArgumentException if any of the required parameters is <code>null</code>.
     * @throws TokenInvalidException if the token is invalid
     * @throws DataConsistencyException if multiple tokens or permissions exist
     * @throws RBACException if there was an error retrieving data from the directory service
     */
    public TokenInfo renewToken(String tokenID) throws RBACException {
        return getOrRenewToken(tokenID, true);
    }

    /*
     * @throws IllegalRBACArgumentException
     * @throws DataConsistencyException
     * @throws TokenInvalidException
     * @throws RBACException
     */
    private TokenInfo getOrRenewToken(String tokenID, boolean renew) throws RBACException {
        GeneralEJB.verifyInputParameters(tokenID);

        Token token = generalEJB.fetchToken(tokenID);
        GeneralEJB.validateToken(token);
        if (renew) {
            token.setExpirationDate(new Timestamp(System.currentTimeMillis() + generalEJB.getExpirationPeriod()));
        }
        TokenInfo tokenInfo = TokenUtilities.fromToken(token);
        try {
            UserInfo userInfo = directoryService.getUserInfo(token.getUserId().toCharArray());
            if (userInfo == null) {
                throw new RBACException(token.getUserId(), Messages.getString(Messages.USER_NOT_FOUND,
                        token.getUserId()));
            }
            tokenInfo.setFirstName(userInfo.getFirstName());
            tokenInfo.setLastName(userInfo.getLastName());
        } catch (DirectoryServiceAccessException e) {
            throw new RBACException(token.getUserId(), Messages.getString(Messages.USER_INFO_RETRIEVAL_FAILED,
                    token.getUserId()), e);
        }
        byte[] signature = signatureEJB.generateSignature(TokenUtilities.generateTokenData(tokenInfo));
        tokenInfo.setSignature(signature);
        return tokenInfo;
    }
}
