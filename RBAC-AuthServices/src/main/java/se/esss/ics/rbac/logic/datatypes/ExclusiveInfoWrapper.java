/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.datatypes;

import se.esss.ics.rbac.jaxb.ExclusiveInfo;

/**
 * 
 * <code>ExclusiveInfoWrapper</code> is a wrapper for the {@link ExclusiveInfo} which also provides the username of the
 * owner of the exclusive access.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class ExclusiveInfoWrapper {

    /** The exclusive access info wrapped by this wrapper */
    private final ExclusiveInfo info;
    /** The owner of the exclusive access info */
    private final String user;
    /** True if the exclusive access was released or false if exclusive access was taken */
    private final boolean released;

    /**
     * Constructs a new ExclusiveInfoWrapper.
     * 
     * @param info the info
     * @param user the owner of the info
     * @param released true if exclusive access was released or false if it didn't exist
     */
    public ExclusiveInfoWrapper(ExclusiveInfo info, String user, boolean released) {
        this.info = info;
        this.user = user;
        this.released = released;
    }

    /**
     * Returns the exclusive access info wrapped by this wrapper.
     * 
     * @return the exclusive access info
     */
    public ExclusiveInfo getInfo() {
        return info;
    }

    /**
     * Returns the owner of the exclusive access.
     * 
     * @return the owner of exclusive access
     */
    public String getUser() {
        return user;
    }

    /**
     * Returns true if exclusive access was released or false if it was taken.
     * 
     * @return true if released or false if taken
     */
    public boolean isReleased() {
        return released;
    }
}
