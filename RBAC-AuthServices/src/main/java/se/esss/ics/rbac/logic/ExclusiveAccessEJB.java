/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.jaxb.ExclusiveInfo;
import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.logic.datatypes.ExclusiveInfoWrapper;
import se.esss.ics.rbac.logic.exception.AuthAndAuthException;
import se.esss.ics.rbac.logic.exception.DataConsistencyException;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.InvalidResourceException;
import se.esss.ics.rbac.logic.exception.TokenInvalidException;
import se.esss.ics.rbac.ConfigurationParameter;
import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Token;

/**
 * 
 * <code>ExclusiveAccessEJB</code> implements the business logic behind the request and release of exclusive access.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Stateless(description = "A bean, which provides the logic to request and release exclusice access.")
public class ExclusiveAccessEJB implements Serializable {

    private static final long serialVersionUID = -1277956125484691989L;

    // default exclusive access duration is 1 hour
    private static final long DEFAULT_EXCLUSIVE_ACCESS_DURATION = 3600000;
    /** The name of the resource that the permissions used by the service belongs to. */
    public static final String RESOURCE_NAME = "RBACManagementStudio";
    /**
     * The name of the permission that allows exclusive access release (the owner of exclusive access and requester have
     * to be the same).
     */
    public static final String RELEASE_EXCLUSIVE_ACCESS = "ReleaseExclusiveAccess";
    /** The name of the configuration parameter that defines the default exclusive access duration. */
    public static final String PARAM_EXLUSIVE_ACCESS_DURATION = "EXCLUSIVE_ACCESS_DURATION";

    @PersistenceContext(unitName = "RBAC")
    private transient EntityManager entityManager;
    @EJB
    private AuthorizationEJB authorizationEJB;
    @EJB
    private GeneralEJB generalEJB;

    /**
     * Method checks, if a token with provided ID exists. If the specified permission already has an exclusive access
     * set, the caller's permissions are checked, before reseting the exclusive access. Exclusive access is granted to
     * the token owner only, if permission allows exclusive access, if caller matches the one in token and if an active
     * exclusive access grant does not already exist for another user. If exclusive access is granted, any previously
     * existing exclusive access for the same permission is removed.
     * 
     * @param tokenID id of the token to be used for user verification
     * @param resourceName name of the resource associated with permissions
     * @param permissionName name of the permission to be granted exclusive access on
     * @param duration time in milliseconds after which the exclusive access will expire
     * 
     * @return exclusive access information containing permission name and access expiration date
     * 
     * @throws IllegalRBACArgumentException if any of the required parameters is <code>null</code>
     * @throws AuthAndAuthException if the the token owner does not have rights to release the exclusive access
     * @throws TokenInvalidException if the user is not logged in
     * @throws InvalidResourceException if the resource or permission does not exist
     * @throws DataConsistencyException if multiple tokens or permissions exist or a rule could not be evaluated
     */
    public ExclusiveInfoWrapper requestExclusiveAccess(String tokenID, String resourceName, String permissionName,
            long duration) throws IllegalRBACArgumentException, DataConsistencyException, InvalidResourceException,
            AuthAndAuthException, TokenInvalidException {
        GeneralEJB.verifyInputParameters(tokenID, resourceName, permissionName);
        Token token = generalEJB.fetchToken(tokenID);
        GeneralEJB.validateToken(token);
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + generalEJB.getExpirationPeriod()));
        return requestExclusiveAccess(token, resourceName, permissionName, duration);
    }

    /**
     * The main logic of the request exclusive access.
     * 
     * @param token the token with which the exclusive access is requested
     * @param resourceName the name of the resource for which the exclusive access is requested
     * @param permissionName the name of the permission for which the exclusive access is requested
     * @param duration the duration of exclusive access
     * 
     * @return the exclusive access wrapper
     * 
     * @throws AuthAndAuthException if the the token owner does not have rights to release the exclusive access
     * @throws InvalidResourceException if the resource or permission does not exist
     * @throws DataConsistencyException if multiple tokens or permissions exist or a rule could not be evaluated
     */
    private ExclusiveInfoWrapper requestExclusiveAccess(Token token, String resourceName, String permissionName,
            long duration) throws DataConsistencyException, InvalidResourceException, AuthAndAuthException {

        List<Resource> resources = entityManager.createNamedQuery("Resource.findByName", Resource.class)
                .setParameter("name", resourceName).getResultList();
        if (resources.isEmpty()) {
            throw new InvalidResourceException(token.getUserId(), resourceName, Messages.getString(
                    Messages.NO_RESOURCE, resourceName));
        }

        Resource resource = resources.get(0);

        // Fetch the specified permission.
        List<Permission> permissions = entityManager.createNamedQuery("Permission.findByName", Permission.class)
                .setParameter("name", permissionName).setParameter("resource", resource).getResultList();

        if (permissions.size() == 1) {
            Permission permission = permissions.get(0);

            // Check if user has this permission.
            PermissionsInfo specificInfo = authorizationEJB.hasPermissionsForValidatedParameters(token, resource,
                    new String[] { permissionName }, true).getInfo();
            if (!specificInfo.isPermissionGranted(permission.getName())) {
                throw new AuthAndAuthException(token.getTokenId(), token.getUserId(), Messages.getString(
                        Messages.EXCLUSIVE_ACCESS_REQUEST_NOT_PERMISSION_OWNER, permissionName, resourceName));
            }

            return new ExclusiveInfoWrapper(createExclusiveAccess(token, permission, resource, duration),
                    token.getUserId(), false);
        } else if (permissions.isEmpty()) {
            throw new InvalidResourceException(token.getUserId(), resourceName, Messages.getString(
                    Messages.NO_PERMISSION, permissionName, resourceName));
        } else {
            throw new DataConsistencyException(token.getUserId(), Messages.getString(Messages.MULTIPLE_PERMISSIONS,
                    permissionName, resourceName));
        }
    }

    /**
     * Creates exclusive access for the given permission and resource. The access is bound to the provided token and is
     * valid for the given duration. If duration is 0, default duration is used.
     * 
     * @param token the token for which the exclusive access is requested
     * @param permission the permission for which exclusive access is requested
     * @param resource the resource that owns the permission
     * @param duration the duration for exclusive access
     * 
     * @return the exclusive access info, if successfully created
     * 
     * @throws AuthAndAuthException if exclusive access is taken by someone else
     * @throws InvalidResourceException if exclusive access is not allowed for the permission
     */
    private ExclusiveInfo createExclusiveAccess(Token token, Permission permission, Resource resource, long duration)
            throws AuthAndAuthException, InvalidResourceException {
        // If exclusive access is already active it may only be extended for the user that
        // currently holds it. If it's not active, create a new one.
        String permissionName = permission.getName();
        String resourceName = resource.getName();

        // Exclusive access is not possible.
        if (!permission.isExclusiveAccessAllowed().booleanValue()) {
            throw new InvalidResourceException(token.getUserId(), permissionName + "/exclusive", Messages.getString(
                    Messages.EXCLUSIVE_ACCESS_REQUEST_NOT_ALLOWED, permissionName, resourceName));
        }

        ExclusiveAccess exclusive;
        if (permission.getExclusiveAccess() == null) {
            exclusive = new ExclusiveAccess();
        } else if (permission.getExclusiveAccess().getEndTime().after(new Date())
                && !permission.getExclusiveAccess().getUserId().equals(token.getUserId())) {
            throw new AuthAndAuthException(token.getTokenId(), token.getUserId(), Messages.getString(
                    Messages.EXCLUSIVE_ACCESS_REQUEST_ALREADY_TAKEN, permissionName, resourceName, permission
                            .getExclusiveAccess().getUserId()));
        } else {
            exclusive = permission.getExclusiveAccess();
        }

        exclusive.setUserId(token.getUserId());
        exclusive.setPermission(permission);

        long currentTime = System.currentTimeMillis();
        long dur = duration == 0 ? getExclusiveAccessDuration() : duration;
        Timestamp startTime = new Timestamp(currentTime);
        Timestamp endTime = new Timestamp(currentTime + dur);
        exclusive.setStartTime(startTime);
        exclusive.setEndTime(endTime);

        entityManager.persist(exclusive);

        return new ExclusiveInfo(permissionName, resourceName, exclusive.getEndTime().getTime());
    }

    /**
     * Method first checks, if a token with provided ID exists. It checks if the specified permission has an exclusive
     * access set and if it does, whether the caller's username matches the one for which exclusivity has been set or if
     * the caller has permission to release the exclusive access for other users. If the permission is granted, the
     * exclusive access is released. Otherwise an exception is thrown.
     * 
     * @param tokenID id of the token to be used for user verification
     * @param resourceName name of the resource associated with permissions
     * @param permissionName name of the permission exclusive access has been granted on
     * 
     * @return the exclusive access info if the exclusive access has been successfully cleared for the permission
     * 
     * @throws IllegalRBACArgumentException if any of the required parameters is <code>null</code>
     * @throws AuthAndAuthException if the the token owner does not have rights to release the exclusive access
     * @throws TokenInvalidException if the user is not logged in
     * @throws InvalidResourceException if the resource or permission does not exist
     * @throws DataConsistencyException if multiple tokens or permissions exist or a rule could not be evaluated
     */
    public ExclusiveInfoWrapper releaseExclusiveAccess(String tokenID, String resourceName, String permissionName)
            throws IllegalRBACArgumentException, DataConsistencyException, InvalidResourceException,
            AuthAndAuthException, TokenInvalidException {
        GeneralEJB.verifyInputParameters(tokenID, resourceName, permissionName);
        Token token = generalEJB.fetchToken(tokenID);
        GeneralEJB.validateToken(token);
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + generalEJB.getExpirationPeriod()));
        return releaseExclusiveAccess(token, resourceName, permissionName);
    }

    /**
     * Release exclusive access. This method expects that the token has been validated and that none of the parameters
     * is null or empty.
     * 
     * @param token the token which requested the release
     * @param resourceName the name of the resource
     * @param permissionName the name of the permission to release
     * 
     * @return the exclusive info wrapper if release was successful
     * 
     * @throws DataConsistencyException is more than one matching permission was found or a rule could not be 
     *          evaluated
     * @throws InvalidResourceException if no matching permission or resource were found
     * @throws AuthAndAuthException if the token does not have permission to release exclusive access
     */
    private ExclusiveInfoWrapper releaseExclusiveAccess(Token token, String resourceName, String permissionName)
            throws DataConsistencyException, InvalidResourceException, AuthAndAuthException {
        List<Resource> resources = entityManager.createNamedQuery("Resource.findByName", Resource.class)
                .setParameter("name", resourceName).getResultList();
        if (resources.isEmpty()) {
            throw new InvalidResourceException(token.getUserId(), resourceName, Messages.getString(
                    Messages.NO_RESOURCE, resourceName));
        }

        Resource resource = resources.get(0);
        // Fetch the specified permission.
        List<Permission> permissions = entityManager.createNamedQuery("Permission.findByName", Permission.class)
                .setParameter("name", permissionName).setParameter("resource", resource).getResultList();

        if (permissions.size() == 1) {
            boolean released = checkAndReleaseExclusiveAccess(token, permissions.get(0));
            return new ExclusiveInfoWrapper(new ExclusiveInfo(permissionName, resourceName, -1), token.getUserId(),
                    released);
        } else if (permissions.isEmpty()) {
            throw new InvalidResourceException(token.getUserId(), permissionName, Messages.getString(
                    Messages.NO_PERMISSION, permissionName, resourceName));
        } else {
            throw new DataConsistencyException(token.getUserId(), Messages.getString(Messages.MULTIPLE_PERMISSIONS,
                    permissionName, resourceName));
        }
    }

    /**
     * Checks if exclusive access can be released and if yes, it is released.
     * 
     * @param token the token that releases the exclusive access
     * @param permission the permission for which exclusive access is being released
     * 
     * @return true if the exclusive access was released or false if it didn't exist
     * 
     * @throws InvalidResourceException if the resource with provided name could not be found, or if one of the
     *             permissions could not be found
     * @throws DataConsistencyException if more permissions were found than requested or rule could not be 
     *              evaluated
     * @throws AuthAndAuthException if the token is not allowed to release the exclusive access, because it is taken by
     *             someone else and the token does not have permission to release the access
     */
    private boolean checkAndReleaseExclusiveAccess(Token token, Permission permission) throws InvalidResourceException,
            DataConsistencyException, AuthAndAuthException {
        ExclusiveAccess exclusive = permission.getExclusiveAccess();
        if (exclusive != null) {
            // Check if the token owner has permission RBACManagementStudio#ReleaseAllExclusiveAccess.
            // If yes he is allowed to release exclusive access for anyone, if he doesn't have it,
            // than only allow him to release his own exclusive access
            PermissionsInfo info = authorizationEJB.hasPermissionsForValidatedParameters(token, RESOURCE_NAME,
                    new String[] { RELEASE_EXCLUSIVE_ACCESS }).getInfo();
            if (exclusive.getUserId().equals(token.getUserId()) || info.isPermissionGranted(RELEASE_EXCLUSIVE_ACCESS)) {
                entityManager.remove(exclusive);
                return true;
            } else {
                throw new AuthAndAuthException(token.getTokenId(), token.getUserId(), Messages.getString(
                        Messages.EXCLUSIVE_ACCESS_USER_ID_DONT_MATCH, token.getUserId(), exclusive.getUserId()));
            }
        }
        return false;
    }

    /**
     * Returns the default exclusive action duration. The value is loaded from the configuration parameter. If the
     * parameter does not exist it is created.
     * 
     * @return the duration in milliseconds
     */
    private long getExclusiveAccessDuration() {
        ConfigurationParameter expirationParameter = entityManager.find(ConfigurationParameter.class,
                PARAM_EXLUSIVE_ACCESS_DURATION);
        if (expirationParameter == null) {
            expirationParameter = new ConfigurationParameter();
            expirationParameter.setName(PARAM_EXLUSIVE_ACCESS_DURATION);
            expirationParameter.setValue(String.valueOf(DEFAULT_EXCLUSIVE_ACCESS_DURATION));
            entityManager.persist(expirationParameter);
        }
        return expirationParameter.getValueAsLong();
    }
}
