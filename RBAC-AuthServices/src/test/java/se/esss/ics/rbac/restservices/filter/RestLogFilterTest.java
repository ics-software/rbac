/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.rbac.restservices.filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Unit tests for {@link RestLogFilter} class.
 *
 * @author Lars Johansson
 */
public class RestLogFilterTest {

    /**
     * Test {@link RestLogFilter#replaceTokenID(String)} method.
     */
    @Test
    public void replaceTokenID() {
        String pathInfo1 = "/auth/version";
        String pathInfo2 = "/auth/ControlsDatabase/WriteUnits/users";
        String pathInfo3 = "/auth/token/f0428983-fe7c-3294-8b8f-af4afcd97a10";
        String pathInfo4 = "/auth/token/f0428983-fe7c-3294-8b8f-af4afcd97a10/isvalid";
        String pathInfo5 = "/auth/token/f0428983-fe7c-3294-8b8f-af4afcd97a10/NamingService/Edit,Manage";

        String pathInfoShort = "/auth/token/f0428983-fe7c-3294-8b8f-af4afcd97a1";
        String pathInfoLong  = "/auth/token/f0428983-fe7c-3294-8b8f-af4afcd97a101";

        assertNull(RestLogFilter.replaceTokenID(null));
        assertEquals("", RestLogFilter.replaceTokenID(""));

        assertEquals(pathInfoShort,             RestLogFilter.replaceTokenID(pathInfoShort));
        assertEquals("/auth/token/{tokenID}1",  RestLogFilter.replaceTokenID(pathInfoLong));

        assertEquals(pathInfo1, RestLogFilter.replaceTokenID(pathInfo1));
        assertEquals(pathInfo2, RestLogFilter.replaceTokenID(pathInfo2));
        assertEquals("/auth/token/{tokenID}",                           RestLogFilter.replaceTokenID(pathInfo3));
        assertEquals("/auth/token/{tokenID}/isvalid",                   RestLogFilter.replaceTokenID(pathInfo4));
        assertEquals("/auth/token/{tokenID}/NamingService/Edit,Manage", RestLogFilter.replaceTokenID(pathInfo5));
    }

}
