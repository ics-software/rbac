/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import se.esss.ics.rbac.logic.SignatureEJB;
import se.esss.ics.rbac.logic.exception.IllegalRBACArgumentException;
import se.esss.ics.rbac.logic.exception.RBACException;
import se.esss.ics.rbac.ConfigurationParameter;

/**
 * 
 * <code>SignatureEJBTest</code> tests the methods of the {@link SignatureEJB}. It verifies that the utilities properly
 * verify the signature if the correct data are provided and that they fail verification if incorrect data are provided.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SignatureEJBTest {

    private EntityManager emMock;
    private SignatureEJB signatureEJB;

    @Before
    public void setUp() throws Exception {
        tearDown();
        emMock = mock(EntityManager.class);
        ConfigurationParameter publicKeyPath = new ConfigurationParameter();
        publicKeyPath.setName(SignatureEJB.PARAM_PUBLIC_KEY_PATH);
        publicKeyPath.setValue("public.key");
        ConfigurationParameter privateKeyPath = new ConfigurationParameter();
        privateKeyPath.setName(SignatureEJB.PARAM_PRIVATE_KEY_PATH);
        privateKeyPath.setValue("private.key");
        when(emMock.find(ConfigurationParameter.class, SignatureEJB.PARAM_PUBLIC_KEY_PATH)).thenReturn(publicKeyPath);
        when(emMock.find(ConfigurationParameter.class, SignatureEJB.PARAM_PRIVATE_KEY_PATH)).thenReturn(privateKeyPath);

        signatureEJB = new SignatureEJB();
        Field f = SignatureEJB.class.getDeclaredField("entityManager");
        f.setAccessible(true);
        f.set(signatureEJB, emMock);
    }

    @After
    public void tearDown() {
        File f = new File("public.key");
        if (f.exists()) {
            f.delete();
        }
        f = new File("private.key");
        if (f.exists()) {
            f.delete();
        }
    }

    /**
     * Tests that signature creation and verification works correctly.
     * 
     * @throws RBACException on error
     */
    @Test
    public void testVerification() throws RBACException {
        assertNotNull("Public key should always exist", signatureEJB.getPublicKey());

        byte[] data = new byte[] { 1, 2, 3, 4, 5 };
        byte[] signature = signatureEJB.generateSignature(data);
        assertNotNull("The signature should never be null", signature);
        boolean verification = signatureEJB.verifySignature(data, signature);
        assertTrue("Verification should be successful when using the right data", verification);
        verification = signatureEJB.verifySignature(new byte[] { 1, 2, 3, 4 }, signature);
        assertFalse("Verification should fail if using wrong data", verification);
        byte[] fabricatedSignature = new byte[64];
        for (int i = 0; i < fabricatedSignature.length; i++) {
            fabricatedSignature[i] = (byte) i;
        }
        verification = signatureEJB.verifySignature(data, fabricatedSignature);
        assertFalse("Verification should fail if using wrong signature", verification);

        try {
            verification = signatureEJB.verifySignature(data, new byte[] { 1, 2, 3, 4, 5 });
            fail("Verification should fail if using signature that is not long enough");
        } catch (RBACException e) {
            assertEquals("Cause should be SignatureException", SignatureException.class, e.getCause().getClass());
            assertEquals("The message should match", "Signature length not correct: got 5 but was expecting 64", e
                    .getCause().getMessage());
        }

        try {
            signatureEJB.generateSignature(null);
            fail("It should not be possible to generate a signature for a null array");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Data byte array was not provided.", e.getMessage());
        }

        try {
            signatureEJB.verifySignature(null, signature);
            fail("It should not be possible to verify a signature for a null array");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Data byte array was not provided.", e.getMessage());
        }

        try {
            signatureEJB.verifySignature(data, null);
            fail("It should not be possible to verify null signature");
        } catch (IllegalRBACArgumentException e) {
            assertEquals("Exception expected", "Signature byte array was not provided.", e.getMessage());
        }

        verify(emMock, times(8)).find(ConfigurationParameter.class, SignatureEJB.PARAM_PUBLIC_KEY_PATH);
        verify(emMock, times(7)).find(ConfigurationParameter.class, SignatureEJB.PARAM_PRIVATE_KEY_PATH);

    }

    /**
     * Test the loading of key pair is successful if the key pair exists.
     * 
     * @throws Exception on error
     */
    @Test
    public void testKeyPairHandling() throws Exception {
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(512, new SecureRandom());
        KeyPair keyPair = keyGenerator.genKeyPair();

        PublicKey pubKey = keyPair.getPublic();
        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubKey.getEncoded());
        try (FileOutputStream pubFOS = new FileOutputStream(new File("public.key"))) {
            pubFOS.write(pubKeySpec.getEncoded());
        }

        PrivateKey pvtKey = keyPair.getPrivate();
        PKCS8EncodedKeySpec pvtKeySpec = new PKCS8EncodedKeySpec(pvtKey.getEncoded());
        try (FileOutputStream pvtFOS = new FileOutputStream(new File("private.key"))) {
            pvtFOS.write(pvtKeySpec.getEncoded());
        }

        assertNotNull("Public key should always exist", signatureEJB.getPublicKey());
        assertEquals("Public key should be equal", pubKey, signatureEJB.getPublicKey());
    }

    /**
     * Tests the private and public key are stored correctly if the location is not specified in the persistence
     * context.
     * 
     * @throws Exception on error
     */
    @Test
    public void testKeyPairSave() throws Exception {
        when(emMock.find(ConfigurationParameter.class, SignatureEJB.PARAM_PUBLIC_KEY_PATH)).thenReturn(null);
        when(emMock.find(ConfigurationParameter.class, SignatureEJB.PARAM_PRIVATE_KEY_PATH)).thenReturn(null);
        PublicKey key = signatureEJB.getPublicKey();
        assertNotNull("Public key generated", key);
        File f = new File("public.key");
        assertTrue("Public key saved", f.exists());
        f = new File("private.key");
        assertTrue("Private key saved", f.exists());

        verify(emMock, Mockito.times(2)).persist(Matchers.any(ConfigurationParameter.class));
    }
}
