/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.logic.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import se.esss.ics.rbac.jaxb.TokenInfo;
import se.esss.ics.rbac.logic.TokenUtilities;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.Token;
import se.esss.ics.rbac.datamodel.TokenRole;
import se.esss.ics.rbac.datamodel.UserRole;

/**
 * 
 * <code>TokenUtilitiesTest</code> tests the methods of the {@link TokenUtilities}.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TokenUtilitiesTest {

    /**
     * Test if the token is properly transformed from {@link Token} to {@link TokenInfo}.
     */
    @Test
    public void testFromToken() {
        Token token = new Token();
        token.setIp("192.168.1.1");
        token.setCreationDate(new Timestamp(System.currentTimeMillis()));
        token.setExpirationDate(new Timestamp(System.currentTimeMillis() + 3600000));
        token.setUserId("johndoe");
        token.setTokenId("tokenID");
        Set<TokenRole> roles = new HashSet<>();
        for (int i = 1; i < 4; i++) {
            Role r = new Role();
            r.setDescription("role description");
            r.setName("Role" + i);
            UserRole ur = new UserRole();
            ur.setRole(r);
            ur.setUserId("johndoe");
            roles.add(new TokenRole(token, ur));
        }
        token.setRole(roles);
        TokenInfo info = TokenUtilities.fromToken(token);

        assertEquals("IP should match", token.getIp(), info.getIp());
        assertEquals("Creation date should match", token.getCreationDate().getTime(), info.getCreationTime());
        assertEquals("Expiration should match", token.getExpirationDate().getTime(), info.getExpirationTime());
        assertEquals("Token ID should match", token.getTokenId(), info.getTokenID());
        assertEquals("User ID should match", token.getUserId(), info.getUserID());
        String[] rr = info.getRoleNames().toArray(new String[0]);
        Arrays.sort(rr);
        assertArrayEquals("Role names should match", new String[] { "Role1", "Role2", "Role3" }, rr);
    }

    /**
     * Test if the token id is generated from the correct fields of {@link TokenInfo}.
     */
    @Test
    public void testGenerateTokenID() {
        TokenInfo info = new TokenInfo();
        info.setCreationTime(System.currentTimeMillis());
        info.setExpirationTime(System.currentTimeMillis() + 20000);
        info.setFirstName("John");
        info.setLastName("Doe");
        info.setIp("192.168.1.1");
        info.setUserID("johndoe");
        info.setSignature(new byte[] { 1, 2, 3, 4, 5 });
        info.setRoleNames(Arrays.asList("Role1", "Role2", "Role3"));

        String id1 = TokenUtilities.generateTokenID(info);
        assertNotNull("Id should never be null", id1);

        info.setExpirationTime(info.getExpirationTime() + 60000);
        String id4 = TokenUtilities.generateTokenID(info);
        assertEquals("Expiration time should not affect the ID", id1, id4);

        info.setFirstName("Jane");
        String id2 = TokenUtilities.generateTokenID(info);
        assertEquals("First name should not affect the ID", id1, id2);

        info.setLastName("Doe-Doe");
        String id3 = TokenUtilities.generateTokenID(info);
        assertEquals("Last name should not affect the ID", id1, id3);

        info.setUserID("janedoe");
        String id5 = TokenUtilities.generateTokenID(info);
        assertEquals("Username should not affect the ID", id1, id5);

        info.setCreationTime(info.getCreationTime() - 10000);
        String id6 = TokenUtilities.generateTokenID(info);
        assertEquals("Creation time should not affect ID", id1, id6);

        info.setIp("192.168.1.2");
        String id7 = TokenUtilities.generateTokenID(info);
        assertEquals("IP should not affect the ID", id1, id7);

        info.setRoleNames(Arrays.asList("Role1", "Role"));
        String id8 = TokenUtilities.generateTokenID(info);
        assertEquals("Roles should not affect the ID", id1, id8);

        info.setTokenID("blablabla");
        String id9 = TokenUtilities.generateTokenID(info);
        assertEquals("Token ID should not affect the ID", id1, id9);

        info.setSignature(new byte[] { 1, 56, 123, 4 });
        String id10 = TokenUtilities.generateTokenID(info);
        assertNotEquals("Signature should affect the ID", id1, id10);
    }
}
