/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.general;

/**
 * <code>Constants</code> is a class which defines constants used in management studio.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public final class Constants {

    /** Empty string. */
    public static final String EMPTY_STRING = "";
    /** Persistence context name used throughout the application. */
    public static final String PERSISTENCE_CONTEXT_NAME = "RBAC";
    /** Wildcard character for all user entries. */
    public static final String ALL = "*";
    /** Wildcard character for DB queries. */ 
    public static final String ALL_DB = "%";
    /** Separator used for parsing CSV files. */
    public static final String SPLIT_COMMA = ",";
    /** General format to be used for displaying timestamps. */
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /** Site container ID in the xhtml files. */
    public static final String SITE_CONTAINER = "siteContainer";
    /** Site banner ID in the xhtml files. */
    public static final String SITE_BANNER = "siteBanner";
    /** Site content panel ID in the xhtml files. */
    public static final String SITE_CONTENT = "siteContent";
    /** Site growl ID in the xhtml files. */
    public static final String SITE_CONTAINER_GROWL = "siteContainerGrowl";

    private Constants() {
    }
}
