/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>RoleEJB</code> is a stateless bean containing utility methods for dealing with roles. Contains methods for
 * retrieving, inserting, deleting and updating role informations. All methods are defined in <code>Roles</code>
 * interface.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@Stateless
public class RoleEJB implements Roles {

    private static final long serialVersionUID = 7657251032593466142L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Roles#getRole(int, boolean)
     */
    @Override
    public Role getRole(int roleId, boolean loadASRules) {
        Role role = em.find(Role.class, roleId);
        // collections are lazy loaded
        if (role != null) {
            role.getPermissions().size();
            role.getManagers().size();
            role.getUsers().size();
            if (loadASRules) {
                role.getRules().size();
            }
        }
        return role;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Roles#getRoles()
     */
    @Override
    public List<Role> getRoles() {
        List<Role> roles = em.createNamedQuery("Role.selectAll", Role.class).getResultList();
        for (Role role : roles) {
            // collections are lazy loaded
            role.getPermissions().size();
        }
        return roles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Roles#getRolesByWildcard(java.lang.String)
     */
    @Override
    public List<Role> getRolesByWildcard(String wildcard) {
        return em.createNamedQuery("Role.findByWildcard", Role.class).setParameter("wildcard", wildcard)
                .getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Roles#getRoleByName(java.lang.String)
     */
    @Override
    public Role getRoleByName(String name) {
        List<Role> roles = em.createNamedQuery("Role.findByName", Role.class).setParameter("name", name)
                .getResultList();
        if (roles.isEmpty()) {
            return null;
        }
        Role role = roles.get(0);
        // collections are lazy loaded
        if (role != null) {
            role.getPermissions().size();
            role.getManagers().size();
            role.getUsers().size();
        }
        return role;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Roles#createRole(se.esss.ics.rbac.datamodel.Role)
     */
    @Override
    public void createRole(Role role) {
        em.persist(role);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Roles#removeRole(int)
     */
    @Override
    public void removeRole(int roleId) {
        em.remove(em.find(Role.class, roleId));
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Roles#setRoles(int, java.util.Map)
     */
    @Override
    public void addPermissions(int roleId, Map<Integer, Boolean> permissions) {
        Role role = em.find(Role.class, roleId);
        // only add those permissions, which have changed
        Set<Permission> permissionsSet = new HashSet<>(role.getPermissions());
        for (Entry<Integer, Boolean> entry : permissions.entrySet()) {
            if (entry.getValue().booleanValue()) {
                Permission permission = em.find(Permission.class, entry.getKey());
                if (permission != null) {
                    permissionsSet.add(permission);
                }
            }
        }
        role.setPermissions(permissionsSet);
        em.merge(role);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Roles#containsPermission(int, int)
     */
    @Override
    public boolean containsPermission(int roleId, int permissionId) {
        Role role = em.find(Role.class, roleId);
        Permission permission = em.find(Permission.class, permissionId);
        if (role != null && permission != null) {
            return role.getPermissions().contains(permission);
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.Roles#updateRole(se.esss.ics.rbac.datamodel.Role)
     */
    @Override
    public void updateRole(Role role) {
        em.merge(role);
    }
}
