/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.converters;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>PermissionConverter</code> is a managed bean
 * (<code>permissionConverter</code>) which implements {@link Converter} interface. Permission converter converts
 * between unique permission identifier and <code>Permission</code> object.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "permissionConverter")
@RequestScoped
public class PermissionConverter implements Converter, Serializable {

    private static final long serialVersionUID = 2339436749425058625L;

    @EJB
    private Permissions permissionEJB;

    /*
     * (non-Javadoc)
     *
     * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().isEmpty()) {
            return null;
        } else {
            try {
                return permissionEJB.getPermission(Integer.parseInt(submittedValue));
            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error",
                        "Not a valid permission id."), exception);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || String.valueOf(value).isEmpty()) {
            return Constants.EMPTY_STRING;
        } else {
            return String.valueOf(((Permission) value).getId());
        }
    }
}
