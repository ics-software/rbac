/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.general;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import se.esss.ics.rbac.ejbs.interfaces.MetaInfo;

/**
 * 
 * <code>ManifestBean</code> is a bean that provides the RBAC version number.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@ManagedBean(name = "manifestBean")
public class ManifestBean implements Serializable {

    private static final long serialVersionUID = 7377666376815255135L;

    @EJB
    private MetaInfo manifestEJB;

    /**
     * Returns the management studio version number.
     * 
     * @return management studio version number
     */
    public String getVersion() {
        return manifestEJB.getRBACManagementVersion();
    }
}
