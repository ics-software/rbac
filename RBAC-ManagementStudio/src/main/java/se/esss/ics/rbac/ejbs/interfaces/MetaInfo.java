/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.IOException;
import java.io.Serializable;
import java.util.jar.Manifest;

import javax.ejb.Local;

/**
 * 
 * <code>MetaInfo</code> provides the software meta information.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
@Local
public interface MetaInfo extends Serializable {

    /**
     * Returns the version number of the management studio.
     * 
     * @return the version number
     */
    String getRBACManagementVersion();

    /**
     * Returns the manifest of this application.
     * 
     * @return the manifest
     * @throws IOException if there was an error loading the manifest
     */
    Manifest getManifest() throws IOException;
}
