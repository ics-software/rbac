/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import se.esss.ics.rbac.Util;
import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Rules;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;

/**
 * <code>RulesBean</code> is a managed bean (<code>rulesBean</code>), which contains rules data and methods for
 * executing actions triggered on <code>/Permissions/Rules</code> page. Rules bean is view scoped so it lives as long as
 * user interacting with rules page view.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "rulesBean")
@ViewScoped
public class RulesBean implements Serializable {

    private static final long serialVersionUID = 782770679286061624L;
    private static final String DEFAULT_RULE_DEFINITION = "R";

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private Permissions permissionsEJB;
    @EJB
    private Rules rulesEJB;
    @Inject
    private RulesRequestCacheBean requestCacheBean;

    private String wildcard = Constants.ALL;
    private String newRuleName;
    private String newRuleDescription;
    private String newRuleDefinition;
    private String updatedRuleName;
    private String updatedRuleDescription;
    private String updatedRuleDefinition;
    private Rule selectedRule;
    private Expression selectedExpression;
    private List<Permission> rulePermissions;

    // expressions
    private List<Expression> expressionsSourceList;
    private List<Expression> selectedExpressionsSourceList;
    private List<Expression> expressionsTargetList;
    private List<Expression> selectedExpressionsTargetList;

    /**
     * Initialises expressions dual list model. Method is called post construct.
     */
    @PostConstruct
    private void initialize() {
        initializeExpressionsDualListModel();
    }

    /**
     * Initialise expressions dual list model which is used in add expression dialog on <code>/Permissions/Rules</code>
     * page.
     */
    private void initializeExpressionsDualListModel() {
        expressionsSourceList = requestCacheBean.getExpressions();
        if (expressionsSourceList == null) {
            expressionsSourceList = new ArrayList<>();
        }
        expressionsTargetList = new ArrayList<Expression>();
    }

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     *
     * @param permissionsBean the permissions bean through which the access permissions are checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     *
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     *
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Returns wildcard pattern which is used for searching by rules. Pattern is entered into search field on
     * <code>/Permissions/Rules</code> page.
     *
     * @return wildcard pattern used for searching by rules.
     */
    public String getWildcard() {
        return wildcard;
    }

    /**
     * Sets wildcard pattern which is used for searching by rules. Pattern is entered into search field on
     * <code>/Permissions/Rules</code> page.
     *
     * @param wildcard wildcard pattern used for searching by rules
     */
    public void setWildcard(String wildcard) {
        this.wildcard = wildcard;
    }

    /**
     * Returns rule which is selected in rules list on <code>/Permissions/Rules</code> page.
     *
     * @return rule selected in rules list.
     */
    public Rule getSelectedRule() {
        return selectedRule;
    }

    /**
     * Sets rule which is selected in rules list on <code>/Permissions/Rules</code> page.
     *
     * @param selectedRule rule selected in rules list
     */
    public void setSelectedRule(Rule selectedRule) {
        if (selectedRule != null && !selectedRule.equals(this.selectedRule)) {
            this.selectedRule = selectedRule;
            this.selectedExpression = null;
        }
    }

    /**
     * Returns expression which is selected in expressions table on <code>/Permissions/Rules</code> page.
     *
     * @return expression selected in expressions table.
     */
    public Expression getSelectedExpression() {
        return selectedExpression;
    }

    /**
     * Sets expression which is selected in expressions table on <code>/Permissions/Rules</code> page.
     *
     * @param selectedExpression expression selected in expressions table
     */
    public void setSelectedExpression(Expression selectedExpression) {
        this.selectedExpression = selectedExpression;
    }

    /**
     * @return selected rule name which is shown in rule name field on <code>/Permissions/Rules</code> page.
     */
    public String getRuleName() {
        if (selectedRule != null) {
            return selectedRule.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new rule name which is entered into rule name field in add rule dialog on <code>/Permissions/Rules</code>
     * page.
     *
     * @return new rule name entered into rule name field.
     */
    public String getNewRuleName() {
        return newRuleName;
    }

    /**
     * Sets new rule name which is entered into rule name field in add rule dialog on <code>/Permissions/Rules</code>
     * page.
     *
     * @param newRuleName new rule name entered into rule name field
     */
    public void setNewRuleName(String newRuleName) {
        this.newRuleName = newRuleName;
    }

    /**
     * Returns updated rule name which is entered into rule name field in edit rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @return updated rule name entered into rule name field.
     */
    public String getUpdatedRuleName() {
        return getRuleName();
    }

    /**
     * Sets updated rule name which is entered into rule name field in edit rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @param updatedRuleName updated rule name entered into rule name field
     */
    public void setUpdatedRuleName(String updatedRuleName) {
        this.updatedRuleName = updatedRuleName;
    }

    /**
     * @return selected rule description which is shown in rule description field on <code>/Permissions/Rules</code>
     *         page.
     */
    public String getRuleDescription() {
        if (selectedRule != null) {
            return selectedRule.getDescription();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new rule description which is entered into rule description field in add rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @return new rule description entered into rule description field.
     */
    public String getNewRuleDescription() {
        return newRuleDescription;
    }

    /**
     * Sets new rule description which is entered into rule description field in add rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @param newRuleDescription new rule description entered into rule description field.
     */
    public void setNewRuleDescription(String newRuleDescription) {
        this.newRuleDescription = newRuleDescription;
    }

    /**
     * Returns updated rule description which is entered into rule description field in edit rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @return updated rule description entered into rule description field.
     */
    public String getUpdatedRuleDescription() {
        return getRuleDescription();
    }

    /**
     * Sets updated rule description which is entered into rule description field in edit rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @param updatedRuleDescription updated rule description entered into rule description field
     */
    public void setUpdatedRuleDescription(String updatedRuleDescription) {
        this.updatedRuleDescription = updatedRuleDescription;
    }

    /**
     * @return selected rule definition which is shown in rule definition field on <code>/Permissions/Rules</code> page.
     */
    public String getRuleDefinition() {
        if (selectedRule != null) {
            return selectedRule.getDefinition();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new rule definition which is entered into rule definition field in add rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @return new rule definition entered into rule definition field.
     */
    public String getNewRuleDefinition() {
        return newRuleDefinition;
    }

    /**
     * Sets new rule definition which is entered into rule definition field in add rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @param newRuleDefinition new rule definition entered into rule definition field.
     */
    public void setNewRuleDefinition(String newRuleDefinition) {
        this.newRuleDefinition = newRuleDefinition;
    }

    /**
     * Returns updated rule definition which is entered into rule definition field in edit rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @return updated rule definition entered into rule definition field.
     */
    public String getUpdatedRuleDefinition() {
        return getRuleDefinition();
    }

    /**
     * Sets updated rule definition which is entered into rule definition field in edit rule dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @param updatedRuleDefinition updated rule definition entered into rule definition field.
     */
    public void setUpdatedRuleDefinition(String updatedRuleDefinition) {
        this.updatedRuleDefinition = updatedRuleDefinition;
    }

    /**
     * Retrieves list of rules from database and returns it. Rules are shown in rules list on
     * <code>/Permissions/Rules</code> page.
     *
     * @return list of rules retrieved from database.
     */
    public List<Rule> getRules() {
        List<Rule> rules = fetchRules(false);
        if (!Util.contains(rules, selectedRule)) {
            selectedRule = null;
            selectedExpression = null;
        }
        return rules;
    }

    /**
     * @return list of expressions retrieved from database which corresponds to the selected rule. Expressions are shown
     *         in expressions table on <code>/Permissions/Rules</code> page.
     */
    public List<Expression> getExpressions() {
        if (selectedRule != null) {
            return new ArrayList<>(selectedRule.getExpression());
        }
        return new ArrayList<>();
    }

    /**
     * @return list of permissions retrieved from database which corresponds to the selected rule. Permissions are shown
     *         in permissions table on <code>/Permissions/Rules</code> page.
     */
    public List<Permission> getPermissions() {
        if (selectedRule != null) {
            rulePermissions = requestCacheBean.getPermissionsByRule(selectedRule);
            return rulePermissions;
        }
        return new ArrayList<>();
    }

    /**
     * Method which is called when user selects row in expressions table.
     *
     * @param event select event
     */
    public void onExpressionRowSelect(SelectEvent event) {
        if (event.getObject() != null && event.getObject() instanceof Expression) {
            selectedExpression = (Expression) event.getObject();
        }
    }

    /**
     * Method which is called when user deselects row in expressions table.
     *
     * @param event deselect event
     */
    public void onExpressionRowUnselect(UnselectEvent event) {
        selectedExpression = null;
    }

    /**
     * Creates new rule if user has permission for managing rules. Method is called when OK button, in add rule dialog
     * on <code>/Permissions/Rules</code> page is clicked. Results of the action are logged and shown to the user as
     * growl message.
     */
    public void createRule() {
        if (!permissionsBean.canManageRule(false)) {
            return;
        }
        final Rule rule = new Rule();
        rule.setName(newRuleName);
        if (newRuleDefinition == null || newRuleDefinition.isEmpty()) {
            newRuleDefinition = DEFAULT_RULE_DEFINITION;
        }
        rule.setDefinition(newRuleDefinition);
        rule.setDescription(newRuleDescription);
        rule.setExpression(new HashSet<Expression>());
        String success = Messages.getString(Messages.SUCCESSFUL_RULE_CREATION, loginBean.getUsername(), rule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RULE_CREATION, loginBean.getUsername(),
                rule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rulesEJB.createRule(rule);
                selectedRule = rule;
                fetchRules(true);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes selected rule if user has permission for managing rules. Method is called when remove button on
     * <code>/Permissions/Rule</code> page is clicked. Results of the action are logged and shown to the user as growl
     * message.
     */
    public void removeRule() {
        if (!permissionsBean.canManageRule(false) || isRemoveRuleButtonDisabled()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_RULE_REMOVAL, loginBean.getUsername(),
                selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RULE_REMOVAL, loginBean.getUsername(),
                selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rulesEJB.removeRule(selectedRule);
                if (rulePermissions != null && !rulePermissions.isEmpty()) {
                    List<Permission> updatedPermissions = new ArrayList<>();
                    for (Permission permission : rulePermissions) {
                        permission.setRule(null);
                        updatedPermissions.add(permission);
                    }
                    permissionsEJB.updatePermissions(updatedPermissions);
                }
                selectedRule = null;
                fetchRules(true);
            }
        });
    }

    /**
     * Updates selected rule if user has permission for managing rules. Method is called when OK button, in edit rule
     * dialog on <code>/Permissions/Rules</code> page is clicked. Results of the action are logged and shown to the user
     * as growl message.
     */
    public void updateRule() {
        if (!permissionsBean.canManageRule(false) || isEditRuleInfoButtonDisabled()) {
            return;
        }
        selectedRule.setName(updatedRuleName);
        selectedRule.setDefinition(updatedRuleDefinition);
        selectedRule.setDescription(updatedRuleDescription);
        String success = Messages.getString(Messages.SUCCESSFUL_RULE_UPDATE, loginBean.getUsername(),
                selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RULE_UPDATE, loginBean.getUsername(),
                selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rulesEJB.updateRule(selectedRule);
                fetchRules(true);
            }
        });
    }

    /**
     * Adds new expression to the selected rule if user has permission for managing rules. Method is called when OK
     * button, in add expression dialog on <code>/Permissions/Rules</code> page is clicked. Results of the action are
     * logged and shown to the user as growl message.
     */
    public void addExpressions() {
        if (!permissionsBean.canManageRule(false) || isAddExpressionButtonDisabled()
                || expressionsTargetList.isEmpty()) {
            return;
        }
        Set<Expression> expressions = selectedRule.getExpression();
        expressions.addAll(expressionsTargetList);
        selectedRule.setExpression(new HashSet<>(expressions));
        String success = Messages.getString(Messages.SUCCESSFUL_RULE_EXPRESSIONS_ASSIGNMENT, loginBean.getUsername(),
                selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RULE_EXPRESSIONS_ASSIGNMENT, loginBean.getUsername(),
                selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rulesEJB.updateRule(selectedRule);
            }
        });
        addAllExpressionsFromTargetToSource();
    }

    /**
     * Removes selected expression from selected rule if user has permission for managing rules. Method is called when
     * remove button on <code>/Permissions/Rules</code> page is clicked. Results of the action are logged and shown to
     * the user as growl message.
     */
    public void removeExpression() {
        if (!permissionsBean.canManageRule(false) || isRemoveExpressionButtonDisabled()) {
            return;
        }
        selectedRule.getExpression().remove(selectedExpression);
        String success = Messages.getString(Messages.SUCCESSFUL_RULE_EXPRESSION_REMOVAL, loginBean.getUsername(),
                selectedExpression.getName(), selectedRule.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RULE_EXPRESSION_REMOVAL, loginBean.getUsername(),
                selectedExpression.getName(), selectedRule.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                rulesEJB.updateRule(selectedRule);
                selectedExpression = null;
            }
        });
    }

    /**
     * Remove rule button is disabled if rule is not selected.
     *
     * @return true if remove rule button is disabled, otherwise false.
     */
    public boolean isRemoveRuleButtonDisabled() {
        return selectedRule == null;
    }

    /**
     * Edit rule info button is disabled if rule is not selected.
     *
     * @return true if edit rule info button is disabled, otherwise false.
     */
    public boolean isEditRuleInfoButtonDisabled() {
        return selectedRule == null;
    }

    /**
     * Add expression button is disabled if rule is not selected.
     *
     * @return true if add expression button is disabled otherwise false.
     */
    public boolean isAddExpressionButtonDisabled() {
        return selectedRule == null;
    }

    /**
     * Remove expression button is disabled if rule is not selected and expression is not selected.
     *
     * @return true if remove expression button is disabled, otherwise false.
     */
    public boolean isRemoveExpressionButtonDisabled() {
        return selectedRule == null || selectedExpression == null;
    }

    /**
     * Method that shows dialog for editing rules. Dialog is shown when edit rule button on
     * <code>/Permissions/Rules</code> page is clicked and if user has permissions for managing rules.
     */
    public void showEditRuleDialog() {
        if (!permissionsBean.canManageRule(false) || isEditRuleInfoButtonDisabled()) {
            return;
        }
        PrimeFaces.current().ajax().update("editRuleForm");
        PrimeFaces.current().executeScript("PF('editRuleDialog').show()");
    }

    /**
     * Method that shows dialog for selecting expressions. Dialog is shown when add expression button on
     * <code>/Permissions/Rules</code> page is clicked and if user has permissions for managing rules.
     */
    public void showSelectExpressionDialog() {
        if (!permissionsBean.canManageRule(false) || isAddExpressionButtonDisabled()) {
            return;
        }
        PrimeFaces.current().executeScript("PF('selectExpressionsDialog').show()");
    }

    /**
     * Method that shows dialog for adding rules. Dialog is shown when add rule button on
     * <code>/Permissions/Rules</code> page is clicked and if user has permissions for managing rules.
     */
    public void showAddRuleDialog() {
        if (!permissionsBean.canManageRule(false)) {
            return;
        }
        PrimeFaces.current().executeScript("PF('addRuleDialog').show()");
    }

    /**
     * Resets variables used in add rule dialog on <code>/Permissions/Rules</code> page to the default values.
     */
    public void setDefaultValues() {
        newRuleName = Constants.EMPTY_STRING;
        newRuleDescription = Constants.EMPTY_STRING;
        newRuleDefinition = Constants.EMPTY_STRING;
    }

    /**
     * Returns list which contains expressions from source list and is shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @return list which contains expressions from source list.
     */
    public List<Expression> getExpressionsSourceList() {
        return expressionsSourceList;
    }

    /**
     * Sets list which contains expressions from source list and is shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @param expressionsSourceList list which contains expressions from source list
     */
    public void setExpressionsSourceList(List<Expression> expressionsSourceList) {
        this.expressionsSourceList = expressionsSourceList;
    }

    /**
     * Returns list which contains expressions from target list and is shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @return list which contains expressions from target list.
     */
    public List<Expression> getExpressionsTargetList() {
        return expressionsTargetList;
    }

    /**
     * Sets list which contains expressions from target list and is shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @param expressionsTargetList list which contains expressions from target list
     */
    public void setExpressionsTargetList(List<Expression> expressionsTargetList) {
        this.expressionsTargetList = expressionsTargetList;
    }

    /**
     * Returns list which contains selected expression from source list and is shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @return list which contains selected expressions from source list.
     */
    public List<Expression> getSelectedExpressionsSourceList() {
        return selectedExpressionsSourceList;
    }

    /**
     * Sets list which contains selected expressions from source list and is shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @param selectedExpressionsSourceList list which contains selected expressions from source list
     */
    public void setSelectedExpressionsSourceList(List<Expression> selectedExpressionsSourceList) {
        this.selectedExpressionsSourceList = selectedExpressionsSourceList;
    }

    /**
     * Returns list which contains selected expressions from target list and is shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @return list which contains selected expressions from target list.
     */
    public List<Expression> getSelectedExpressionsTargetList() {
        return selectedExpressionsTargetList;
    }

    /**
     * Sets list which contains selected expressions from target list, and is shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     *
     * @param selectedExpressionsTargetList list which contains selected expressions from target list
     */
    public void setSelectedExpressionsTargetList(List<Expression> selectedExpressionsTargetList) {
        this.selectedExpressionsTargetList = selectedExpressionsTargetList;
    }

    /**
     * Moves selected expressions from source list to the target list. Lists are shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     */
    public void addExpressionsFromSourceToTarget() {
        if (!selectedExpressionsSourceList.isEmpty()) {
            expressionsTargetList.addAll(selectedExpressionsSourceList);
        }
        expressionsSourceList.removeAll(selectedExpressionsSourceList);
        selectedExpressionsSourceList.clear();
    }

    /**
     * Moves selected expressions from target list to the source list. Lists are shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     */
    public void addExpressionsFromTargetToSource() {
        if (!selectedExpressionsTargetList.isEmpty()) {
            expressionsSourceList.addAll(selectedExpressionsTargetList);
        }
        expressionsTargetList.removeAll(selectedExpressionsTargetList);
        selectedExpressionsTargetList.clear();
    }

    /**
     * Moves all expressions from source list to the target list. Lists are shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     */
    public void addAllExpressionsFromSourceToTarget() {
        if (!expressionsSourceList.isEmpty()) {
            expressionsTargetList.addAll(expressionsSourceList);
        }
        expressionsSourceList.clear();
        selectedExpressionsSourceList.clear();
    }

    /**
     * Moves all expressions from target list to the source list. Lists are shown on select expressions dialog on
     * <code>/Permissions/Rules</code> page.
     */
    public void addAllExpressionsFromTargetToSource() {
        if (!expressionsTargetList.isEmpty()) {
            expressionsSourceList.addAll(expressionsTargetList);
        }
        expressionsTargetList.clear();
        selectedExpressionsTargetList.clear();
    }

    /**
     * @param fromDB fetch rules from database, or return cached rules.
     *
     * @return list of rules
     */
    private List<Rule> fetchRules(boolean fromDB) {
        List<Rule> rules = null;
        if (Constants.ALL.equals(wildcard)) {
            rules = requestCacheBean.getRules(fromDB);
        } else {
            rules = requestCacheBean.getRulesByWildcard(wildcard + Constants.ALL_DB, fromDB);
        }
        return rules;
    }
}
