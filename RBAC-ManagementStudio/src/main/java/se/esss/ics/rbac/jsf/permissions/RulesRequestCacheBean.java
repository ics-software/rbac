/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.Expression;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Rule;
import se.esss.ics.rbac.ejbs.interfaces.Expressions;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Rules;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>RulesRequestCacheBean</code> is a request bean used on the rules page. It provides access to DB content by
 * caching the results for the duration of the request. Using this bean eliminates multiple calls to the database, which
 * can be a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "rulesRequestCacheBean")
@RequestScoped
public class RulesRequestCacheBean implements Serializable {

    private static final long serialVersionUID = 7480401365552694531L;

    @EJB
    private Expressions expressionEJB;
    @EJB
    private Permissions permissionsEJB;
    @EJB
    private Rules rulesEJB;

    private String currentWildcard = Constants.ALL_DB;
    private Rule currentRule;

    private List<Rule> rulesList;
    private List<Rule> rulesListWildcard;
    private List<Expression> expressionsList;
    private List<Permission> permissionsList;

    /**
     * @return list of expressions retrieved from database which corresponds to the selected rule. Expressions are shown
     *         in expressions table on <code>/Permissions/Rules</code> page.
     */
    public List<Expression> getExpressions() {
        if (expressionsList == null) {
            expressionsList = expressionEJB.getExpressions();
        }
        return expressionsList;
    }

    /**
     * Retrieves list of rules from database and returns it. Rules are shown in rules list on
     * <code>/Permissions/Rules</code> page.
     * 
     * @param refresh if true retrieves rules from database otherwise return cached rules
     * 
     * @return list of rules retrieved from database.
     */
    public List<Rule> getRules(boolean refresh) {
        if (refresh || rulesList == null) {
            rulesList = rulesEJB.getRules();
        }
        return rulesList;
    }

    /**
     * Retrieves list of rules (which matches wildcard) from database and returns it. Rules are shown in rules list on
     * <code>/Permissions/Rules</code> page.
     * 
     * @param wildcard search rules by wildcard
     * @param refresh if true retrieves rules from database otherwise return cached rules
     * 
     * @return list of rules retrieved from database.
     */
    public List<Rule> getRulesByWildcard(String wildcard, boolean refresh) {
        if (refresh || rulesList == null || !currentWildcard.equals(wildcard)) {
            currentWildcard = wildcard;
            rulesListWildcard = rulesEJB.getRulesByWildcard(wildcard);
        }
        return rulesListWildcard;
    }

    /**
     * @param rule the rule for which the permissions are returned
     * @return list of permissions retrieved from database which corresponds to the selected rule. Permissions are shown
     *         in permissions table on <code>/Permissions/Rules</code> page.
     */
    public List<Permission> getPermissionsByRule(Rule rule) {
        if (permissionsList == null || rule.getId() != currentRule.getId()) {
            currentRule = rule;
            permissionsList = permissionsEJB.getPermissionsByRule(rule);
        }
        return permissionsList;
    }
}
