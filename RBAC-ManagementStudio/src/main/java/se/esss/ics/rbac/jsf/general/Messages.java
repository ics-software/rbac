/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.general;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

/**
 * <code>Messages</code> provides externalisation facilities for all strings related to Management Studio.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
public final class Messages {

    // user roles
    public static final String SUCCESSFUL_ROLE_ASSIGNMENT = "successfulRoleAssignment";
    public static final String UNSUCCESSFUL_ROLE_ASSIGNMENT = "unsuccessfulRoleAssignment";
    public static final String SUCCESSFUL_ROLE_ASSIGNMENTS = "successfulRoleAssignments";
    public static final String UNSUCCESSFUL_ROLE_ASSIGNMENTS = "unsuccessfulRoleAssignments";
    public static final String SUCCESSFUL_ROLE_DELEGATION = "successfulRoleDelegation";
    public static final String UNSUCCESSFUL_ROLE_DELEGATION = "unsuccessfulRoleDelegation";
    public static final String SUCCESSFUL_ROLE_EDIT = "successfulRoleCorrection";
    public static final String UNSUCCESSFUL_ROLE_EDIT = "unsuccessfulRoleCorrection";
    public static final String SUCCESSFUL_ROLE_ASSIGNMENT_REMOVAL = "successfulRoleAssignmentRemoval";
    public static final String UNSUCCESSFUL_ROLE_ASSIGNMENT_REMOVAL = "unsuccessfulRoleAssignmentRemoval";

    // roles
    public static final String SUCCESSFUL_ROLE_CREATION = "successfulRoleCreation";
    public static final String UNSUCCESSFUL_ROLE_CREATION = "unsuccessfulRoleCreation";
    public static final String SUCCESSFUL_ROLE_UPDATE = "successfulRoleUpdate";
    public static final String UNSUCCESSFUL_ROLE_UPDATE = "unsuccessfulRoleUpdate";
    public static final String SUCCESSFUL_ROLE_REMOVAL = "successfulRoleRemoval";
    public static final String UNSUCCESSFUL_ROLE_REMOVAL = "unsuccessfulRoleRemoval";
    public static final String SUCCESSFUL_ROLES_PERMISSION_ASSIGNMENT = "successfulRolesPermissionAssignment";
    public static final String UNSUCCESSFUL_ROLES_PERMISSION_ASSIGNMENT = "unsuccessfulRolesPermissionAssignment";
    public static final String SUCCESSFUL_ROLE_PERMISSIONS_ASSIGNMENT = "successfulRolePermissionsAssignment";
    public static final String UNSUCCESSFUL_ROLE_PERMISSIONS_ASSIGNMENT = "unsuccessfulRolePermissionsAssignment";
    public static final String SUCCESSFUL_ROLE_PERMISSION_REMOVAL = "successfulRolePermissionRemoval";
    public static final String UNSUCCESSFUL_ROLE_PERMISSION_REMOVAL = "unsuccessfulRolePermissionRemoval";
    public static final String SUCCESSFUL_ROLE_MANAGERS_ASSIGNMENT = "successfulRoleManagersAssignment";
    public static final String UNSUCCESSFUL_ROLE_MANAGERS_ASSIGNMENT = "unsuccessfulRoleManagersAssignment";
    public static final String SUCCESSFUL_ROLE_MANAGER_REMOVAL = "successfulRoleManagerRemoval";
    public static final String UNSUCCESSFUL_ROLE_MANAGER_REMOVAL = "unsuccessfulRoleManagerRemoval";

    // permissions
    public static final String SUCCESSFUL_PERMISSION_CREATION = "successfulPermissionCreation";
    public static final String UNSUCCESSFUL_PERMISSION_CREATION = "unsuccessfulPermissionCreation";
    public static final String SUCCESSFUL_PERMISSIONS_CREATION = "successfulPermissionsCreation";
    public static final String UNSUCCESSFUL_PERMISSIONS_CREATION = "unsuccessfulPermissionsCreation";
    public static final String SUCCESSFUL_PERMISSION_REMOVAL = "successfulPermissionRemoval";
    public static final String UNSUCCESSFUL_PERMISSION_REMOVAL = "unsuccessfulPermissionRemoval";
    public static final String SUCCESSFUL_PERMISSION_UPDATE = "successfulPermissionUpdate";
    public static final String UNSUCCESSFUL_PERMISSION_UPDATE = "unsuccessfulPermissionUpdate";

    // rules
    public static final String SUCCESSFUL_RULE_CREATION = "successfulRuleCreation";
    public static final String UNSUCCESSFUL_RULE_CREATION = "unsuccessfulRuleCreation";
    public static final String SUCCESSFUL_RULE_REMOVAL = "successfulRuleRemoval";
    public static final String UNSUCCESSFUL_RULE_REMOVAL = "unsuccessfulRuleRemoval";
    public static final String SUCCESSFUL_RULE_UPDATE = "successfulRuleUpdate";
    public static final String UNSUCCESSFUL_RULE_UPDATE = "unsuccessfulRuleUpdate";
    public static final String SUCCESSFUL_RULE_EXPRESSIONS_ASSIGNMENT = "successfulRuleExpressionsAssignment";
    public static final String UNSUCCESSFUL_RULE_EXPRESSIONS_ASSIGNMENT = "unsuccessfulRuleExpressionsAssignment";
    public static final String SUCCESSFUL_RULE_EXPRESSION_REMOVAL = "successfulRuleExpressionRemoval";
    public static final String UNSUCCESSFUL_RULE_EXPRESSION_REMOVAL = "unsuccessfulRuleExpressionRemoval";

    // expressions
    public static final String SUCCESSFUL_EXPRESSION_CREATION = "successfulExpressionCreation";
    public static final String UNSUCCESSFUL_EXPRESSION_CREATION = "unsuccessfulExpressionCreation";
    public static final String SUCCESSFUL_EXPRESSION_REMOVAL = "successfulExpressionRemoval";
    public static final String UNSUCCESSFUL_EXPRESSION_REMOVAL = "unsuccessfulExpressionRemoval";
    public static final String SUCCESSFUL_EXPRESSION_UPDATE = "successfulExpressionUpdate";
    public static final String UNSUCCESSFUL_EXPRESSION_UPDATE = "unsuccessfulExpressionUpdate";

    // ip groups
    public static final String SUCCESSFUL_IPGROUP_CREATION = "successfulIPGroupCreation";
    public static final String UNSUCCESSFUL_IPGROUP_CREATION = "unsuccessfulIPGroupCreation";
    public static final String SUCCESSFUL_IPGROUP_REMOVAL = "successfulIPGroupRemoval";
    public static final String UNSUCCESSFUL_IPGROUP_REMOVAL = "unsuccessfulIPGroupRemoval";
    public static final String SUCCESSFUL_IPGROUP_UPDATE = "successfulIPGroupUpdate";
    public static final String UNSUCCESSFUL_IPGROUP_UPDATE = "unsuccessfulIPGroupUpdate";
    public static final String SUCCESSFUL_IPGROUP_MEMBER_ASSIGNMENT = "successfulIPGroupMemberAssignment";
    public static final String UNSUCCESSFUL_IPGROUP_MEMBER_ASSIGNMENT = "unsuccessfulIPGroupMemberAssignment";
    public static final String SUCCESSFUL_IPGROUP_MEMBER_REMOVAL = "successfulIPGroupMemberRemoval";
    public static final String UNSUCCESSFUL_IPGROUP_MEMBER_REMOVAL = "unsuccessfulIPGroupMemberRemoval";

    // exclusive accesses
    public static final String SUCCESSFUL_EA_REQUEST = "successfulEARequest";
    public static final String UNSUCCESSFUL_EA_REQUEST = "unsuccessfulEARequest";
    public static final String SUCCESSFUL_EA_RELEASE = "successfulEARelease";
    public static final String UNSUCCESSFUL_EA_RELEASE = "unsuccessfulEARelease";
    public static final String EXCLUSIVE_ACCESS_ALLOWED = "exclusiveAccessAllowed";
    public static final String EXCLUSIVE_ACCESS_NOT_ALLOWED = "exclusiveAccessNotAllowed";

    // overview
    public static final String SUCCESSFUL_FILTERED_PERMISSIONS_RETRIEVAL = "successfulFilteredPermissionsRetrieval";
    public static final String UNSUCCESSFUL_FILTERED_PERMISSIONS_RETRIEVAL = "unsuccessfulFilteredPermissionsRetrieval";

    // resources
    public static final String SUCCESSFUL_RESOURCE_CREATION = "successfulResourceCreation";
    public static final String UNSUCCESSFUL_RESOURCE_CREATION = "unsuccessfulResourceCreation";
    public static final String SUCCESSFUL_RESOURCE_REMOVAL = "successfulResourceRemoval";
    public static final String UNSUCCESSFUL_RESOURCE_REMOVAL = "unsuccessfulResourceRemoval";
    public static final String SUCCESSFUL_RESOURCE_UPDATE = "successfulResourceUpdate";
    public static final String UNSUCCESSFUL_RESOURCE_UPDATE = "unsuccessfulResourceUpdate";
    public static final String SUCCESSFUL_RESOURCE_MANAGERS_ASSIGNMENT = "successfulResourceManagersAssignment";
    public static final String UNSUCCESSFUL_RESOURCE_MANAGERS_ASSIGNMENT = "unsuccessfulResourceManagersAssignment";
    public static final String SUCCESSFUL_RESOURCE_MANAGER_REMOVAL = "successfulResourceManagerRemoval";
    public static final String UNSUCCESSFUL_RESOURCE_MANAGER_REMOVAL = "unsuccessfulResourceManagerRemoval";

    // tokens
    public static final String SUCCESSFUL_TOKEN_INVALIDATION = "successfulTokenInvalidation";
    public static final String UNSUCCESSFUL_TOKEN_INVALIDATION = "unsuccessfulTokenInvalidation";
    public static final String SUCCESSFUL_TOKENS_INVALIDATION = "successfulTokensInvalidation";
    public static final String UNSUCCESSFUL_TOKENS_INVALIDATION = "unsuccessfulTokensInvalidation";
    public static final String SUCCESSFUL_TOKENS_REMOVAL = "successfulTokensRemoval";
    public static final String UNSUCCESSFUL_TOKENS_REMOVAL = "unsuccessfulTokensRemoval";

    // PV Access Management
    public static final String SUCCESSFUL_ASG_CREATION = "successfulASGCreation";
    public static final String UNSUCCESSFUL_ASG_CREATION = "unsuccessfulASGCreation";
    public static final String SUCCESSFUL_ASG_REMOVAL = "successfulASGRemoval";
    public static final String UNSUCCESSFUL_ASG_REMOVAL = "unsuccessfulASGRemoval";
    public static final String SUCCESSFUL_ASG_UPDATE = "successfulASGUpdate";
    public static final String UNSUCCESSFUL_ASG_UPDATE = "unsuccessfulASGUpdate";
    public static final String SUCCESSFUL_ASG_INPUT_CREATION = "successfulASGInputCreation";
    public static final String UNSUCCESSFUL_ASG_INPUT_CREATION = "unsuccessfulASGInputCreation";
    public static final String SUCCESSFUL_ASG_INPUT_REMOVAL = "successfulASGInputRemoval";
    public static final String UNSUCCESSFUL_ASG_INPUT_REMOVAL = "unsuccessfulASGInputRemoval";
    public static final String SUCCESSFUL_ASG_INPUTS_UPDATE = "successfulASGInputsUpdate";
    public static final String UNSUCCESSFUL_ASG_INPUTS_UPDATE = "unsuccessfulASGInputsUpdate";
    public static final String SUCCESSFUL_ASG_RULE_CREATION = "successfulASGRuleCreation";
    public static final String UNSUCCESSFUL_ASG_RULE_CREATION = "unsuccessfulASGRuleCreation";
    public static final String SUCCESSFUL_ASG_RULE_REMOVAL = "successfulASGRuleRemoval";
    public static final String UNSUCCESSFUL_ASG_RULE_REMOVAL = "unsuccessfulASGRuleRemoval";
    public static final String SUCCESSFUL_ASG_RULE_UPDATE = "successfulASGRuleUpdate";
    public static final String UNSUCCESSFUL_ASG_RULE_UPDATE = "unsuccessfulASGRuleUpdate";
    public static final String SUCCESSFUL_ASG_RULE_ROLE_ASSIGNMENT = "successfulASGRuleRoleAssignment";
    public static final String UNSUCCESSFUL_ASG_RULE_ROLE_ASSIGNMENT = "unsuccessfulASGRuleRoleAssignment";
    public static final String SUCCESSFUL_ASG_RULE_ROLE_REMOVAL = "successfulASGRuleRoleRemoval";
    public static final String UNSUCCESSFUL_ASG_RULE_ROLE_REMOVAL = "unsuccessfulASGRuleRoleRemoval";
    public static final String SUCCESSFUL_ASG_RULE_IPGROUP_ASSIGNMENT = "successfulASGRuleRoleIPGroupAssignment";
    public static final String UNSUCCESSFUL_ASG_RULE_IPGROUP_ASSIGNMENT = "unsuccessfulASGRuleRoleIPGroupAssignment";
    public static final String SUCCESSFUL_ASG_RULE_IPGROUP_REMOVAL = "successfulASGRuleRoleIPGroupRemoval";
    public static final String UNSUCCESSFUL_ASG_RULE_IPGROUP_REMOVAL = "unsuccessfulASGRuleRoleIPGroupRemoval";
    public static final String SUCCESSFUL_LOGS_RETRIEVAL = "successfulLogsRetrieval";
    public static final String UNSUCCESSFUL_LOGS_RETRIEVAL = "unsuccessfulLogsRetrieval";
    public static final String UNSUCCESSFUL_INPUT_CREATION_MAX_INDEX = "unsuccessfulInputCreationMaxIndex";

    // export & import
    public static final String SUCCESSFUL_PERMISSIONS_IMPORT = "successfulPermissionsImport";
    public static final String UNSUCCESSFUL_PERMISSIONS_IMPORT = "unsuccessfulPermissionsImport";
    public static final String SUCCESSFUL_PERMISSION_EXPORT = "successfulPermissionExport";
    public static final String UNSUCCESSFUL_PERMISSION_EXPORT = "unsuccessfulPermissionExport";

    // admin settings
    public static final String SUCCESSFUL_DEFAULT_PERMISSIONS_INSTALL = "successfulDefaultPermissionsInstall";
    public static final String UNSUCCESSFUL_DEFAULT_PERMISSIONS_INSTALL = "unsuccessfulDefaultPermissionsInstall";
    public static final String SUCCESSFUL_TOKEN_EXPIRATION_PERIOD_CHANGE = "successfulTokenExpirationPeriodChange";
    public static final String UNSUCCESSFUL_TOKEN_EXPIRATION_PERIOD_CHANGE = "unsuccessfulTokenExpirationPeriodChange";
    public static final String SUCCESSFUL_EA_DURATION_PERIOD_CHANGE = "successfulEADurationPeriodChange";
    public static final String UNSUCCESSFUL_EA_DURATION_PERIOD_CHANGE = "unsuccessfulEADurationPeriodChange";
    public static final String SUCCESSFUL_KEYS_REGENERATE = "successfulKeysRegenerate";
    public static final String UNSUCCESSFUL_KEYS_REGENERATE = "unsuccessfulKeysRegenerate";
    public static final String SUCCESSFUL_DEFAULT_ASG_PERMISSION_CHANGE = "successfulDefaultASGPermissionChange";
    public static final String UNSUCCESSFUL_DEFAULT_ASG_PERMISSION_CHANGE = "unsuccessfulDefaultASGPermissionChange";
    public static final String SUCCESSFUL_ADMIN_PASSWORD_CHANGE = "successfulAdminPasswordChange";
    public static final String UNSUCCESSFUL_ADMIN_PASSWORD_CHANGE = "unsuccessfullAdminPasswordChange";
    public static final String REDIRECT_ERROR = "redirectError";

    // others
    public static final String FILTER_WARN = "filterWarn";
    public static final String LDAP_EXCEPTION = "ldapException";
    public static final String INVALID_DATES = "invalidDates";
    public static final String EMAIL_WARNING = "emailWarn";
    public static final String EA_RESET_EXCEPTION = "eaResetException";
    public static final String TARGET_LIST_CONTAINS_PERMISSION = "targetListContainsPermissionWarn";
    public static final String RESOURCE_CONTAINS_PERMISSION = "resourceContainsPermissionWarn";
    public static final String LOGIN_EXCEPTION = "loginException";
    public static final String FILE_GENERATING_EXCEPTION = "fileGeneratingException";
    public static final String ADMIN_PASSWORD_EXCEPTION = "adminPasswordException";
    public static final String KEYS_RETRIEVING_EXCEPTION = "keysRetrievingException";
    public static final String EMPTY_FIELDS_EXCEPTION = "emptyFieldsException";

    public static final String ACF_FILE_GENERATION_PROBLEM = "acfFileGenerationProblem";
    public static final String IMPORT_IO_EXCEPTION = "importIOException";
    public static final String EXPORT_IO_EXCEPTION = "exportIOException";

    private static final Properties MESSAGES = new Properties();
    static {
        try (InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("management_studio_messages.properties")) {
            MESSAGES.load(stream);
        } catch (IOException e) {
            // ignore
        }
    }

    /**
     * Returns the text which belongs to the given key. The text is then formatted with the given attributes. Each tag
     * {n} is replaced by the attribute under the specified index.
     * 
     * @param key the key
     * @param attributes the attributes
     * @return the text (if not defined, the bracketed key is returned)
     */
    public static String getString(String key, Object... attributes) {
        String text = getString(key);
        try {
            return MessageFormat.format(text, attributes);
        } catch (IllegalArgumentException e) {
            return '<' + key + '>';
        }
    }

    /**
     * Returns the text which belongs to the given key.
     * 
     * @param key the key
     * @return the text loaded from the properties file
     */
    public static String getString(String key) {
        String s = MESSAGES.getProperty(key);
        return (s == null) ? '<' + key + '>' : s;
    }

    private Messages() {
    }
}
