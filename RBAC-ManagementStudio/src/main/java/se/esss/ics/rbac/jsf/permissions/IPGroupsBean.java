/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.permissions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import se.esss.ics.rbac.Util;
import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.ejbs.interfaces.IPGroups;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;

/**
 * <code>IPGroupsBean</code> is a managed bean (<code>ipGroupsBean</code>), which contains IP groups data and methods
 * for executing actions triggered on <code>/Permissions/IPGroups</code> page. IP groups bean is view scoped so it lives
 * as long as user interacting with IP groups page view.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "ipGroupsBean")
@ViewScoped
public class IPGroupsBean implements Serializable {

    private static final long serialVersionUID = -4606470694487210438L;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private IPGroups ipGroupsEJB;
    @Inject
    private IPGroupsRequestCacheBean requestCacheBean;

    private String wildcard = Constants.ALL;
    private String newIpGroupName;
    private String newIpGroupDescription;
    private String updatedIpGroupName;
    private String updatedIpGroupDescription;
    private String ipGroupMember;
    private IPGroup selectedIPGroup;
    private String selectedMember;

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     *
     * @param permissionsBean the permission bean through which the permissions can be checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     *
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     *
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Returns wildcard pattern which is used for searching by IP groups. Pattern is entered into search field on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @return wildcard pattern used for searching by IP groups.
     */
    public String getWildcard() {
        return wildcard;
    }

    /**
     * Sets wildcard pattern which is used for searching by IP groups. Pattern is entered into search field on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @param wildcard wildcard pattern used for searching by IP groups
     */
    public void setWildcard(String wildcard) {
        this.wildcard = wildcard;
    }

    /**
     * Returns IP group which is selected in IP groups list on <code>/Permissions/IPGroups</code> page.
     *
     * @return selected IP group.
     */
    public IPGroup getSelectedIPGroup() {
        return selectedIPGroup;
    }

    /**
     * Sets IP group which is selected in IP groups list on <code>/Permissions/IPGroups</code> page.
     *
     * @param selectedIPGroup selected IP group
     */
    public void setSelectedIPGroup(IPGroup selectedIPGroup) {
        if (selectedIPGroup != null && !selectedIPGroup.equals(this.selectedIPGroup)) {
            this.selectedIPGroup = selectedIPGroup;
            this.selectedMember = Constants.EMPTY_STRING;
        }
    }

    /**
     * Returns IP group member which is selected in members table on <code>/Permissions/IPGroups</code> page.
     *
     * @return selected IP group member.
     */
    public String getSelectedMember() {
        return selectedMember;
    }

    /**
     * Sets IP group member which is selected in members table on <code>/Permissions/IPGroups</code> page.
     *
     * @param selectedMember selected IP group member
     */
    public void setSelectedMember(String selectedMember) {
        this.selectedMember = selectedMember;
    }

    /**
     * Returns IP group member value which is entered into member field in add IP group member dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @return IP group member entered into member field.
     */
    public String getIPGroupMember() {
        return ipGroupMember;
    }

    /**
     * Sets IP group member value which is entered into member field in add IP group member dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @param ipGroupMember IP group member
     */
    public void setIPGroupMember(String ipGroupMember) {
        this.ipGroupMember = removeSpaces(ipGroupMember);
    }

    /**
     * @return selected IP group name which is shown in IP group name field on <code>/Permissions/IPGroups</code> page.
     */
    public String getIPGroupName() {
        if (isIPGroupSelected()) {
            return selectedIPGroup.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new IP group name which is entered into IP group name field in add IP group dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @return new IP group name which is entered into IP group name field.
     */
    public String getNewIPGroupName() {
        return newIpGroupName;
    }

    /**
     * Sets new IP group name which is entered into IP group name field in add IP group dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @param newIpGroupName new IP group name
     */
    public void setNewIPGroupName(String newIpGroupName) {
        this.newIpGroupName = removeSpaces(newIpGroupName);
    }

    /**
     * Returns updated IP group name which is entered into IP group name field in edit IP group dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @return updated IP group name which is entered into IP group name field.
     */
    public String getUpdatedIPGroupName() {
        return getIPGroupName();
    }

    /**
     * Sets updated IP group name which is entered into IP group name field in edit IP group dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @param updatedIpGroupName updated IP group name
     */
    public void setUpdatedIPGroupName(String updatedIpGroupName) {
        this.updatedIpGroupName = removeSpaces(updatedIpGroupName);
    }

    /**
     * @return selected IP group description which is shown in IP group description field on
     *         <code>/Permissions/IPGroups</code> page.
     */
    public String getIPGroupDescription() {
        if (isIPGroupSelected()) {
            return selectedIPGroup.getDescription();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new IP group description which is entered into IP group description field in add IP group dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @return new IP group description which is entered into IP group description field.
     */
    public String getNewIPGroupDescription() {
        return newIpGroupDescription;
    }

    /**
     * Sets new IP group description which is entered into IP group description field in add IP group dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @param newIpGroupDescription new IP group description
     */
    public void setNewIPGroupDescription(String newIpGroupDescription) {
        this.newIpGroupDescription = newIpGroupDescription;
    }

    /**
     * Returns updated IP group description which is entered into IP group description field in edit IP group dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @return updated IP group description which is entered into IP group description field.
     */
    public String getUpdatedIPGroupDescription() {
        return getIPGroupDescription();
    }

    /**
     * Sets updated IP group description which is entered into IP group description field in edit IP group dialog on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @param updatedIpGroupDescription updated IP group description
     */
    public void setUpdatedIPGroupDescription(String updatedIpGroupDescription) {
        this.updatedIpGroupDescription = updatedIpGroupDescription;
    }

    /**
     * Retrieves list of IP groups from database and returns it. IP groups are shown in IP groups list on
     * <code>/Permissions/IPGroups</code> page.
     *
     * @return list of IP groups retrieved from database.
     */
    public List<IPGroup> getIPGroups() {
        List<IPGroup> ipGroups = fetchIPGroups(false);
        if (!Util.contains(ipGroups, selectedIPGroup)) {
            selectedIPGroup = null;
            selectedMember = Constants.EMPTY_STRING;
        }
        return ipGroups;
    }

    /**
     * Returns list of members corresponds to the selected IP group.
     *
     * @return selected IP group members.
     */
    public List<String> getMembers() {
        if (isIPGroupSelected()) {
            List<String> list = new ArrayList<>(selectedIPGroup.getIp());
            Collections.sort(list);
            return list;
        }
        return new ArrayList<>();
    }

    /**
     * Method which is called when user selects row in IP groups member table.
     *
     * @param event select event
     */
    public void onMemberRowSelect(SelectEvent event) {
        if (event.getObject() != null && event.getObject() instanceof String) {
            selectedMember = (String) event.getObject();
        }
    }

    /**
     * Method which is called when user deselects row in IP groups member table.
     *
     * @param event deselect event
     */
    public void onMemberRowUnselect(UnselectEvent event) {
        selectedMember = null;
    }

    /**
     * @return true if IP group is selected, otherwise false.
     */
    public boolean isIPGroupSelected() {
        return selectedIPGroup != null;
    }

    /**
     * @return true if IP group member is selected, otherwise false.
     */
    public boolean isMemberSelected() {
        return isIPGroupSelected() && selectedMember != null && !selectedMember.isEmpty();
    }

    /**
     * Creates new IP group if user has permission for managing rules. Method is called when OK button in add IP group
     * dialog on <code>/Permissions/IPGroups</code> page is clicked. Results of the action are logged and shown to the
     * user as growl message.
     */
    public void createIPGroup() {
        if (!permissionsBean.canManageRule(false)) {
            return;
        }
        final IPGroup ipGroup = new IPGroup();
        ipGroup.setName(newIpGroupName);
        ipGroup.setDescription(newIpGroupDescription);
        ipGroup.setIp(new HashSet<String>());
        String success = Messages.getString(Messages.SUCCESSFUL_IPGROUP_CREATION, loginBean.getUsername(),
                ipGroup.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_IPGROUP_CREATION, loginBean.getUsername(),
                ipGroup.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                ipGroupsEJB.createIPGroup(ipGroup);
                selectedIPGroup = ipGroup;
                fetchIPGroups(true);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes selected IP group if user has permission for managing rules. Method is called when remove button on
     * <code>/Permissions/IPGroups</code> page is clicked. Results of the action are logged and shown to the user as
     * growl message.
     */
    public void removeIPGroup() {
        if (!permissionsBean.canManageRule(false) || !isIPGroupSelected()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_IPGROUP_REMOVAL, loginBean.getUsername(),
                selectedIPGroup.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_IPGROUP_REMOVAL, loginBean.getUsername(),
                selectedIPGroup.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                ipGroupsEJB.removeIPGroup(selectedIPGroup);
                selectedIPGroup = null;
                fetchIPGroups(true);
            }
        });
    }

    /**
     * Updates selected IP group if user has permission for managing rules. Method is called when OK button, in edit IP
     * group dialog on <code>/Permissions/IPGroups</code> page is clicked. Results of the action are logged and shown to
     * the user as growl message.
     */
    public void updateIPGroup() {
        if (!permissionsBean.canManageRule(false) || !isIPGroupSelected()) {
            return;
        }
        selectedIPGroup.setName(updatedIpGroupName);
        selectedIPGroup.setDescription(updatedIpGroupDescription);
        String success = Messages.getString(Messages.SUCCESSFUL_IPGROUP_UPDATE, loginBean.getUsername(),
                selectedIPGroup.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_IPGROUP_UPDATE, loginBean.getUsername(),
                selectedIPGroup.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                ipGroupsEJB.updateIPGroup(selectedIPGroup);
                fetchIPGroups(true);
            }
        });
    }

    /**
     * Creates new IP group member if user has permission for managing rules. Method is called when OK button in add IP
     * group member dialog on <code>/Permissions/IPGroups</code> page is clicked. Results of the action are logged and
     * shown to the user as growl message.
     */
    public void createIPGroupMember() {
        if (!permissionsBean.canManageRule(false) || !isIPGroupSelected()) {
            return;
        }
        Set<String> members = new HashSet<>(selectedIPGroup.getIp());
        members.add(ipGroupMember);
        selectedIPGroup.setIp(members);
        String success = Messages.getString(Messages.SUCCESSFUL_IPGROUP_MEMBER_ASSIGNMENT, loginBean.getUsername(),
                ipGroupMember, selectedIPGroup.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_IPGROUP_MEMBER_ASSIGNMENT, loginBean.getUsername(),
                ipGroupMember, selectedIPGroup.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                ipGroupsEJB.updateIPGroup(selectedIPGroup);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes IP group member if user has permission for managing rules. Method is called when remove button on
     * <code>/Permissions/IPGroups</code> page is clicked. Results of the action are logged and shown to the user as
     * growl message.
     */
    public void removeMember() {
        if (!permissionsBean.canManageRule(false) || !isMemberSelected()) {
            return;
        }
        selectedIPGroup.getIp().remove(selectedMember);
        String success = Messages.getString(Messages.SUCCESSFUL_IPGROUP_MEMBER_REMOVAL, loginBean.getUsername(),
                selectedMember, selectedIPGroup.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_IPGROUP_MEMBER_REMOVAL, loginBean.getUsername(),
                selectedMember, selectedIPGroup.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                ipGroupsEJB.updateIPGroup(selectedIPGroup);
                selectedMember = null;
            }
        });
    }

    /**
     * Removes spaces from given IP group name.
     *
     * @param ipGroupName IP group name
     *
     * @return ipGroupName IP group name without spaces.
     */
    private static String removeSpaces(String ipGroupName) {
        return ipGroupName.replaceAll("\\p{Z}", Constants.EMPTY_STRING);
    }

    /**
     * Sets values used in add IP group dialog on <code>/Permissions/IPGroups</code> page to the default values.
     */
    public void setDefaultValues() {
        newIpGroupName = Constants.EMPTY_STRING;
        newIpGroupDescription = Constants.EMPTY_STRING;
        ipGroupMember = Constants.EMPTY_STRING;
    }

    /**
     * Shows the dialog to create a new IP group.
     */
    public void showAddIPGroupDialog() {
        if (!permissionsBean.canManageRule(true)) {
            return;
        }
        PrimeFaces.current().executeScript("PF('addIPGroupDialog').show()");
    }

    /**
     * Shows the dialog to edit an IP group.
     */
    public void showEditIPGroupDialog() {
        if (!isIPGroupSelected() || !permissionsBean.canManageRule(true)) {
            return;
        }
        PrimeFaces.current().ajax().update("editIPGroupForm");
        PrimeFaces.current().executeScript("PF('editIPGroupDialog').show()");
    }

    /**
     * Shows the dialog to edit an IP group.
     */
    public void showAddIPGroupMemberDialog() {
        if (!isIPGroupSelected() || !permissionsBean.canManageRule(true)) {
            return;
        }
        PrimeFaces.current().executeScript("PF('addIPGroupMemberDialog').show()");
    }

    /**
     * @param fromDB fetch ip groups from database, or return cached ip groups.
     *
     * @return list of ip groups
     */
    private List<IPGroup> fetchIPGroups(boolean fromDB) {
        List<IPGroup> ipGroups = null;
        if (Constants.ALL.equals(wildcard)) {
            ipGroups = requestCacheBean.getIPGroups(fromDB);
        } else {
            ipGroups = requestCacheBean.getIPGroupsByWildcard(wildcard + Constants.ALL_DB, fromDB);
        }
        return ipGroups;
    }
}
