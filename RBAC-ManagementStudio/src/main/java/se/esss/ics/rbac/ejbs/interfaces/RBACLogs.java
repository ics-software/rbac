/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.RBACLog;
import se.esss.ics.rbac.RBACLog.Action;
import se.esss.ics.rbac.RBACLog.Severity;

/**
 * <code>RBACLogs</code> interface defines methods for dealing with RBAC log entries.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface RBACLogs extends Serializable {

    /**
     * Retrieves RBAC log entries which corresponds to the given parameters from database.
     * 
     * @param startTime start time
     * @param endTime end time
     * @param actions types of the log actions
     * @param severities severities of the log entries
     * @param users unique users identifiers that initiated logged actions
     * 
     * @return retrieved log entries which corresponds to the given parameters.
     */
    List<RBACLog> getLogs(Date startTime, Date endTime, Action[] actions, Severity[] severities, List<String> users);
    
    /**
     * Retrieves RBAC log entries which corresponds to the given parameters from database.
     * 
     * @param startTime start time
     * @param endTime end time
     * @param actions types of the log actions
     * @param severities severities of the log entries
     * 
     * @return retrieved log entries which corresponds to the given parameters.
     */
    List<RBACLog> getLogs(Date startTime, Date endTime, Action[] actions, Severity[] severities);
}
