/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.resources;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.Util;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.dsaccess.DirectoryServiceAccessException;
import se.esss.ics.rbac.dsaccess.UserInfo;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.PermissionsBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;
import se.esss.ics.rbac.jsf.users.UsersBean;
import se.esss.ics.rbac.jsf.utils.SetWithConstructorInput;

/**
 * <code>DefinitionsBean</code> is a managed bean (<code>resourcesBean</code>), which contains resources data and
 * methods for executing actions triggered on <code>/Resources/Definitions</code> page. Resources bean is view scoped so
 * it lives as long as user interacting with resources view.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "resourcesBean")
@ViewScoped
public class DefinitionsBean implements Serializable {

    private static final long serialVersionUID = -3633079429508121589L;
    private static final transient Logger LOGGER = LoggerFactory.getLogger(DefinitionsBean.class);
    private static final Charset CHARSET = Charset.forName("UTF-8");

    @ManagedProperty(value = "#{usersBean}")
    private UsersBean usersBean;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{permissionsBean}")
    private PermissionsBean permissionsBean;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private Resources resourcesEJB;
    @EJB
    private Permissions permissionsEJB;
    @EJB
    private Roles rolesEJB;
    @Inject
    private RequestCacheBean requestCacheBean;

    private String wildcard = Constants.ALL;
    private String newResourceName;
    private String newResourceDescription;
    private String updatedResourceDescription;
    private Resource selectedResource;
    private Resource selectedResourcePickList;
    private Permission selectedPermission;
    private UserInfo selectedManager;
    private List<Resource> resources;
    private List<Resource> resourcesPickList;
    private List<Permission> selectedPermissions;
    private List<Permission> unselectedPermissions;
    private boolean resourceChanged;

    // permissions
    private List<Permission> permissionsSourceList;
    private List<Permission> selectedPermissionsSourceList;
    private List<Permission> permissionsTargetList;
    private List<Permission> selectedPermissionsTargetList;

    // managers
    private List<UserInfo> managersSourceList;
    private List<UserInfo> selectedManagersSourceList;
    private List<UserInfo> managersTargetList;
    private List<UserInfo> selectedManagersTargetList;

    /**
     * Initialises managers and permissions dual list models. Method is called post construct.
     */
    @PostConstruct
    private void initialize() {
        initializeManagersDualList();
        permissionsSourceList = new ArrayList<Permission>();
        permissionsTargetList = new ArrayList<Permission>();
        selectedPermissions = new ArrayList<>();
        unselectedPermissions = new ArrayList<>();
        resourceChanged = true;
    }

    /**
     * Sets users bean, which holds informations about users from LDAP.
     *
     * @param usersBean managed bean with informations about users
     */
    public void setUsersBean(UsersBean usersBean) {
        this.usersBean = usersBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     *
     * @param loginBean the login bean through which logged in user info is retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets permissions bean which contains methods for determining permissions for specific actions for the logged in
     * user.
     *
     * @param permissionsBean the bean through which actions permissions are checked
     */
    public void setPermissionsBean(PermissionsBean permissionsBean) {
        this.permissionsBean = permissionsBean;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     *
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * Returns resource which is selected in resources list on <code>/Resources/Definitions</code> page.
     *
     * @return resource selected in resources list.
     */
    public Resource getSelectedResource() {
        return selectedResource;
    }

    /**
     * Sets resource which is selected in resources list on <code>/Resources/Definitions</code> page.
     *
     * @param selectedResource resource selected in resources list
     */
    public void setSelectedResource(Resource selectedResource) {
        this.selectedResource = selectedResource;
        this.selectedManager = null;
    }

    /**
     * Returns resource which is selected in resources drop down list in add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return resource selected in resources drop down list.
     */
    public Resource getSelectedResourcePickList() {
        return selectedResourcePickList;
    }

    /**
     * Sets resource which is selected in resources drop down list in add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @param selectedResourcePickList resource selected in resources drop down list
     */
    public void setSelectedResourcePickList(Resource selectedResourcePickList) {
        this.selectedResourcePickList = selectedResourcePickList;
        resourceChanged = true;
    }

    /**
     * Returns manager which is selected in managers list on <code>/Resources/Definitions</code> page.
     *
     * @return manager selected in managers list.
     */
    public UserInfo getSelectedManager() {
        return selectedManager;
    }

    /**
     * Sets manager which is selected in managers list on <code>/Resources/Definitions</code> page.
     *
     * @param selectedManager manager selected in managers list
     */
    public void setSelectedManager(UserInfo selectedManager) {
        this.selectedManager = selectedManager;
    }

    /**
     * Returns wildcard pattern which is used for searching by resources. Pattern is entered into search field on
     * <code>/Resources/Definitions</code> page.
     *
     * @return wildcard pattern used for searching by resources.
     */
    public String getWildcard() {
        return wildcard;
    }

    /**
     * Sets wildcard pattern which is used for searching by resources. Pattern is entered into search field on
     * <code>/Resources/Definitions</code> page.
     *
     * @param wildcard wildcard pattern used for searching by resources
     */
    public void setWildcard(String wildcard) {
        this.wildcard = wildcard;
    }

    /**
     * @return selected resource name which is shown in resource name field on <code>/Resources/Definitions</code> page.
     */
    public String getResourceName() {
        if (selectedResource != null) {
            return selectedResource.getName();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new resource name which is entered into resource name field in add new resource dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return new resource name entered into resource name field.
     */
    public String getNewResourceName() {
        return newResourceName;
    }

    /**
     * Sets new resource name which is entered into resource name field in add new resource dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @param newResourceName new resource name entered into resource name field
     */
    public void setNewResourceName(String newResourceName) {
        this.newResourceName = removeSpaces(newResourceName);
    }

    /**
     * @return selected resource description which is shown in resource description field on
     *         <code>/Resources/Definitions</code> page.
     */
    public String getResourceDescription() {
        if (selectedResource != null) {
            return selectedResource.getDescription();
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Returns new resource description which is entered into resource description field in add new resource dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return new resource description entered into resource description field.
     */
    public String getNewResourceDescription() {
        return newResourceDescription;
    }

    /**
     * Sets new resource description which is entered into resource description field in add new resource dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @param newResourceDescription new resource description entered into resource description field
     */
    public void setNewResourceDescription(String newResourceDescription) {
        this.newResourceDescription = newResourceDescription;
    }

    /**
     * Returns updated resource description which is entered into resource description field in edit resource
     * description dialog on <code>/Resources/Definitions</code> page.
     *
     * @return updated resource description entered into resource description field.
     */
    public String getUpdatedResourceDescription() {
        return getResourceDescription();
    }

    /**
     * Sets updated resource description which is entered into resource description field in edit resource description
     * dialog on <code>/Resources/Definitions</code> page.
     *
     * @param updatedResourceDescription updated resource description entered into resource description field
     */
    public void setUpdatedResourceDescription(String updatedResourceDescription) {
        this.updatedResourceDescription = updatedResourceDescription;
    }

    /**
     * Retrieves list of resources from database and returns it. Resources are shown in resources list on
     * <code>/Resources/Definitions</code> page.
     *
     * @return list of resources retrieved from database.
     */
    public List<Resource> getResources() {
        resources = fetchResources(false);
        if (!Util.contains(resources, selectedResource)) {
            selectedResource = null;
            selectedManager = null;
            selectedPermission = null;
        }
        return resources;
    }

    /**
     * Retrieves list of all resources from database and returns it. Resources are shown in resources drop down list in
     * add permissions dialog on <code>/Resources/Definitions</code> page.
     *
     * @return list of resources retrieved from database.
     */
    public List<Resource> getResourcesPickList() {
        resourcesPickList = requestCacheBean.getResources(false);
        return resourcesPickList;
    }

    /**
     * Retrieves list of permissions corresponds to the selected resource. Permissions are shown in permissions table on
     * <code>/Resources/Definitions</code> page.
     *
     * @return permissions corresponds to the selected resource.
     */
    public List<Permission> getPermissions() {
        List<Permission> list = new ArrayList<>();
        if (selectedResource != null) {
            list.addAll(selectedResource.getPermissions());
            Collections.sort(list);
        }
        return list;
    }

    /**
     * Retrieves list of managers corresponds to the selected resource. Managers are shown in managers list on
     * <code>/Resources/Definitions</code> page.
     *
     * @return managers corresponds to the selected resource.
     */
    public List<UserInfo> getManagers() {
        if (selectedResource != null) {
            try {
                List<UserInfo> usersInfo = new ArrayList<>();
                if (selectedResource.getManagers() != null) {
                    for (String username : selectedResource.getManagers()) {
                        UserInfo info = usersBean.getUser(username);
                        if (info == null || usersInfo.contains(info)) {
                            continue;
                        }
                        usersInfo.add(info);
                    }
                    return usersInfo;
                }
            } catch (DirectoryServiceAccessException e) {
                LOGGER.error(Messages.getString(Messages.LDAP_EXCEPTION), e);
            }
        }
        return new ArrayList<>();
    }

    /**
     * Method which is called when user selects permission in permissions table on <code>/Resources/Definitions</code>
     * page.
     *
     * @param event select event
     */
    public void onPermissionRowSelect(SelectEvent event) {
        if (event.getObject() != null && event.getObject() instanceof Permission) {
            selectedPermission = (Permission) event.getObject();
        }
    }

    /**
     * Method which is called when user deselects permission in permissions table on <code>/Resources/Definitions</code>
     * page.
     *
     * @param event deselect event
     */
    public void onPermissionRowUnselect(UnselectEvent event) {
        selectedPermission = null;
    }

    /**
     * Method which is called when user transfers permission from source to target list in pick list component which is
     * in add permissions dialog on <code>/Resources/Definitions</code> page. Method checks if permission already exist
     * in this resource or if is already in target list.
     *
     * @param permissions the permissions that are being transferred
     * @param isAdd true if the permissions are added to the selected list or false if they are added to the unselected
     *        list
     */
    public void onPermissionTransfer(List<Permission> permissions, boolean isAdd) {
        for (Permission permission : permissions) {
            if (isAdd) {
                if (!isPermissionInTargetList(permission) && !isPermissionInResource(permission)) {
                    selectedPermissions.add(permission);
                    unselectedPermissions.remove(permission);
                }
            } else {
                unselectedPermissions.add(permission);
                selectedPermissions.remove(permission);
            }
        }
        permissionsSourceList = new ArrayList<Permission>(unselectedPermissions);
        permissionsTargetList = new ArrayList<Permission>(selectedPermissions);
    }

    /**
     * Creates new resource if user has permission for adding resources. Method is called when OK button, in add new
     * resource dialog on <code>/Resources/Definitions</code> page is clicked. Results of the action are logged and
     * shown to the user as growl message.
     */
    public void createResource() {
        if (!permissionsBean.canAddResource(false)) {
            return;
        }
        final Resource resource = new Resource();
        resource.setName(newResourceName);
        resource.setDescription(newResourceDescription);
        resource.setManagers(new SetWithConstructorInput<>(loginBean.getUsername()));
        resource.setPermissions(new HashSet<Permission>(0));
        String success = Messages.getString(Messages.SUCCESSFUL_RESOURCE_CREATION, loginBean.getUsername(),
                resource.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RESOURCE_CREATION, loginBean.getUsername(),
                resource.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                resourcesEJB.createResource(resource);
                selectedResource = resource;
                fetchResources(true);
            }
        });
        setDefaultValues();
    }

    /**
     * Removes selected resource if user has permission for managing resources. Method is called when remove resource
     * button on <code>/Resources/Definitions</code> page is clicked. Results of the action are logged and shown to the
     * user as growl message.
     */
    public void removeResource() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_RESOURCE_REMOVAL, loginBean.getUsername(),
                selectedResource.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RESOURCE_REMOVAL, loginBean.getUsername(),
                selectedResource.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                for (Permission permission : selectedResource.getPermissions()) {
                    for (Role role : permission.getRole()) {
                        role.getPermissions().remove(permission);
                        rolesEJB.updateRole(role);
                    }
                }
                resourcesEJB.removeResource(selectedResource);
                selectedResource = null;
                fetchResources(true);
            }
        });
    }

    /**
     * Updates resource if user has permission for managing resources. Method is called when OK button, in edit resource
     * description dialog on <code>/Resources/Definitions</code> page is clicked. Results of the action are logged and
     * shown to the user as growl message.
     */
    public void updateResource() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        selectedResource.setDescription(updatedResourceDescription);
        String success = Messages.getString(Messages.SUCCESSFUL_RESOURCE_UPDATE, loginBean.getUsername(),
                selectedResource.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RESOURCE_UPDATE, loginBean.getUsername(),
                selectedResource.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                resourcesEJB.updateResource(selectedResource);
                fetchResources(true);
            }
        });
    }

    /**
     * Adds permissions to the selected resource if user has permission for managing resources. Method is called when OK
     * button, in add permissions dialog on <code>/Resources/Definitions</code> page is clicked. Results of the action
     * are logged and shown to the user as growl message.
     */
    public void addPermissions() {
        if (!permissionsBean.canManageResource(selectedResource, false) || permissionsTargetList.isEmpty()) {
            return;
        }
        final Set<Permission> permissions = new HashSet<>();
        for (Permission permission : permissionsTargetList) {
            // copy the permission, but replace the resource
            Permission p = permission.deriveWith(selectedResource);
            permissions.add(p);
        }
        String success = Messages.getString(Messages.SUCCESSFUL_PERMISSIONS_CREATION, loginBean.getUsername(),
                selectedResource.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_PERMISSIONS_CREATION, loginBean.getUsername(),
                selectedResource.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                permissionsEJB.createPermissions(permissions);
                selectedResource = resourcesEJB.getResource(selectedResource.getId());
            }
        });
        permissionsTargetList = new ArrayList<Permission>();
        permissionsSourceList = new ArrayList<Permission>();
        selectedPermissions = new ArrayList<>();
    }

    /**
     * Removes permission from selected resource if user has permission for managing resources. Method is called when
     * remove permission button on <code>/Resources/Definitions</code> page is clicked. Results of the action are logged
     * and shown to the user as growl message.
     */
    public void removePermission() {
        if (!permissionsBean.canManageResource(selectedResource, false) || isRemovePermissionButtonDisabled()) {
            return;
        }
        String success = Messages.getString(Messages.SUCCESSFUL_PERMISSION_REMOVAL, loginBean.getUsername(),
                selectedPermission.getName(), selectedResource.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_PERMISSION_REMOVAL, loginBean.getUsername(),
                selectedPermission.getName(), selectedResource.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                for (Role role : selectedPermission.getRole()) {
                    role.getPermissions().remove(selectedPermission);
                    rolesEJB.updateRole(role);
                }
                permissionsEJB.removePermission(selectedPermission);
                selectedResource = resourcesEJB.getResource(selectedResource.getId());
                selectedPermission = null;
            }
        });

    }

    /**
     * Adds managers to the selected resource if user has permission for managing resources. Method is called when OK
     * button, in add managers dialog on <code>/Resources/Definitions</code> page is clicked. Results of the action are
     * logged and shown to the user as growl message.
     */
    public void addManagers() {
        if (!permissionsBean.canManageResource(selectedResource, false) || managersTargetList.isEmpty()) {
            return;
        }
        Set<String> managers = new HashSet<>(selectedResource.getManagers());
        for (UserInfo manager : managersTargetList) {
            managers.add(manager.getUsername());
        }
        selectedResource.setManagers(managers);
        String success = Messages.getString(Messages.SUCCESSFUL_RESOURCE_MANAGERS_ASSIGNMENT, loginBean.getUsername(),
                selectedResource.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RESOURCE_MANAGERS_ASSIGNMENT,
                loginBean.getUsername(), selectedResource.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                resourcesEJB.updateResource(selectedResource);
            }
        });
        initializeManagersDualList();
    }

    /**
     * Removes manager from selected resource if user has permission for managing resources. Method is called when
     * remove manager button on <code>/Resources/Definitions</code> page is clicked. Results of the action are logged
     * and shown to the user as growl message.
     */
    public void removeManager() {
        if (!permissionsBean.canManageResource(selectedResource, false) || isRemoveManagerButtonDisabled()) {
            return;
        }
        selectedResource.getManagers().remove(selectedManager.getUsername());
        String success = Messages.getString(Messages.SUCCESSFUL_RESOURCE_MANAGER_REMOVAL, loginBean.getUsername(),
                selectedManager.getUsername(), selectedResource.getName());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_RESOURCE_MANAGER_REMOVAL, loginBean.getUsername(),
                selectedManager.getUsername(), selectedResource.getName());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                resourcesEJB.updateResource(selectedResource);
                selectedManager = null;
            }
        });
    }

    /**
     * Remove manager button on <code>/Resources/Definitions</code> page is disabled if no manager is selected or no
     * resource is selected.
     *
     * @return true if manager button is disabled, otherwise false.
     */
    public boolean isRemoveManagerButtonDisabled() {
        return selectedManager == null || selectedResource == null;
    }

    /**
     * Remove permission button on <code>/Resources/Definitions</code> page is disabled if no permission is selected or
     * no resource is selected.
     *
     * @return true if permission button is disabled, otherwise false.
     */
    public boolean isRemovePermissionButtonDisabled() {
        return selectedPermission == null || selectedResource == null;
    }

    /**
     * Method that handles file upload. If file ends with <code>.xlsx</code> calls
     * {@link DefinitionsBean#importFromExcelFile(UploadedFile)} method, else if file ends with <code>.csv</code> calls
     * {@link DefinitionsBean#importFromCSVFile(UploadedFile)} method.
     *
     * @param event file upload event
     */
    public void handleFileUpload(FileUploadEvent event) {
        if (!permissionsBean.canAddResource(false)) {
            return;
        }
        UploadedFile file = event.getFile();
        if (file.getFileName().toLowerCase().endsWith(".xlsx")) {
            importFromExcelFile(file);
        } else if (file.getFileName().toLowerCase().endsWith(".csv")) {
            importFromCSVFile(file);
        }
    }

    /**
     * Method that shows dialog for editing resource description. Dialog is shown when edit description button on
     * <code>/Resources/Definitions</code> page is clicked and if user has permissions for managing resources.
     */
    public void showEditDescriptionDialog() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }

        PrimeFaces.current().ajax().update("editDescriptionForm");
        PrimeFaces.current().executeScript("PF('editDescriptionDialog').show()");
    }

    /**
     * Method that shows dialog for adding managers. Dialog is shown when add managers button on
     * <code>/Resources/Definitions</code> page is clicked and if user has permissions for managing resources.
     */
    public void showAddManagersDialog() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        PrimeFaces.current().executeScript("PF('addManagersDialog').show()");
    }

    /**
     * Method that shows dialog for adding resources. Dialog is shown when add resource button on
     * <code>/Resources/Definitions</code> page is clicked and if user has permissions for adding resources.
     */
    public void showAddResourceDialog() {
        if (!permissionsBean.canAddResource(false)) {
            return;
        }
        PrimeFaces.current().executeScript("PF('addResourceDialog').show()");
    }

    /**
     * Method that shows dialog for adding permissions. Dialog is shown when add permissions button on
     * <code>/Resources/Definitions</code> page is clicked and if user has permissions for managing resources.
     */
    public void showAddPermissionsDialog() {
        if (!permissionsBean.canManageResource(selectedResource, false)) {
            return;
        }
        PrimeFaces.current().ajax().update("addPermissionsForm");
        PrimeFaces.current().executeScript("PF('addPermissionsDialog').show()");
    }

    /**
     * Method that shows dialog for importing resources. Dialog is shown when import button on
     * <code>/Resources/Definitions</code> page is clicked and if user has permissions for adding resources.
     */
    public void showImportResourcesDialog() {
        if (!permissionsBean.canAddResource(false)) {
            return;
        }
        PrimeFaces.current().executeScript("PF('importResourcesDialog').show()");
    }

    /**
     * Resets variables used in add new resource dialog and in add permissions dialog on
     * <code>/Resources/Definitions</code> page to the default values.
     */
    public void setDefaultValues() {
        newResourceName = Constants.EMPTY_STRING;
        newResourceDescription = Constants.EMPTY_STRING;
        if (selectedResourcePickList != null && !resourcesPickList.isEmpty()) {
            selectedResourcePickList = resourcesPickList.get(0);
        }
        resourceChanged = true;
        selectedPermissions = new ArrayList<>();
        unselectedPermissions = new ArrayList<>();
        permissionsSourceList = new ArrayList<Permission>(unselectedPermissions);
        permissionsTargetList = new ArrayList<Permission>(selectedPermissions);
    }

    /**
     * Returns list which contains permissions from source list and is shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return list which contains permissions from source list.
     */
    public List<Permission> getPermissionsSourceList() {
        if (selectedResource != null && selectedResourcePickList != null && resourceChanged) {
            if (selectedResource.getId() == selectedResourcePickList.getId()) {
                unselectedPermissions = new ArrayList<>();
            } else {
                selectedResourcePickList = resourcesEJB.getResource(selectedResourcePickList.getId());
                Set<Permission> permissions = selectedResourcePickList.getPermissions();
                for (Permission permission : selectedPermissions) {
                    if (permissions.contains(permission)) {
                        permissions.remove(permission);
                    }
                }
                unselectedPermissions = new ArrayList<>(permissions);
            }
            permissionsSourceList = new ArrayList<Permission>(unselectedPermissions);
            permissionsSourceList.removeAll(permissionsTargetList);
            resourceChanged = false;
        }
        return permissionsSourceList;
    }

    /**
     * Sets list which contains permissions from source list and is shown on add add permissions dialog on
     * <code>/PVAccessManagement</code> page.
     *
     * @param permissionsSourceList list which contains permissions from source list
     */
    public void setPermissionsSourceList(List<Permission> permissionsSourceList) {
        this.permissionsSourceList = permissionsSourceList;
    }

    /**
     * Returns list which contains permissions from target list and is shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return list which contains permissions from target list.
     */
    public List<Permission> getPermissionsTargetList() {
        return permissionsTargetList;
    }

    /**
     * Sets list which contains permissions from target list and is shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @param permissionsTargetList list which contains permissions from target list
     */
    public void setPermissionsTargetList(List<Permission> permissionsTargetList) {
        this.permissionsTargetList = permissionsTargetList;
    }

    /**
     * Returns list which contains selected permissions from source list and is shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return list which contains selected permissions from source list.
     */
    public List<Permission> getSelectedPermissionsSourceList() {
        return selectedPermissionsSourceList;
    }

    /**
     * Sets list which contains selected permissions from source list and is shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @param selectedPermissionsSourceList list which contains selected permissions from source list
     */
    public void setSelectedPermissionsSourceList(List<Permission> selectedPermissionsSourceList) {
        this.selectedPermissionsSourceList = selectedPermissionsSourceList;
    }

    /**
     * Returns list which contains selected permissions from target list and is shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return list which contains selected permissions from target list.
     */
    public List<Permission> getSelectedPermissionsTargetList() {
        return selectedPermissionsTargetList;
    }

    /**
     * Sets list which contains selected permissions from target list, and is shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @param selectedPermissionsTargetList list which contains selected permissions from target list
     */
    public void setSelectedPermissionsTargetList(List<Permission> selectedPermissionsTargetList) {
        this.selectedPermissionsTargetList = selectedPermissionsTargetList;
    }

    /**
     * Moves selected permissions from source list to the target list. Lists are shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     */
    public void addPermissionsFromSourceToTarget() {
        onPermissionTransfer(selectedPermissionsSourceList, true);
    }

    /**
     * Moves selected permissions from target list to the source list. Lists are shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     */
    public void addPermissionsFromTargetToSource() {
        onPermissionTransfer(selectedPermissionsTargetList, false);
    }

    /**
     * Moves all permissions from source list to the target list. Lists are shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     */
    public void addAllPermissionsFromSourceToTarget() {
        onPermissionTransfer(permissionsSourceList, true);
    }

    /**
     * Moves all permissions from target list to the source list. Lists are shown on add permissions dialog on
     * <code>/Resources/Definitions</code> page.
     */
    public void addAllPermissionsFromTargetToSource() {
        onPermissionTransfer(permissionsTargetList, false);
    }

    /**
     * Returns list which contains users from source list and is shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return list which contains users from source list.
     */
    public List<UserInfo> getManagersSourceList() {
        return managersSourceList;
    }

    /**
     * Sets list which contains users from source list and is shown on add managers dialog on
     * <code>/Resources/Definitions</code> page
     *
     * @param managersSourceList list which contains roles users source list
     */
    public void setManagersSourceList(List<UserInfo> managersSourceList) {
        this.managersSourceList = managersSourceList;
    }

    /**
     * Returns list which contains users from target list and is shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return list which contains users from target list.
     */
    public List<UserInfo> getManagersTargetList() {
        return managersTargetList;
    }

    /**
     * Sets list which contains users from target list and is shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @param managersTargetList list which contains users from target list
     */
    public void setManagersTargetList(List<UserInfo> managersTargetList) {
        this.managersTargetList = managersTargetList;
    }

    /**
     * Returns list which contains selected users from source list and is shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return list which contains selected users from source list.
     */
    public List<UserInfo> getSelectedManagersSourceList() {
        return selectedManagersSourceList;
    }

    /**
     * Sets list which contains selected users from source list and is shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @param selectedManagersSourceList list which contains selected users from source list
     */
    public void setSelectedManagersSourceList(List<UserInfo> selectedManagersSourceList) {
        this.selectedManagersSourceList = selectedManagersSourceList;
    }

    /**
     * Returns list which contains selected users from target list and is shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @return list which contains selected users from target list.
     */
    public List<UserInfo> getSelectedManagersTargetList() {
        return selectedManagersTargetList;
    }

    /**
     * Sets list which contains selected users from target list, and is shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     *
     * @param selectedManagersTargetList list which contains selected users from target list
     */
    public void setSelectedManagersTargetList(List<UserInfo> selectedManagersTargetList) {
        this.selectedManagersTargetList = selectedManagersTargetList;
    }

    /**
     * Moves selected users from source list to the target list. Lists are shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     */
    public void addManagersFromSourceToTarget() {
        if (!selectedManagersSourceList.isEmpty()) {
            managersTargetList.addAll(selectedManagersSourceList);
        }
        managersSourceList.removeAll(selectedManagersSourceList);
        selectedManagersSourceList.clear();
    }

    /**
     * Moves selected users from target list to the source list. Lists are shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     */
    public void addManagersFromTargetToSource() {
        if (!selectedManagersTargetList.isEmpty()) {
            managersSourceList.addAll(selectedManagersTargetList);
        }
        managersTargetList.removeAll(selectedManagersTargetList);
        selectedManagersTargetList.clear();
    }

    /**
     * Moves all users from source list to the target list. Lists are shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     */
    public void addAllManagersFromSourceToTarget() {
        if (!managersSourceList.isEmpty()) {
            managersTargetList.addAll(managersSourceList);
        }
        managersSourceList.clear();
        selectedManagersSourceList.clear();
    }

    /**
     * Moves all users from target list to the source list. Lists are shown on add managers dialog on
     * <code>/Resources/Definitions</code> page.
     */
    public void addAllManagersFromTargetToSource() {
        if (!managersTargetList.isEmpty()) {
            managersSourceList.addAll(managersTargetList);
        }
        managersTargetList.clear();
        selectedManagersTargetList.clear();
    }

    /**
     * Generates excel file which format is defined as: |Name|Description|Resource|Exclusive Access Allowed|.
     *
     * @return generated excel file.
     */
    public StreamedContent getExcelFile() {
        if (resources == null) {
            return null;
        }
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            XSSFSheet sheet = workbook.createSheet("Permissions");
            int rowNumber = 0;
            Row row = sheet.createRow(rowNumber++);
            row.createCell(0).setCellValue("Name");
            row.createCell(1).setCellValue("Description");
            row.createCell(2).setCellValue("Resource");
            row.createCell(3).setCellValue("Exclusive Access Allowed");
            for (Resource resource : resources) {
                for (Permission permission : resource.getPermissions()) {
                    row = sheet.createRow(rowNumber++);
                    row.createCell(0).setCellValue(permission.getName());
                    row.createCell(1).setCellValue(permission.getDescription());
                    row.createCell(2).setCellValue(resource.getName());
                    row.createCell(3).setCellValue(permission.isExclusiveAccessAllowed());
                }
            }
            // auto sized columns
            for (int i = 0; i < 4; i++) {
                sheet.autoSizeColumn(i);
            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            StreamedContent sc = new DefaultStreamedContent(inputStream,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "resources.xlsx");
            taskBean.showFacesMessage(FacesMessage.SEVERITY_INFO,
                    Messages.getString(Messages.SUCCESSFUL_PERMISSION_EXPORT, loginBean.getUsername()),
                    Constants.EMPTY_STRING, true);
            return sc;
        } catch (IOException e) {
            LOGGER.error(Messages.getString(Messages.EXPORT_IO_EXCEPTION), e);
        }
        return null;
    }

    /**
     * Generates CSV file which format is defined as: Name,Description,Resource,Exclusive Access Allowed.
     *
     * @return generated CSV file.
     */
    public StreamedContent getCSVFile() {
        if (resources == null) {
            return null;
        }
        int permisisonCount = 0;
        for (Resource resource : resources) {
            permisisonCount += resource.getPermissions().size();
        }
        StringBuilder content = new StringBuilder(60 + permisisonCount * 150)
                .append("Name,Description,Resource,ExclusiveAccessAllowed\n");
        for (Resource resource : resources) {
            for (Permission permission : resource.getPermissions()) {
                content.append(permission.getName())
                        .append(',')
                        .append((permission.getDescription() == null) ? Constants.EMPTY_STRING : resource
                                .getDescription()).append(',').append(resource.getName()).append(',')
                        .append(permission.isExclusiveAccessAllowed()).append('\n');
            }
        }
        InputStream inputStream = new ByteArrayInputStream(content.toString().getBytes(CHARSET));
        taskBean.showFacesMessage(FacesMessage.SEVERITY_INFO,
                Messages.getString(Messages.SUCCESSFUL_PERMISSION_EXPORT, loginBean.getUsername()),
                Constants.EMPTY_STRING, true);
        return new DefaultStreamedContent(inputStream, "text/csv", "resources.csv");
    }

    /**
     * Imports permissions from excel file. Excel file format is defined as:
     *
     * |Name|Description|Resource|Exclusive Access Allowed|
     *
     * @param uploadedFile excel file from which permissions will be imported
     */
    private void importFromExcelFile(UploadedFile uploadedFile) {
        try (InputStream stream = uploadedFile.getInputstream(); XSSFWorkbook workbook = new XSSFWorkbook(stream)) {
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            // jump over first row - headings
            if (rowIterator.hasNext()) {
                rowIterator.next();
            }
            final List<Permission> permissions = new ArrayList<>();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (row.getCell(0) != null && row.getCell(2) != null) {
                    // create resource
                    Resource resource = new Resource();
                    resource.setName(row.getCell(2).getStringCellValue());
                    resource.setManagers(new SetWithConstructorInput<>(loginBean.getUsername()));
                    // create permission
                    Permission permission = new Permission();
                    permission.setName(row.getCell(0).getStringCellValue());
                    permission.setDescription(row.getCell(1) == null ? Constants.EMPTY_STRING : row.getCell(1)
                            .getStringCellValue());
                    permission.setExclusiveAccessAllowed(row.getCell(3) == null ? false : row.getCell(3)
                            .getBooleanCellValue());
                    permission.setResource(resource);
                    permissions.add(permission);
                }
            }
            String success = Messages.getString(Messages.SUCCESSFUL_PERMISSIONS_IMPORT, loginBean.getUsername());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_PERMISSIONS_IMPORT, loginBean.getUsername());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    permissionsEJB.createPermissionsAndResources(permissions);
                }
            });
        } catch (IOException e) {
            LOGGER.error(Messages.getString(Messages.IMPORT_IO_EXCEPTION), e);
        }
    }

    /**
     * Imports permissions from CSV file. CSV file format is defined as:
     *
     * Name,Description,Resource,Exclusive Access Allowed
     *
     * @param uploadedFile CSV file from which permissions will be imported
     */
    private void importFromCSVFile(UploadedFile file) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputstream(), CHARSET))) {
            String line = br.readLine();
            final List<Permission> permissions = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                String[] permissionData = line.split(Constants.SPLIT_COMMA);
                int permissionDataLength = permissionData.length;
                if (permissionDataLength > 0 && !permissionData[0].isEmpty()) {
                    Permission permission = new Permission();
                    permission.setName(permissionData[0]);
                    permission.setDescription(permissionDataLength > 1 ? permissionData[1] : Constants.EMPTY_STRING);
                    if (permissionDataLength < 2 || permissionData[2].isEmpty()) {
                        continue;
                    } else {
                        Resource resource = new Resource();
                        resource.setName(permissionData[2]);
                        resource.setManagers(new SetWithConstructorInput<>(loginBean.getUsername()));
                        permission.setResource(resource);
                    }
                    permission.setExclusiveAccessAllowed(permissionDataLength > 3 ? Boolean.valueOf(permissionData[3])
                            : false);
                    permissions.add(permission);
                }
            }
            String success = Messages.getString(Messages.SUCCESSFUL_PERMISSIONS_IMPORT, loginBean.getUsername());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_PERMISSIONS_IMPORT, loginBean.getUsername());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    permissionsEJB.createPermissionsAndResources(permissions);
                }
            });
        } catch (IOException e) {
            LOGGER.error(Messages.getString(Messages.IMPORT_IO_EXCEPTION), e);
        }
    }

    /**
     * Removes spaces from resource name.
     *
     * @param resourceName resource name
     *
     * @return resource name without spaces.
     */
    private static String removeSpaces(String resourceName) {
        return resourceName.replaceAll("\\p{Z}", Constants.EMPTY_STRING);
    }

    /**
     * Initialises managers dual list model which data are used in add managers dialog on
     * <code>/Resources/Definitions</code> page.
     */
    private void initializeManagersDualList() {
        List<UserInfo> sourceList = null;
        try {
            sourceList = usersBean.getUsersList(Constants.ALL);
        } catch (DirectoryServiceAccessException e) {
            LOGGER.error(Messages.getString(Messages.LDAP_EXCEPTION), e);
        }
        sourceList = (sourceList == null) ? new ArrayList<UserInfo>() : sourceList;
        managersSourceList = new ArrayList<UserInfo>(sourceList);
        managersTargetList = new ArrayList<UserInfo>();
        selectedManagersSourceList = new ArrayList<UserInfo>();
        selectedManagersTargetList = new ArrayList<UserInfo>();
    }

    /**
     * Checks if permission is already in permissions target list. If permission is in target list returns true,
     * otherwise false.
     *
     * @param permission permission which is checked
     *
     * @return true if permission is already in target list, otherwise false.
     */
    private boolean isPermissionInTargetList(Permission permission) {
        for (Permission p : selectedPermissions) {
            if (permission.getName().equals(p.getName())) {
                String detail = "Permission: " + p.getName();
                taskBean.showFacesMessage(FacesMessage.SEVERITY_WARN,
                        Messages.getString(Messages.TARGET_LIST_CONTAINS_PERMISSION), detail, false);
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if selected resource already contains permission. If permission is in selected returns true, otherwise
     * false.
     *
     * @param permission permission which is checked
     *
     * @return true if permission is already in selected resource, otherwise false.
     */
    private boolean isPermissionInResource(Permission permission) {
        for (Permission resourcePermission : selectedResource.getPermissions()) {
            if (permission.getName().equals(resourcePermission.getName())) {
                String detail = "Permission: " + resourcePermission.getName();
                taskBean.showFacesMessage(FacesMessage.SEVERITY_WARN,
                        Messages.getString(Messages.RESOURCE_CONTAINS_PERMISSION), detail, false);
                return true;
            }
        }
        return false;
    }

    /**
     * @param fromDB fetch resources from database, or return cached resources.
     *
     * @return list of resources
     */
    private List<Resource> fetchResources(boolean fromDB) {
        if (Constants.ALL.equals(wildcard)) {
            return requestCacheBean.getResources(fromDB);
        } else {
            return requestCacheBean.getResourcesByWildcard("%" + wildcard + "%", fromDB);
        }
    }
}
