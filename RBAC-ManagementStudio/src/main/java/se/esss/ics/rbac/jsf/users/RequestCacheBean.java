/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.users;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.datamodel.UserRole;
import se.esss.ics.rbac.datamodel.UserRole.AssignmentType;
import se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.ejbs.interfaces.UserRoles;
import se.esss.ics.rbac.jsf.users.UserInfoBean.UserPermission;

/**
 *
 * <code>RequestCacheBean</code> is a request bean used on the users page. It provides access to DB content by caching
 * the results for the duration of the request. Using this bean eliminates multiple calls to the database, which can be
 * a result of primefaces requesting specific data multiple times.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@ManagedBean(name = "usersRequestCacheBean")
@RequestScoped
public class RequestCacheBean implements Serializable {

    private static final long serialVersionUID = -8424001220686889133L;

    @ManagedProperty(value = "#{userInfoBean}")
    private UserInfoBean usersBean;
    @EJB
    private Roles rolesEJB;
    @EJB
    private Resources resourcesEJB;
    @EJB
    private UserRoles userRolesEJB;
    @EJB
    private ExclusiveAccesses exclusiveAccessEJB;

    private List<Role> roles;
    private List<Resource> resources;
    private List<UserRole> assignedUserRoles;
    private List<ExclusiveAccess> activeExclusiveAccesses;

    /**
     * @param usersBean the user info bean
     */
    public void setUsersBean(UserInfoBean usersBean) {
        this.usersBean = usersBean;
    }

    /**
     * @return the list of all roles defined in the system
     */
    public List<Role> getRoles() {
        if (roles == null) {
            roles = rolesEJB.getRoles();
        }
        return roles;
    }

    /**
     * @return the list of all resources defined in the system
     */
    public List<Resource> getResources() {
        if (resources == null) {
            resources = resourcesEJB.getResources();
        }
        return resources;
    }

    /**
     * @return the list of all roles that are currently assigned to the selected user
     */
    public List<UserRole> getAssignedRoles() {
        if (usersBean.getSelectedUser() == null) {
            return new ArrayList<UserRole>(0);
        }
        if (assignedUserRoles == null) {
            assignedUserRoles = userRolesEJB.getUserRoles(usersBean.getSelectedUser().getUsername());
        }
        return assignedUserRoles;
    }

    /**
     * @return the list of all exclusive accesses that are owned by the selected user
     */
    public List<ExclusiveAccess> getActiveExclusiveAccess() {
        if (usersBean.getSelectedUser() == null) {
            return new ArrayList<ExclusiveAccess>(0);
        }
        if (activeExclusiveAccesses == null) {
            activeExclusiveAccesses = exclusiveAccessEJB.getActiveExclusiveAccesses(usersBean.getSelectedUser()
                    .getUsername());
        }
        return activeExclusiveAccesses;
    }

    /**
     * @return list of user permissions based on roles of the selected user. If no user is selected return empty list.
     *         Permission name, resource name and roles are show in the table on <code>/Users/Permissions</code> page.
     */
    public List<UserPermission> getUserPermissions() {
        if (usersBean.getSelectedUser() != null) {
            List<UserRole> assignedRoles = getAssignedRoles();
            List<UserPermission> userPermissions = new ArrayList<>();
            for (UserRole userRole : assignedRoles) {
                if ((userRole.getAssignment() == AssignmentType.DELEGATED
                        || userRole.getAssignment() == AssignmentType.LIMITED)
                        && System.currentTimeMillis() > userRole.getEndTime().getTime()) {
                    continue;
                }
                for (Permission permission : userRole.getRole().getPermissions()) {
                    UserPermission userPermission = new UserPermission(permission.getName(), permission.getResource()
                            .getName(), userRole.getRole().getName());
                    int idx = userPermissions.indexOf(userPermission);
                    if (idx > -1) {
                        userPermissions.get(idx).merge(userPermission);
                    } else {
                        userPermissions.add(userPermission);
                    }
                }
            }
            Collections.sort(userPermissions);
            return userPermissions;
        }
        return new ArrayList<>();
    }
}
