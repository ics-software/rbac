/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.admin;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.esss.ics.rbac.access.RBACConnector;
import se.esss.ics.rbac.access.RBACConnectorException;
import se.esss.ics.rbac.ejbs.RBACAccessEJB;
import se.esss.ics.rbac.ejbs.interfaces.Admin;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.general.Constants;
import se.esss.ics.rbac.jsf.general.ManagementStudioExecutionTask;
import se.esss.ics.rbac.jsf.general.ManagementStudioTaskBean;
import se.esss.ics.rbac.jsf.general.Messages;

/**
 * <code>Settings</code> is a managed bean (<code>settingsBean</code>), that contains methods for changing default
 * parameter values such as administrator password, token expiration period, etc. Settings bean is session scoped, so it
 * lives for the duration of the whole session.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "settingsBean")
@ViewScoped
public class Settings implements Serializable {

    private static final long serialVersionUID = 7009756782600239357L;

    private static final transient Logger LOGGER = LoggerFactory.getLogger(RBACAccessEJB.class);

    public static final String RBAC_USER_MANUAL_URL = "rbac.userManualURL";

    private int actionNumber;
    private char[] currentPassword;
    private char[] newPassword;
    private char[] confirmedNewPassword;
    private String exclusiveAccessDuration;
    private String tokenDuration;
    private String rulePermission;

    @ManagedProperty(value = "#{adminAuthBean}")
    private Authentication adminAuthBean;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{installPermissions}")
    private InstallPermissions installPermissions;
    @ManagedProperty(value = "#{taskBean}")
    private ManagementStudioTaskBean taskBean;
    @EJB
    private Admin adminAuthEJB;

    /**
     * Sets login bean used for checking if a user is logged in.
     *
     * @param loginBean the login bean through which the logged in user info can be retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Sets admin authentication bean used for checking if a user has provided admin password.
     *
     * @param adminAuthBean the admin authentication bean which handles admin authentication
     */
    public void setAdminAuthBean(Authentication adminAuthBean) {
        this.adminAuthBean = adminAuthBean;
    }

    /**
     * Sets install permissions bean.
     *
     * @param installPermissions the install permissions bean through which default permissions are installed
     */
    public void setInstallPermissions(InstallPermissions installPermissions) {
        this.installPermissions = installPermissions;
    }

    /**
     * Sets management studio task bean used for executing tasks.
     *
     * @param taskBean the management studio task bean
     */
    public void setTaskBean(ManagementStudioTaskBean taskBean) {
        this.taskBean = taskBean;
    }

    /**
     * @return current password which is entered into current password field on <code>/Admin/Settings</code> page.
     */
    public char[] getCurrentPassword() {
        return currentPassword != null ? Arrays.copyOf(currentPassword, currentPassword.length) : null;
    }

    /**
     * Sets current password which is entered into current password field on <code>/Admin/Settings</code> page.
     *
     * @param currentPassword current password
     */
    public void setCurrentPassword(char[] currentPassword) {
        this.currentPassword = currentPassword != null ? Arrays.copyOf(currentPassword, currentPassword.length) : null;
    }

    /**
     * @return new password which is entered into new password field on <code>/Admin/Settings</code> page.
     */
    public char[] getNewPassword() {
        return newPassword != null ? Arrays.copyOf(newPassword, newPassword.length) : null;
    }

    /**
     * Sets new password which is entered into new password field on <code>/Admin/Settings</code> page.
     *
     * @param newPassword new password
     */
    public void setNewPassword(char[] newPassword) {
        this.newPassword = newPassword != null ? Arrays.copyOf(newPassword, newPassword.length) : null;
    }

    /**
     * @return confirmed password which is entered into confirm password field on <code>/Admin/Settings</code> page.
     */
    public char[] getConfirmedNewPassword() {
        return confirmedNewPassword != null ? Arrays.copyOf(confirmedNewPassword, confirmedNewPassword.length) : null;
    }

    /**
     * Sets confirmed password which is entered into confirm password field on <code>/Admin/Settings</code> page.
     *
     * @param confirmedNewPassword confirmed password
     */
    public void setConfirmedNewPassword(char[] confirmedNewPassword) {
        this.confirmedNewPassword = confirmedNewPassword != null
                ? Arrays.copyOf(confirmedNewPassword, confirmedNewPassword.length)
                : null;
    }

    /**
     * @return current default exclusive access duration (exclusive access expiration period). Value is shown in current
     *         default exclusive access duration field on <code>/Admin/Settings</code> page.
     */
    public String getCurrentExclusiveAccessDuration() {
        return String.valueOf(adminAuthEJB.getExclusiveAccessExpirationPeriod() / 60000);
    }

    /**
     * @return new exclusive access duration (exclusive access expiration period). Value is retrieved from new default
     *         exclusive access duration field on <code>/Admin/Settings</code> page.
     */
    public String getExclusiveAccessDuration() {
        return exclusiveAccessDuration;
    }

    /**
     * Sets exclusive access duration (exclusive access expiration period). Value is retrieved from new default
     * exclusive access duration field on <code>/Admin/Settings</code> page.
     *
     * @param exclusiveAccessDuration exclusive access duration
     */
    public void setExclusiveAccessDuration(String exclusiveAccessDuration) {
        this.exclusiveAccessDuration = exclusiveAccessDuration;
    }

    /**
     * @return current default token duration (token expiration period). Value is shown in current token duration field
     *         on <code>/Admin/Settings</code> page.
     */
    public String getCurrentTokenDuration() {
        return String.valueOf(adminAuthEJB.getTokenExpirationPeriod() / 60000);
    }

    /**
     * @return new token duration (token expiration period). Value is retrieved from new token duration field on
     *         <code>/Admin/Settings</code> page.
     */
    public String getTokenDuration() {
        return tokenDuration;
    }

    /**
     * Sets token duration (token expiration period). Value is retrieved from new token duration field on
     * <code>/Admin/Settings</code> page.
     *
     * @param tokenDuration token duration
     */
    public void setTokenDuration(String tokenDuration) {
        this.tokenDuration = tokenDuration;
    }

    /**
     * @return current default rule permission. Value is shown in ASG rule permission radio box on
     *         <code>/Admin/Settings</code> page.
     */
    public String getRulePermission() {
        return adminAuthEJB.getAccessSecurityRulePermission();
    }

    /**
     * Sets new access security rule permission. Value is retrieved from ASG rule permission radio box on
     * <code>/Admin/Settings</code> page.
     *
     * @param rulePermission access security rule permission
     */
    public void setRulePermission(String rulePermission) {
        this.rulePermission = rulePermission;
    }

    /**
     * @return informations about public key. Informations are shown in text area on <code>/Admin/Settings</code> page.
     */
    public String getPublicKeyInfo() {
        try {
            PublicKey pubKey = RBACConnector.getInstance().getPublicKey();
            StringBuilder sb = new StringBuilder(130).append("Public key info:").append("\n\nAlgorithm Name: ")
                    .append(pubKey.getAlgorithm()).append("\nEncoding Format: ").append(pubKey.getFormat());
            if (pubKey instanceof RSAPublicKey) {
                RSAPublicKey key = (RSAPublicKey) pubKey;
                sb.append("\nPublic Exponent: ").append(key.getPublicExponent());
            }
            return sb.toString();
        } catch (RBACConnectorException e) {
            LOGGER.error(Messages.getString(Messages.KEYS_RETRIEVING_EXCEPTION), e);
        }
        return Constants.EMPTY_STRING;
    }

    /**
     * Executes selected action which is identified by integer (1 - 5).
     * <ul>
     * <li>1 - install default permissions</li>
     * <li>2 - regenerate public and private keys</li>
     * <li>3 - change token expiration period</li>
     * <li>4 - change exclusive access expiration period</li>
     * <li>5 - redirect administrator to logs</li>
     * <li>6 - set default ASG permission</li>
     * </ul>
     */
    public void executeAction() {
        adminAuthBean.authenticate();
        if (adminAuthBean.getIsAuthenticated()) {
            if (actionNumber == 1) {
                installPermissions();
            } else if (actionNumber == 2) {
                regenerateKeys();
            } else if (actionNumber == 3) {
                changeTokenExpirationPeriod();
            } else if (actionNumber == 4) {
                changeExclusiveAccessExpirationPeriod();
            } else if (actionNumber == 5) {
                redirectToLogs();
            } else if (actionNumber == 6) {
                setDefaultASGPermission();
            }
        } else {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR,
                    Messages.getString(Messages.ADMIN_PASSWORD_EXCEPTION), Constants.EMPTY_STRING, false);
        }
        adminAuthBean.setEnteredPassword(null);
        actionNumber = 0;
    }

    /**
     * @return generated public key file (public.key). Method is called if download key button on
     *         <code>/Admin/Settings</code> page is clicked.
     */
    public StreamedContent getGeneratedFile() {
        PublicKey pubKey = null;
        try {
            pubKey = RBACConnector.getInstance().getPublicKey();
            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubKey.getEncoded());
            InputStream is = new ByteArrayInputStream(pubKeySpec.getEncoded());
            return new DefaultStreamedContent(is, "text/plain", "public.key");
        } catch (RBACConnectorException e) {
            LOGGER.error(Messages.getString(Messages.FILE_GENERATING_EXCEPTION), e);
        }
        return null;
    }

    /**
     * Shows password dialog, if user is logged in. Password dialog is shown if any button except confirm change of the
     * administrator password button or download key button on <code>/Admin/Settings</code> page is clicked.
     *
     * @param actionNumber action number, button identifier
     */
    public void showPasswordDialog(long actionNumber) {
        if (loginBean.isLoggedInValidated()) {
            this.actionNumber = (int) actionNumber;
            PrimeFaces.current().executeScript("PF('enterPasswordDialog').show()");
        } else {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.LOGIN_EXCEPTION),
                    Constants.EMPTY_STRING, false);
        }
    }

    /**
     * @return true if buttons are disabled (user is not logged in) or false if buttons are enabled (user is logged in).
     */
    public boolean isButtonDisabled() {
        return !loginBean.isLoggedInValidated();
    }

    /**
     * Changes administrator password. Method is called if confirm change administrator password button on
     * <code>/Admin/Settings</code> page is clicked. Administrator password is changed successfully, if all fields are
     * non-empty and if administrator password is correct and if the new password is same as confirmed password.
     */
    public void changePassword() {
        if (isCharArrayEmpty(currentPassword) || isCharArrayEmpty(newPassword)
                || isCharArrayEmpty(confirmedNewPassword)) {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.EMPTY_FIELDS_EXCEPTION),
                    Constants.EMPTY_STRING, false);
        } else {
            String success = Messages.getString(Messages.SUCCESSFUL_ADMIN_PASSWORD_CHANGE, loginBean.getUsername());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_ADMIN_PASSWORD_CHANGE, loginBean.getUsername());
            if (Arrays.equals(newPassword, confirmedNewPassword)) {
                final boolean checkPassword = adminAuthEJB.authenticate(currentPassword);
                if (checkPassword) {
                    taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                        @Override
                        public void execute() throws Exception {
                            adminAuthEJB.changePassword(newPassword);
                        }
                    });
                    return;
                }
            }
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, failure, Constants.EMPTY_STRING, false);
        }
        currentPassword = null;
        newPassword = null;
        confirmedNewPassword = null;
        adminAuthBean.invalidate();
    }

    /**
     * Installs default permissions. Method is called if install permission button is clicked and if entered
     * administrator password in enter administrator password dialog on <code>/Admin/Settings</code> page is correct.
     */
    private void installPermissions() {
        String success = Messages.getString(Messages.SUCCESSFUL_DEFAULT_PERMISSIONS_INSTALL, loginBean.getUsername());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_DEFAULT_PERMISSIONS_INSTALL, loginBean.getUsername());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                installPermissions.installPermissions();
            }
        });
        adminAuthBean.invalidate();
    }

    /**
     * Changes the default token expiration period. Method is called if change token duration period button is clicked
     * and if entered administrator password in enter administrator password dialog on <code>/Admin/Settings</code> page
     * is correct.
     */
    private void changeTokenExpirationPeriod() {
        if (tokenDuration == null || tokenDuration.isEmpty()) {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.EMPTY_FIELDS_EXCEPTION),
                    Constants.EMPTY_STRING, false);
        } else {
            String success = Messages.getString(Messages.SUCCESSFUL_TOKEN_EXPIRATION_PERIOD_CHANGE,
                    loginBean.getUsername());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_TOKEN_EXPIRATION_PERIOD_CHANGE,
                    loginBean.getUsername());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    long minutes = Long.parseLong(tokenDuration) * 60000;
                    adminAuthEJB.setTokenExpirationPeriod(minutes);
                }
            });
        }
        tokenDuration = Constants.EMPTY_STRING;
        adminAuthBean.invalidate();
    }

    /**
     * Changes the default exclusive access expiration period. Method is called if confirm change default exclusive
     * access duration button is clicked and if entered administrator password in enter administrator password dialog on
     * <code>/Admin/Settings</code> page is correct.
     */
    private void changeExclusiveAccessExpirationPeriod() {
        if (exclusiveAccessDuration == null || exclusiveAccessDuration.isEmpty()) {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.EMPTY_FIELDS_EXCEPTION),
                    Constants.EMPTY_STRING, false);
        } else {
            String success = Messages.getString(Messages.SUCCESSFUL_EA_DURATION_PERIOD_CHANGE, loginBean.getUsername());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_EA_DURATION_PERIOD_CHANGE,
                    loginBean.getUsername());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    long minutes = Long.parseLong(exclusiveAccessDuration) * 60000;
                    adminAuthEJB.setExclusiveAccessExpirationPeriod(minutes);
                }
            });
        }
        exclusiveAccessDuration = Constants.EMPTY_STRING;
        adminAuthBean.invalidate();
    }

    /**
     * Regenerates private and public key pair. Method is called if regenerate keys button is clicked and if entered
     * administrator password in enter administrator password dialog on <code>/Admin/Settings</code> page is correct.
     */
    private void regenerateKeys() {
        String success = Messages.getString(Messages.SUCCESSFUL_KEYS_REGENERATE, loginBean.getUsername());
        String failure = Messages.getString(Messages.UNSUCCESSFUL_KEYS_REGENERATE, loginBean.getUsername());
        taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
            @Override
            public void execute() throws Exception {
                adminAuthEJB.regenerateKeyPair();
            }
        });
        adminAuthBean.invalidate();
    }

    /**
     * Redirects users to logs page. If entered administrator password in enter administrator password dialog is
     * correct, redirects users to <code>/Admin/Logs</code> page.
     */
    private static void redirectToLogs() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletResponse response = (HttpServletResponse) context.getResponse();
        try {
            response.sendRedirect(context.getRequestContextPath() + "/admin/logs.xhtml");
        } catch (IOException e) {
            LOGGER.error(Messages.getString(Messages.REDIRECT_ERROR,
                    context.getRequestContextPath() + "/admin/logs.xhtml"), e);
        }
    }

    /**
     * Set the permission which is to be used with the default group.
     */
    private void setDefaultASGPermission() {
        if (rulePermission == null || rulePermission.isEmpty()) {
            taskBean.showFacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString(Messages.EMPTY_FIELDS_EXCEPTION),
                    Constants.EMPTY_STRING, false);
        } else {
            String success = Messages.getString(Messages.SUCCESSFUL_DEFAULT_ASG_PERMISSION_CHANGE,
                    loginBean.getUsername());
            String failure = Messages.getString(Messages.UNSUCCESSFUL_DEFAULT_ASG_PERMISSION_CHANGE,
                    loginBean.getUsername());
            taskBean.execute(success, failure, new ManagementStudioExecutionTask<Exception>() {
                @Override
                public void execute() throws Exception {
                    adminAuthEJB.setAccessSecurityRulePermission(rulePermission);
                }
            });
            adminAuthBean.invalidate();
        }
    }

    /**
     * Checks if given char array is empty and return true if is empty, otherwise false.
     *
     * @param array char array
     * @return true if char array is empty, otherwise false.
     */
    private static boolean isCharArrayEmpty(char[] array) {
        return array == null || array.length == 0;
    }

    /**
     * @return the url pointing to the RBAC help pages
     */
    public String getUserManualURL() {
        String s = System.getProperty(RBAC_USER_MANUAL_URL);
        if (s == null) {
            s = "https://help.esss.lu.se/help/rbac/user-role-help.xhtml";
        }
        return s;
    }
}
