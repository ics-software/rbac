/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.pvaccess;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.ejbs.interfaces.AccessSecurityGroups;
import se.esss.ics.rbac.ejbs.interfaces.IPGroups;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.pvaccess.AccessSecurityGroup;

/**
 * <code>RequestCacheBean</code> is a request bean used on the groups page. It provides access to DB content by caching
 * the results for the duration of the request. Using this bean eliminates multiple calls to the database, which can be
 * a result of primefaces requesting specific data multiple times.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "groupsRequestCacheBean")
@RequestScoped
public class RequestCacheBean implements Serializable {

    private static final long serialVersionUID = 2134549594035120094L;

    @EJB
    private AccessSecurityGroups groupEJB;
    @EJB
    private Roles rolesEJB;
    @EJB
    private IPGroups ipGroupsEJB;

    private String currentWildcard;

    private List<Role> roles;
    private List<IPGroup> ipGroups;
    private List<AccessSecurityGroup> groups;
    private List<AccessSecurityGroup> groupsByWildcard;

    /**
     * @return roles list which is used in add roles for selected rule dialog on <code>/PVAccessManagement</code> page.
     */
    public List<Role> getRoles() {
        if (roles == null) {
            roles = rolesEJB.getRoles();
        }
        return roles;
    }

    /**
     * @return ip groups list which is used in add IP groups for selected rule dialog on
     *         <code>/PVAccessManagement</code> page.
     */
    public List<IPGroup> getIPGroups() {
        if (ipGroups == null) {
            ipGroups = ipGroupsEJB.getIPGroups();
        }
        return ipGroups;
    }

    /**
     * Retrieves list of access security groups from database and returns it. Access security groups are shown in access
     * security groups list on <code>/PVAccessManagement</code> page.
     * 
     * @param refresh if true retrieves groups from database otherwise return cached groups
     * 
     * @return list of access security groups retrieved from database.
     */
    public List<AccessSecurityGroup> getAccessSecurityGroups(boolean refresh) {
        if (refresh || groups == null) {
            groups = groupEJB.getAccessSecurityGroups();
        }
        return groups;
    }

    /**
     * Retrieves list of access security groups from database (which matches wildcard) and returns it. Access security
     * groups are shown in access security groups list on <code>/PVAccessManagement</code> page.
     * 
     * @param wildcard search groups by wildcard
     * @param refresh if true retrieves groups from database otherwise return cached groups
     * 
     * @return list of access security groups retrieved from database.
     */
    public List<AccessSecurityGroup> getAccessSecurityGroupsByWildcard(String wildcard, boolean refresh) {
        if (refresh || groupsByWildcard == null || !currentWildcard.equals(wildcard)) {
            currentWildcard = wildcard;
            groupsByWildcard = groupEJB.getAccessSecurityGroupsByWildcard(wildcard);
        }
        return groupsByWildcard;
    }
}
