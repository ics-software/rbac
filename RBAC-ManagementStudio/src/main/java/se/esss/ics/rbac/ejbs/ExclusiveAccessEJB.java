/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.datamodel.ExclusiveAccess;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>ExclusiveAccessEJB</code> is a stateless bean containing utility methods for dealing with exclusive accesses.
 * Contains methods for retrieving, inserting, deleting and updating exclusive access informations. All methods are
 * defined in <code>ExclusiveAccesses</code> interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class ExclusiveAccessEJB implements ExclusiveAccesses {

    private static final long serialVersionUID = 3387142352919626179L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses#getExclusiveAccess(int)
     */
    @Override
    public ExclusiveAccess getExclusiveAccess(int exclusiveAccessId) {
        return em.find(ExclusiveAccess.class, exclusiveAccessId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses#getExclusiveAccesses()
     */
    @Override
    public List<ExclusiveAccess> getExclusiveAccesses() {
        return em.createNamedQuery("ExclusiveAccess.selectAll", ExclusiveAccess.class).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses#getActiveExclusiveAccesses(java.lang.String)
     */
    @Override
    public List<ExclusiveAccess> getActiveExclusiveAccesses(String userId) {
        List<ExclusiveAccess> result = em.createNamedQuery("ExclusiveAccess.findByUserId", ExclusiveAccess.class)
                .setParameter("userId", userId).getResultList();
        //for initialisation of lazily loaded stuff 
        for (ExclusiveAccess exclusiveAccess : result) {
            exclusiveAccess.getPermission().getResource();
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses#getExclusiveAccessPermissions()
     */
    @Override
    public List<Permission> getExclusiveAccessPermissions() {
        return em.createNamedQuery("ExclusiveAccess.selectPermissions", Permission.class).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses#releaseExclusiveAccess(se.esss.ics.rbac.datamodel.
     * ExclusiveAccess)
     */
    @Override
    public void releaseExclusiveAccess(ExclusiveAccess exclusiveAccess) {
        em.remove(em.find(ExclusiveAccess.class, exclusiveAccess.getId()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses#releaseExclusiveAccesses(java.util.List)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void releaseExclusiveAccesses(List<ExclusiveAccess> exclusiveAccesses) {
        int i = 0;
        for (ExclusiveAccess exclusiveAccess : exclusiveAccesses) {
            em.remove(em.find(ExclusiveAccess.class, exclusiveAccess.getId()));
            if (i % 20 == 0) {
                em.flush();
                em.clear();
            }
            i++;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.ExclusiveAccesses#createExclusiveAccesses(java.util.List)
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createExclusiveAccesses(List<ExclusiveAccess> exclusiveAccessList) {
        for (int i = 0; i < exclusiveAccessList.size(); i++) {
            em.persist(exclusiveAccessList.get(i));
            if (i % 20 == 0) {
                em.flush();
                em.clear();
            }
        }
    }
}
