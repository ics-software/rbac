/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.esss.ics.rbac.datamodel.IPGroup;
import se.esss.ics.rbac.ejbs.interfaces.IPGroups;
import se.esss.ics.rbac.jsf.general.Constants;

/**
 * <code>IPGroupEJB</code> is a stateless bean containing utility methods for dealing with IP groups. Contains methods
 * for retrieving, inserting, deleting and updating IP group informations. All methods are defined in
 * <code>IPGroups</code> interface.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Stateless
public class IPGroupEJB implements IPGroups {

    private static final long serialVersionUID = -9103241262914064412L;

    @PersistenceContext(unitName = Constants.PERSISTENCE_CONTEXT_NAME)
    private transient EntityManager em;

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.IPGroups#getIPGroup(int)
     */
    @Override
    public IPGroup getIPGroup(int ipGroupId) {
        IPGroup group = em.find(IPGroup.class, ipGroupId);
        // collections are lazy loaded
        if (group != null) {
            group.getIp().size();
        }
        return group;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.IPGroups#getIPGroups()
     */
    @Override
    public List<IPGroup> getIPGroups() {
        return em.createNamedQuery("IPGroup.selectAll", IPGroup.class).getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.IPGroups#getIPGroupsByWildcard(java.lang.String)
     */
    @Override
    public List<IPGroup> getIPGroupsByWildcard(String wildcard) {
        return em.createNamedQuery("IPGroup.findByWildcard", IPGroup.class).setParameter("wildcard", wildcard)
                .getResultList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.IPGroups#createIPGroup(se.esss.ics.rbac.datamodel.IPGroup)
     */
    @Override
    public void createIPGroup(IPGroup ipGroup) {
        em.persist(ipGroup);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.IPGroups#updateIPGroup(se.esss.ics.rbac.datamodel.IPGroup)
     */
    @Override
    public void updateIPGroup(IPGroup ipGroup) {
        em.merge(ipGroup);
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.esss.ics.rbac.ejbs.interfaces.IPGroups#removeIPGroup(se.esss.ics.rbac.datamodel.IPGroup)
     */
    @Override
    public void removeIPGroup(IPGroup ipGroup) {
        em.remove(em.find(IPGroup.class, ipGroup.getId()));
    }
}
