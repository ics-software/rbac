/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jsf.roles;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;

import se.esss.ics.rbac.NamedEntity;
import se.esss.ics.rbac.datamodel.Permission;
import se.esss.ics.rbac.datamodel.Resource;
import se.esss.ics.rbac.datamodel.Role;
import se.esss.ics.rbac.ejbs.interfaces.Permissions;
import se.esss.ics.rbac.ejbs.interfaces.Resources;
import se.esss.ics.rbac.ejbs.interfaces.Roles;
import se.esss.ics.rbac.jsf.auth.LoginBean;
import se.esss.ics.rbac.jsf.auth.SessionPermissionsBean;

/**
 * <code>MatrixBean</code> is a managed bean (<code>matrixBean</code>), which contains roles data and methods for
 * executing actions triggered on <code>/Roles/Matrix</code> page. Matrix bean is view scoped so it lives as long as
 * user interacting with matrix page view.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "matrixBean")
@ViewScoped
public class MatrixBean implements Serializable {

    private static final long serialVersionUID = -7546228293553844141L;

    private static final String UNSUCCESSFUL_PERMISSION_CHANGE = "Permissions could not be assigned.";
    private static final String SUCCESSFUL_PERMISSION_CHANGE = "Permissions assigned.";

    @ManagedProperty(value = "#{sessionPermissionsBean}")
    private SessionPermissionsBean sessionPermissionsBean;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @EJB
    private Roles rolesEJB;
    @EJB
    private Permissions permissionsEJB;
    @EJB
    private Resources resourcesEJB;

    private Map<Integer, String> roles;
    private Map<Integer, String> permissions;
    private Map<Integer, Map<Integer, Boolean>> rolePermissions;
    private Map<Integer, Resource> permissionResources;
    private List<Role> selectedRoles;

    // roles
    private List<Role> rolesSourceList;
    private List<Role> selectedRolesSourceList;
    private List<Role> rolesTargetList;
    private List<Role> selectedRolesTargetList;

    // resources
    private List<Resource> resourcesSourceList;
    private List<Resource> selectedResourcesSourceList;
    private List<Resource> resourcesTargetList;
    private List<Resource> selectedResourcesTargetList;

    /**
     * Initialises resources dual list model which data are shown in select resources dialog on
     * <code>/Roles/Matrix</code> page and roles dual list model which data are shown in select roles dialog on the same
     * page. Method is called post construct.
     */
    @PostConstruct
    private void initialize() {
        initializeResourcesDualList();
        initializeRolesDualList();
    }

    private void refreshRolesList() {
        List<Role> newRolesSourceList = rolesEJB.getRoles();
        updateList(newRolesSourceList, rolesSourceList, rolesTargetList);
    }

    private void refreshResourcesList() {
        List<Resource> newResourcesSourceList = resourcesEJB.getResources();
        updateList(newResourcesSourceList, resourcesSourceList, resourcesTargetList);
    }

    private <T extends NamedEntity> void updateList(List<T> newList, List<T> sourceList, List<T> targetList) {
        if (newList == null) {
            newList = new ArrayList<>();
        }
        //find out which entities were removed from the database
        List<T> removedEntities = new ArrayList<>();
        removedEntities.addAll(sourceList);
        removedEntities.addAll(targetList);
        for (T r : newList) {
            removedEntities.remove(r);
        }
        //find out which have been added: those will remain in the newList
        for (T r : sourceList) {
            newList.remove(r);
        }
        for (T r : targetList) {
            newList.remove(r);
        }
        //remove the ones that no longer exist
        if (!removedEntities.isEmpty()) {
            for (T r : removedEntities) {
                sourceList.remove(r);
                targetList.remove(r);
            }
        }
        //add the new ones to the source list
        for (T r : newList) {
            sourceList.add(r);
        }
        Collections.sort(sourceList,new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

    /**
     * Set session permission bean.
     *
     * @param sessionPermissionsBean session permission bean
     */
    public void setSessionPermissionsBean(SessionPermissionsBean sessionPermissionsBean) {
        this.sessionPermissionsBean = sessionPermissionsBean;
    }

    /**
     * Sets login bean used for checking if a user is logged in.
     *
     * @param loginBean the bean through which logged in user info is retrieved
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    /**
     * Initialises roles dual list model which data are shown in select roles dialog on <code>/Roles/Matrix</code> page.
     */
    private void initializeRolesDualList() {
        rolesSourceList = rolesEJB.getRoles();
        if (rolesSourceList == null) {
            rolesSourceList = new ArrayList<>();
        }
        rolesTargetList = new ArrayList<Role>();
    }

    /**
     * Initialises resources dual list model which data are shown in select resources dialog on
     * <code>/Roles/Matrix</code> page.
     */
    private void initializeResourcesDualList() {
        resourcesSourceList = resourcesEJB.getResources();
        if (resourcesSourceList == null) {
            resourcesSourceList = new ArrayList<>();
        }
        resourcesTargetList = new ArrayList<Resource>();
    }

    /**
     * Return a map with permission id as key and with string representation of permission as value. Value is used as
     * table row title on <code>/Roles/Matrix</code>.
     *
     * @return map with permission id as key and with string representation of permission as value.
     */
    public Map<Integer, String> getPermissions() {
        return permissions;
    }

    /**
     * Return a map with role id as key and with role name as value. Value is used as table column title on table on
     * <code>/Roles/Matrix</code>.
     *
     * @return map with role id as key and with role name as value.
     */
    public Map<Integer, String> getRoles() {
        return roles;
    }

    /**
     * Return generated roles permission map which data are shown in table on <code>/Roles/Matrix</code> page.
     *
     * @return role permissions map which data are shown on <code>/Roles/Matrix</code> page.
     */
    public Map<Integer, Map<Integer, Boolean>> getRolePermissions() {
        return rolePermissions;
    }

    /**
     * Creates a map which holds informations about permissions, key is permission id and value is string representation
     * of permission. Also creates a map which holds informations about permission resources, key is permission id,
     * value is resource.
     */
    public void createPermissionsMap() {
        permissions = new LinkedHashMap<>();
        permissionResources = new LinkedHashMap<>();
        for (Resource resource : resourcesTargetList) {
            for (Permission permission : permissionsEJB.getPermissionsByResource(resource.getId())) {
                permissions.put(permission.getId(), permission.getName() + " (" + permission.getResource().getName()
                        + ")");
                permissionResources.put(permission.getId(), permission.getResource());
            }
        }
        // initializeResourcesDualList(); do not reinitialise, we need to be able to remove resources as well
        createRolePermissionsMap();
    }

    /**
     * Creates a map which holds informations about roles. Key is role id and value is role name.
     */
    public void createRolesMap() {
        roles = new LinkedHashMap<>();
        selectedRoles = new ArrayList<>(rolesTargetList);
        for (Role role : rolesTargetList) {
            roles.put(role.getId(), role.getName());
        }
        // initializeRolesDualList(); do not reinitialise, we need to be able to remove resources as well
        createRolePermissionsMap();
    }

    /**
     * Creates a map which holds informations about roles and permissions. Role is a key for value which is
     * <code>Map&lt;Integer, Boolean&gt;</code>. <code>Map&lt;Integer, Boolean&gt;</code> key is permission id, value is
     * true if role contains this permission, otherwise is false.
     */
    public void createRolePermissionsMap() {
        if (roles != null && permissions != null) {
            rolePermissions = new LinkedHashMap<>();
            for (Integer role : roles.keySet()) {
                Map<Integer, Boolean> permissionMap = new LinkedHashMap<>();
                for (Integer permission : permissions.keySet()) {
                    permissionMap.put(permission, rolesEJB.containsPermission(role, permission));
                }
                rolePermissions.put(role, permissionMap);
            }
        }
    }

    /**
     * Method checks, if there were any unauthorised changes made and displays an error message if there were, or stores
     * the changes, if there were not. This method is called when submit button on <code>/Roles/Matrix</code> is
     * clicked. Results of the action are logged and shown to the user as growl message.
     */
    public void onSubmit() {
        if (!loginBean.isLoggedInValidated()) {
            return;
        }
        Set<Entry<Integer, Map<Integer, Boolean>>> rolePermissionsSet = rolePermissions.entrySet();
        Set<Resource> resources = new HashSet<>();
        for (Entry<Integer, Map<Integer, Boolean>> roleEntry : rolePermissionsSet) {
            // Determine if role's permissions have been changed.
            Role newRole = getRoleByID(roleEntry.getKey());
            Role oldRole = rolesEJB.getRole(newRole.getId(), false);
            Set<Entry<Integer, Boolean>> permissionSet = roleEntry.getValue().entrySet();
            for (Entry<Integer, Boolean> permissionEntry : permissionSet) {
                // Permissions set to true should have been present in the old role.
                boolean permissionChanged = true;
                for (Permission oldPermission : oldRole.getPermissions()) {
                    if (permissionEntry.getKey() == oldPermission.getId()) {
                        permissionChanged = false;
                        break;
                    }
                }
                if (permissionChanged == permissionEntry.getValue()) {
                    resources.add(permissionResources.get(permissionEntry.getKey()));
                }
            }
        }
        StringBuilder errorMessage = new StringBuilder(resources.size() * 50);
        for (Resource r : resources) {
            if (!sessionPermissionsBean.canManageResource(r)) {
                errorMessage.append("Can not edit resource ").append(r.getName()).append(".\n");
            }
        }
        // If there were any errors show them instead of proceeding.
        if (errorMessage.length() == 0) {
            for (Entry<Integer, Map<Integer, Boolean>> entry : rolePermissionsSet) {
                rolesEJB.addPermissions(entry.getKey(), entry.getValue());
            }
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, SUCCESSFUL_PERMISSION_CHANGE,
                            "All changes applied successfully."));
        } else {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, UNSUCCESSFUL_PERMISSION_CHANGE, errorMessage
                            .toString().trim()));
        }
    }

    /**
     * Method which is called when cancel button from matrix page is clicked. Results of the action are logged and shown
     * to the user as growl message.
     */
    public void onCancel() {
        createRolePermissionsMap();
    }

    /**
     * Returns list which contains roles from source list and is shown on add roles dialog on <code>/Roles/Matrix</code>
     * page.
     *
     * @return list which contains roles from source list.
     */
    public List<Role> getRolesSourceList() {
        return rolesSourceList;
    }

    /**
     * Sets list which contains roles from source list and is shown on add roles dialog on <code>/Roles/Matrix</code>
     * page.
     *
     * @param rolesSourceList list which contains roles from source list
     */
    public void setRolesSourceList(List<Role> rolesSourceList) {
        this.rolesSourceList = rolesSourceList;
    }

    /**
     * Returns list which contains roles from target list and is shown on add roles dialog on <code>/Roles/Matrix</code>
     * page.
     *
     * @return list which contains roles from target list.
     */
    public List<Role> getRolesTargetList() {
        return rolesTargetList;
    }

    /**
     * Sets list which contains roles from target list and is shown on add roles dialog on <code>/Roles/Matrix</code>
     * page.
     *
     * @param rolesTargetList list which contains roles from target list
     */
    public void setRolesTargetList(List<Role> rolesTargetList) {
        this.rolesTargetList = rolesTargetList;
    }

    /**
     * Returns list which contains selected roles from source list and is shown on add roles dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @return list which contains selected roles from source list.
     */
    public List<Role> getSelectedRolesSourceList() {
        return selectedRolesSourceList;
    }

    /**
     * Sets list which contains selected roles from source list and is shown on add roles dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @param selectedRolesSourceList list which contains selected roles from source list
     */
    public void setSelectedRolesSourceList(List<Role> selectedRolesSourceList) {
        this.selectedRolesSourceList = selectedRolesSourceList;
    }

    /**
     * Returns list which contains selected roles from target list and is shown on add roles dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @return list which contains selected roles from target list.
     */
    public List<Role> getSelectedRolesTargetList() {
        return selectedRolesTargetList;
    }

    /**
     * Sets list which contains selected roles from target list, and is shown on add roles dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @param selectedRolesTargetList list which contains selected roles from target list
     */
    public void setSelectedRolesTargetList(List<Role> selectedRolesTargetList) {
        this.selectedRolesTargetList = selectedRolesTargetList;
    }

    /**
     * Moves selected roles from source list to the target list. Lists are shown on the select roles dialog on
     * <code>/Roles/Matrix</code> page..
     */
    public void addRolesFromSourceToTarget() {
        if (!selectedRolesSourceList.isEmpty()) {
            rolesTargetList.addAll(selectedRolesSourceList);
        }
        rolesSourceList.removeAll(selectedRolesSourceList);
        selectedRolesSourceList.clear();
    }

    /**
     * Moves selected roles from target list to the source list. Lists are shown on the select roles dialog on
     * <code>/Roles/Matrix</code> page.
     */
    public void addRolesFromTargetToSource() {
        if (!selectedRolesTargetList.isEmpty()) {
            rolesSourceList.addAll(selectedRolesTargetList);
        }
        rolesTargetList.removeAll(selectedRolesTargetList);
        selectedRolesTargetList.clear();
    }

    /**
     * Moves all roles from source list to the target list. Lists are shown on the select roles dialog on
     * <code>/Roles/Matrix</code> page.
     */
    public void addAllRolesFromSourceToTarget() {
        if (!rolesSourceList.isEmpty()) {
            rolesTargetList.addAll(rolesSourceList);
        }
        rolesSourceList.clear();
        selectedRolesSourceList.clear();
    }

    /**
     * Moves all roles from target list to the source list. Lists are shown on the select roles dialog on
     * <code>/Roles/Matrix</code> page.
     */
    public void addAllRolesFromTargetToSource() {
        if (!rolesTargetList.isEmpty()) {
            rolesSourceList.addAll(rolesTargetList);
        }
        rolesTargetList.clear();
        selectedRolesTargetList.clear();
    }

    /**
     * Returns list which contains resources from source list and is shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @return list which contains resources from source list.
     */
    public List<Resource> getResourcesSourceList() {
        return resourcesSourceList;
    }

    /**
     * Sets list which contains resources from source list and is shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @param resourcesSourceList list which contains resources from source list
     */
    public void setResourcesSourceList(List<Resource> resourcesSourceList) {
        this.resourcesSourceList = resourcesSourceList;
    }

    /**
     * Returns list which contains resources from target list and is shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @return list which contains resources from target list.
     */
    public List<Resource> getResourcesTargetList() {
        return resourcesTargetList;
    }

    /**
     * Sets list which contains resources from target list and is shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @param resourcesTargetList list which contains resources from target list
     */
    public void setResourcesTargetList(List<Resource> resourcesTargetList) {
        this.resourcesTargetList = resourcesTargetList;
    }

    /**
     * Returns list which contains selected resources from source list and is shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @return list which contains selected resources from source list.
     */
    public List<Resource> getSelectedResourcesSourceList() {
        return selectedResourcesSourceList;
    }

    /**
     * Sets list which contains selected resources from source list and is shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @param selectedResourcesSourceList list which contains selected resources from source list
     */
    public void setSelectedResourcesSourceList(List<Resource> selectedResourcesSourceList) {
        this.selectedResourcesSourceList = selectedResourcesSourceList;
    }

    /**
     * Returns list which contains selected resources from target list and is shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @return list which contains selected resources from target list.
     */
    public List<Resource> getSelectedResourcesTargetList() {
        return selectedResourcesTargetList;
    }

    /**
     * Sets list which contains selected resources from target list, and is shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     *
     * @param selectedResourcesTargetList list which contains selected resources from target list
     */
    public void setSelectedResourcesTargetList(List<Resource> selectedResourcesTargetList) {
        this.selectedResourcesTargetList = selectedResourcesTargetList;
    }

    /**
     * Moves selected resources from source list to the target list. Lists are shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     */
    public void addResourcesFromSourceToTarget() {
        if (!selectedResourcesSourceList.isEmpty()) {
            resourcesTargetList.addAll(selectedResourcesSourceList);
        }
        resourcesSourceList.removeAll(selectedResourcesSourceList);
        selectedResourcesSourceList.clear();
    }

    /**
     * Moves selected resources from target list to the source list. Lists are shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     */
    public void addResourcesFromTargetToSource() {
        if (!selectedResourcesTargetList.isEmpty()) {
            resourcesSourceList.addAll(selectedResourcesTargetList);
        }
        resourcesTargetList.removeAll(selectedResourcesTargetList);
        selectedResourcesTargetList.clear();
    }

    /**
     * Moves all resources from source list to the target list. Lists are shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     */
    public void addAllResourcesFromSourceToTarget() {
        if (!resourcesSourceList.isEmpty()) {
            resourcesTargetList.addAll(resourcesSourceList);
        }
        resourcesSourceList.clear();
        selectedResourcesSourceList.clear();
    }

    /**
     * Moves all resources from target list to the source list. Lists are shown on select resources dialog on
     * <code>/Roles/Matrix</code> page.
     */
    public void addAllResourcesFromTargetToSource() {
        if (!resourcesTargetList.isEmpty()) {
            resourcesSourceList.addAll(resourcesTargetList);
        }
        resourcesTargetList.clear();
        selectedResourcesTargetList.clear();
    }

    /**
     * Returns role which is identified by given role identifier.
     *
     * @param roleID unique role identifier
     *
     * @return role which is identified by given role identifier.
     */
    private Role getRoleByID(Integer roleID) {
        for (Role role : selectedRoles) {
            if (role.getId() == roleID) {
                return role;
            }
        }
        return null;
    }

    /**
     * Method that shows the roles dialog.
     */
    public void showRolesDialog() {
        refreshRolesList();
        PrimeFaces.current().ajax().update("selectRolesForm");
        PrimeFaces.current().executeScript("PF('selectRolesDialog').show()");
    }

    /**
     * Method that shows the roles dialog.
     */
    public void showResourcesDialog() {
        refreshResourcesList();
        PrimeFaces.current().ajax().update("selectResourcesForm");
        PrimeFaces.current().executeScript("PF('selectResourcesDialog').show()");
    }

}
