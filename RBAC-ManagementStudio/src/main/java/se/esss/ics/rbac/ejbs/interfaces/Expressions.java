/* 
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.ejbs.interfaces;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import se.esss.ics.rbac.datamodel.Expression;

/**
 * <code>Expressions</code> interface defines methods for dealing with expressions.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@Local
public interface Expressions extends Serializable {

    /**
     * Retrieves expression identified by <code>expressionId</code> from database and returns it.
     * 
     * @param expressionId unique expression identifier
     * 
     * @return specific expression identified by id.
     */
    Expression getExpression(int expressionId);

    /**
     * @return list of the expressions retrieved from database.
     */
    List<Expression> getExpressions();

    /**
     * Retrieves all expressions whose name matches the <code>wildcard</code> pattern from database and returns it.
     * 
     * @param wildcard wildcard pattern
     * 
     * @return list of expressions whose name matches the wildcard pattern.
     */
    List<Expression> getExpressionsByWildcard(String wildcard);

    /**
     * Inserts created expression into database.
     * 
     * @param expression created expression
     */
    void createExpression(Expression expression);

    /**
     * Removes expression from database.
     * 
     * @param expression expression which will be removed from database
     */
    void removeExpression(Expression expression);

    /**
     * Updates expression.
     * 
     * @param expression updated expression
     */
    void updateExpression(Expression expression);
}
