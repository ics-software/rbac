/* ******************************************************************************
 * Copyright (c) 2010 Oak Ridge National Laboratory.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 ******************************************************************************/
package org.csstudio.utility.pv;

/**
 * 
 * <code>PVException</code> is an exception thrown when there is an error handling the PVs.
 * 
 */
public class PVException extends Exception {

    private static final long serialVersionUID = -5727248636247672446L;

    private final String pvName;
    
    /**
     * Constructs a new PVException.
     * 
     * @param pvName the name of the PV that triggered this exception
     * @param message the exception message
     */
    public PVException(String pvName, String message) {
        super(message);
        this.pvName = pvName;
    }

    /**
     * Construct a new exception.
     * 
     * @param pvName the name of the PV that triggered this exception
     * @param message the exception message
     * @param cause the cause of the exception
     */
    public PVException(String pvName, String message, Throwable cause) {
        super(message, cause);
        this.pvName = pvName;
    }
    
    /**
     * Returns the name of the PV that triggered this exception.
     * 
     * @return the pvName
     */
    public String getPVName() {
        return pvName;
    }

}
