/* 
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values.internal;

import java.util.Objects;

import org.csstudio.data.values.INumericMetaData;

/**
 * Implementation of {@link INumericMetaData}.
 * 
 * @see INumericMetaData
 * @author Kay Kasemir
 */
public class NumericMetaData implements INumericMetaData {
    private final double dispLow;
    private final double dispHigh;
    private final double warnLow;
    private final double warnHigh;
    private final double alarmLow;
    private final double alarmHigh;
    private final int prec;
    private final String units;

    /**
     * Constructs a new numeric meta data from pieces.
     * 
     * @param dispLow the displayer low value (LOPR)
     * @param dispHigh the displayer high value (HOPR)
     * @param warnLow the warning low limit (LOW)
     * @param warnHigh the warning high limit (HIGH)
     * @param alarmLow the alarm low limit (LOLO)
     * @param alarmHigh the alarm high limit (HIHI)
     * @param prec the precision (PREC)
     * @param units the engineering units (EGU)
     */
    public NumericMetaData(double dispLow, double dispHigh, double warnLow, double warnHigh, double alarmLow,
            double alarmHigh, int prec, String units) {
        this.dispLow = dispLow;
        this.dispHigh = dispHigh;
        this.warnLow = warnLow;
        this.warnHigh = warnHigh;
        this.alarmLow = alarmLow;
        this.alarmHigh = alarmHigh;
        this.prec = prec;
        this.units = units;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.INumericMetaData#getDisplayLow()
     */
    @Override
    public double getDisplayLow() {
        return dispLow;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.INumericMetaData#getDisplayHigh()
     */
    @Override
    public double getDisplayHigh() {
        return dispHigh;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.INumericMetaData#getWarnLow()
     */
    @Override
    public double getWarnLow() {
        return warnLow;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.INumericMetaData#getWarnHigh()
     */
    @Override
    public double getWarnHigh() {
        return warnHigh;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.INumericMetaData#getAlarmLow()
     */
    @Override
    public double getAlarmLow() {
        return alarmLow;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.INumericMetaData#getAlarmHigh()
     */
    @Override
    public double getAlarmHigh() {
        return alarmHigh;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.INumericMetaData#getPrecision()
     */
    @Override
    public int getPrecision() {
        return prec;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.csstudio.platform.data.INumericMetaData#getUnits()
     */
    @Override
    public String getUnits() {
        return units;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        long temp = Double.doubleToLongBits(alarmHigh);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(alarmLow);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(dispHigh);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(dispLow);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + prec;
        result = prime * result + ((units == null) ? 0 : units.hashCode());
        temp = Double.doubleToLongBits(warnHigh);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(warnLow);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        NumericMetaData other = (NumericMetaData) obj;
        if (Double.doubleToLongBits(alarmHigh) != Double.doubleToLongBits(other.alarmHigh)) {
            return false;
        } else if (Double.doubleToLongBits(alarmLow) != Double.doubleToLongBits(other.alarmLow)) {
            return false;
        } else if (Double.doubleToLongBits(dispHigh) != Double.doubleToLongBits(other.dispHigh)) {
            return false;
        } else if (Double.doubleToLongBits(dispLow) != Double.doubleToLongBits(other.dispLow)) {
            return false;
        } else if (Double.doubleToLongBits(warnHigh) != Double.doubleToLongBits(other.warnHigh)) {
            return false;
        } else if (Double.doubleToLongBits(warnLow) != Double.doubleToLongBits(other.warnLow)) {
            return false;
        } else if (prec != other.prec) {
            return false;
        }
        return Objects.equals(units, other.units);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new StringBuilder(250).append("NumericMetaData:\n    units     :").append(units)
                .append("\n    prec      :").append(prec).append("\n    dispLow   :").append(dispLow)
                .append("\n    dispHigh  :").append(dispHigh).append("\n    alarmLow  :").append(alarmLow)
                .append("\n    warnLow   :").append(warnLow).append("\n    warnHigh  :").append(warnHigh)
                .append("\n    alarmHigh :").append(alarmHigh).toString();
    }
}
