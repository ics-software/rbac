/* 
 * Copyright (c) 2008 Stiftung Deutsches Elektronen-Synchrotron, 
 * Member of the Helmholtz Association, (DESY), HAMBURG, GERMANY.
 *
 * THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN "../AS IS" BASIS. 
 * WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND 
 * NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR 
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE 
 * IN ANY RESPECT, THE USER ASSUMES THE COST OF ANY NECESSARY SERVICING, REPAIR OR 
 * CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. 
 * NO USE OF ANY SOFTWARE IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 * DESY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, 
 * OR MODIFICATIONS.
 * THE FULL LICENSE SPECIFYING FOR THE SOFTWARE THE REDISTRIBUTION, MODIFICATION, 
 * USAGE AND OTHER RIGHTS AND OBLIGATIONS IS INCLUDED WITH THE DISTRIBUTION OF THIS 
 * PROJECT IN THE FILE LICENSE.HTML. IF THE LICENSE IS NOT INCLUDED YOU MAY FIND A COPY 
 * AT HTTP://WWW.DESY.DE/LEGAL/LICENSE.HTM
 */
package org.csstudio.data.values;

/**
 * Base interface for all control system values.
 * <p>
 * The <code>Value</code> handles all the other 'stuff' that comes with a control system value except for the actual
 * value itself:
 * <ul>
 * <li>Time stamp
 * <li>A severity code that indicates if the data reflects some sort of warning or error state.
 * <li>A status string that explains the severity.
 * <li>Meta data that might be useful for displaying the data.
 * </ul>
 * 
 * It also offers convenience routines for displaying values.
 * <p>
 * In some cases, that's already all that there might be, because a sample can have a severity that indicates that there
 * was no value, and the status describes why.
 * <p>
 * In most cases, however, access to the actual data requires the specific subtypes <code>DoubleValue</code>,
 * <code>StringValue</code> etc.
 * 
 * @see IDoubleValue
 * @see ILongValue
 * @see IStringValue
 * @see IEnumeratedValue
 * @author Kay Kasemir
 */
public interface IValue {

    /** @see #format(Format, int) */
    public static enum Format {
        /** Use all the MetaData information. */
        DEFAULT,

        /** If possible, use decimal representation. */
        DECIMAL,

        /** If possible, use exponential notation. */
        EXPONENTIAL,

        /** If possible, convert to String */
        STRING,

        /** If possible, use decimal representation with minimum possible decimal digits */
        ADAPTIVE_DECIMAL
    }

    /**
     * Get the time stamp.
     * 
     * @return The time stamp.
     */
    ITimestamp getTime();

    /**
     * Get the severity info.
     * 
     * @see ISeverity
     * @see #getStatus()
     * @return The severity info.
     */
    ISeverity getSeverity();

    /**
     * Get the status text that might describe the severity.
     * 
     * @see #getSeverity()
     * @return The status string.
     */
    String getStatus();

    /**
     * Meta Data that helps with using the value, mostly for formatting.
     * <p>
     * It might be OK for some value types to only have <code>null</code> MetaData, while others might require a
     * specific one like <code>NumericMetaData</code>.
     * 
     * @return The Meta Data.
     */
    IMetaData getMetaData();

    /**
     * Format the value as a string.
     * <p>
     * This means only the numeric or string value. Not the timestamp, not the severity and status.
     * <p>
     * 
     * @param how Detail on how to format.
     * @param precision Might be used by some format types to select for example the number of digits after the decimal
     *            point. A precision of '-1' might select the default-precision obtained from the MetaData.
     * @return This Value's value as a string.
     */
    String format(Format how, int precision);

    /**
     * Format the value via the Default format. Typically this means: using the meta data.
     * <p>
     * 
     * @return This Value's value as a string.
     * @see #format(Format, int)
     */
    String format();
}
