/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.jaxb.util;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * <code>NameValuePair</code> is a wrapper object for map entry. It defines XML structure of the entry using the JAXB
 * annotations to allow persisting the entry to XML.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 * 
 */
public class NameValuePair implements Serializable {

    private static final long serialVersionUID = -1228896798054011734L;
    private String name;
    private String value;

    /**
     * Constructs a new name value pair.
     */
    public NameValuePair() {
        // EMPTY
    }

    /**
     * Constructs a new name value pair.
     * 
     * @param name the name
     * @param value the value
     */
    public NameValuePair(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * @return the name of the element
     */
    @XmlAttribute(name = "name")
    public String getName() {
        return name;
    }

    /**
     * Sets a new name for this element.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value of the element
     */
    @XmlValue
    public String getValue() {
        return value;
    }

    /**
     * Sets a new value of the element.
     * 
     * @param value the value
     */
    public void setValue(String value) {
        this.value = value;
    }
}
