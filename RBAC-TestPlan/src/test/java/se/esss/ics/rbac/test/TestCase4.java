/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertNotNull;

import java.awt.Dimension;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.test.internal.Base;
import se.esss.ics.rbac.test.internal.XMLAnswerListener;
import se.esss.ics.rbac.test.internal.XMLSecurityFacade;

/**
 * 
 * <code>TestCase4: Web Services</code> verifies that the service is available via https protocol
 * and displays the raw messages received from the RESTful services.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase4 extends Base {

    private static final String SERVICE_URL = "https://rbac.esss.lu.se/service";
    private static final String HOST = "jboss01.esss.lu.se";
    private static final int PORT = 443;

//    private static final String SERVICE_URL = "https://localhost:8443/service";
//    private static final String HOST = "localhost";
//    private static final int PORT = 8443;
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        System.setProperty(RBACProperties.KEY_PRIMARY_SERVICES_URL, SERVICE_URL);
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_HOST, HOST);
        System.setProperty(RBACProperties.KEY_PRIMARY_SSL_PORT, String.valueOf(PORT));
    }

    /**
     * Test that the service uses https protocol.
     */
    @Test
    public void rbac248() throws SecurityFacadeException {
        String version = SecurityFacade.getDefaultInstance().getRBACVersion();
        assertNotNull("Version should not be null, which means we have successfully connected via https.", version);
    }
    
    /**
     * Displays the XML response of the restful methods.
     */
    @Test
    public void rbac129XML() throws SecurityFacadeException {
        XMLSecurityFacade facade = new XMLSecurityFacade();
        
        facade.setAnswerListener(new Answer("POST auth/token Response"));
        Token t = facade.authenticate();
        
        facade.setAnswerListener(new Answer("GET auth/{username}/role Response"));
        facade.getRolesForUser(username);
        
        facade.setAnswerListener(new Answer("GET auth/token/{token_id} Response"));
        facade.setToken("abc".toCharArray());
        facade.setToken(t.getTokenID());
        facade.getToken();
        
        facade.setAnswerListener(new Answer("POST auth/token/{token_id}/renew Response"));
        facade.renewToken();
                
        facade.setAnswerListener(new Answer("GET auth/version Response"));
        facade.getRBACVersion();
        
        facade.setAnswerListener(new Answer("GET auth/token/{token_id}/RBACManagementStudio/InvalidateTokens,ManageASG"));
        facade.hasPermissions("RBACManagementStudio", "InvalidateTokens", "ManageASG");
        
        facade.setAnswerListener(new Answer("POST auth/token/{token_id}/RBACManagementStudio/InvalidateTokens/exclusive"));
        facade.requestExclusiveAccess("RBACManagementStudio", "InvalidateTokens", 20);
    }
    
    /**
     * Test the error messages of the failed calls to the RESTful services.
     */
    @Test
    public void rbac129ErrorResponses() throws SecurityFacadeException {
        
        XMLSecurityFacade facade = new XMLSecurityFacade();
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials("foo", "bar".toCharArray());
            }
        });
        
        facade.setAnswerListener(new Answer("Login attempt with invalid username",true));
        facade.authenticate();
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, "bar".toCharArray());
            }
        });
        facade.setAnswerListener(new Answer("Login attempt with invalid password",true));
        facade.authenticate();
                       
        facade.setAnswerListener(new Answer("Get invalid token",true));
        facade.setToken("abc".toCharArray());
        facade.getToken();
        
        facade.setAnswerListener(new Answer("Renew invalid token",true));
        facade.renewToken();
          
        facade.setAnswerListener(null);
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
        facade.authenticate();
        facade.setAnswerListener(new Answer("Request permission for unknown resource",true));
        facade.hasPermissions("foo", "InvalidateTokens");
        
        facade.setAnswerListener(new Answer("Request unknown permission",true));
        facade.hasPermissions("RBACManagementStudio", "foobar");
              
        facade.setAnswerListener(new Answer("Request invalid exclusive access",true));
        facade.requestExclusiveAccess("RBACManagementStudio", "ManageRole", 20);
        
        facade.setAnswerListener(null);
        facade.requestExclusiveAccess("RBACManagementStudio", "InvalidateTokens", 20);
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials("rbactester2", password);
            }
        });
        facade.authenticate();
        facade.setAnswerListener(new Answer("Release invalid exclusive access",true));
        facade.releaseExclusiveAccess("RBACManagementStudio", "InvalidateTokens");
    }
    
    private static class Answer implements XMLAnswerListener {
        
        private final String title;
        private final boolean plainText;
        
        Answer(String title) {
            this(title,false);
        }
        
        Answer(String title, boolean plainText) {
            this.title = title;
            this.plainText = plainText;
        }

        @Override
        public void answerReceived(String message) {
            display(message, title);            
        }
        
        private void display(String message, String title) {
            JTextArea area = new JTextArea();
            area.setEditable(false);
            area.setLineWrap(true);
            area.setWrapStyleWord(true);
            if (plainText) {
                area.setText(message);
            } else {
                area.setText(format(message));
            }
            area.setMinimumSize(new Dimension(500,300));
            area.setPreferredSize(new Dimension(500,300));
                            
            JOptionPane.showMessageDialog(null, area, title, JOptionPane.INFORMATION_MESSAGE);
        }
        
        private static String format(String unformattedXml) {
            try {
                final Document document = parseXmlFile(unformattedXml);

                OutputFormat format = new OutputFormat(document);
                format.setLineWidth(65);
                format.setIndenting(true);
                format.setIndent(2);
                Writer out = new StringWriter();
                XMLSerializer serializer = new XMLSerializer(out, format);
                serializer.serialize(document);

                return out.toString();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private static Document parseXmlFile(String in) {
            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource(new StringReader(in));
                return db.parse(is);
            } catch (ParserConfigurationException e) {
                throw new RuntimeException(e);
            } catch (SAXException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
}
