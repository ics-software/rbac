/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.test.internal.Base;

/**
 * 
 * <code>TestCase1: Authentication</code> verifies requirements RBAC-100 and RBAC-110.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase1 extends Base {

    /**
     * Test that no special hardware is required for authentication.
     */
    @Test
    public void rbac100() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
        // user should be successfully authenticated
        Token token = facade.authenticate();
        assertNotNull("Security token was received", token);
        assertNotNull("Token is signed", token.getRBACSignature());
    }

    /**
     * Test that authentication is based on the username and password. This test provides the proper username and
     * password to the SecurityFacade during authentication. It later checks that username and password were requested
     * by the facade and verifies that the received token matches the given username. The process is repeated for
     * invalid username and or password (foo and bar).
     */
    @Test
    public void rbac110() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        final boolean[] credentialsRequested = new boolean[1];
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                credentialsRequested[0] = true;
                return new Credentials(username, password);
            }
        });
        Token token = facade.authenticate();
        assertNotNull("Security token was received", token);
        assertEquals("Username of the token should be the same as the authenticated username", username,
                token.getUsername());
        assertTrue("Username and password was requested", credentialsRequested[0]);

        facade.logout();

        // test with invalid username and password
        credentialsRequested[0] = false;
        facade.setDefaultSecurityCallback(new SecurityCallbackAdapter() {
            @Override
            public Credentials getCredentials() {
                credentialsRequested[0] = true;
                return new Credentials("foo", "bar".toCharArray());
            }
        });
        try {
            token = facade.authenticate();
            fail("Authentication should fail due to incorrect username and or password");
        } catch (SecurityFacadeException e) {
            assertNotNull("Exception expected", e);
        }
        assertTrue("Username and password was requested", credentialsRequested[0]);
    }
}
