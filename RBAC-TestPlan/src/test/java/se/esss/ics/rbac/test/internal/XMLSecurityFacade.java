/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test.internal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXB;

import se.esss.ics.rbac.test.UsernameAndPassword;
import se.esss.ics.rbac.access.AutoLogout;
import se.esss.ics.rbac.access.ConnectionExecutionTask;
import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ExclusiveAccess;
import se.esss.ics.rbac.access.FacadeUtilities;
import se.esss.ics.rbac.access.IConnectionFactory;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.SecurityFacadeListener;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.access.impl.RBACConnectionFactory;
import se.esss.ics.rbac.jaxb.ExclusiveInfo;
import se.esss.ics.rbac.jaxb.PermissionsInfo;
import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * 
 * <code>XMLSecurityFacade</code> calls 
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 *
 */
public final class XMLSecurityFacade implements ISecurityFacade {

    private static final long serialVersionUID = -7657682296833934173L;

    private static final String AUTH_TOKEN = "/auth/token/";

    private static final Map<String, String> NO_PARAMETERS_MAP = new HashMap<>(0);

    private final IConnectionFactory connectionFactory;
    private SecurityCallback defaultSecurityCallback;
    
    private XMLAnswerListener answerListener;

    private String localhostIP;
    private Token token;
    private char[] tokenID;

    /**
     * Construct new security facade using the {@link RBACConnectionFactory}.
     */
    public XMLSecurityFacade() {
        connectionFactory = RBACConnectionFactory.getInstance();
        defaultSecurityCallback = new SecurityCallbackAdapter() {
            /* @see se.esss.ics.rbac.access.SecurityCallbackAdapter#getCredentials() */
            @Override
            public Credentials getCredentials() {
                return new Credentials(UsernameAndPassword.username,UsernameAndPassword.password);
            }
        };
    }


    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#authenticate()
     */
    @Override
    public Token authenticate() throws SecurityFacadeException {
        Credentials credentials = getDefaultSecurityCallback().getCredentials();
        return credentials == null ? null : authenticateInternally(credentials, getDefaultSecurityCallback());
    }

    private Token authenticateInternally(Credentials credentials, SecurityCallback callback)
            throws SecurityFacadeException {
        Map<String, String> parameters = new LinkedHashMap<>(3);
        try {
            byte[] usernameData = credentials.getUsername().getBytes(FacadeUtilities.CHARSET);
            ByteBuffer buffer = FacadeUtilities.CHARSET.newEncoder().encode(CharBuffer.wrap(credentials.getPassword()));
            byte[] passwordData = Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.limit());
            byte[] rawAuthData = new byte[usernameData.length + passwordData.length + 1];
            System.arraycopy(usernameData, 0, rawAuthData, 0, usernameData.length);
            rawAuthData[usernameData.length] = (byte) ':';
            System.arraycopy(passwordData, 0, rawAuthData, usernameData.length + 1, passwordData.length);
            parameters.put(FacadeUtilities.HEADER_AUTHORISATION,
                    "BASIC " + DatatypeConverter.printBase64Binary(rawAuthData));
        } catch (CharacterCodingException e) {
            throw new SecurityFacadeException("Could not authenticate.",e,0);
        }
        parameters.put(FacadeUtilities.HEADER_IP, credentials.getIP() == null ? getLocalhostIP() : credentials.getIP());
        parameters.put(FacadeUtilities.HEADER_ROLES, credentials.getPreferredRole());
        return execute(callback, AUTH_TOKEN, FacadeUtilities.POST, parameters,
                new ConnectionExecutionTask<Token, SecurityFacadeException>() {
                    @Override
                    public Token execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.CREATED) {
                            String answer = readAndSendAnswer(connection.getInputStream());
                            Token bToken = new Token(JAXB.unmarshal(new StringReader(answer), TokenInfo.class));
                            setTokenInternal(bToken);
                            return bToken;
                        } else {
                            readAndSendAnswer(connection.getErrorStream());
                            return null;
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#authenticate(se.esss.ics.rbac.access.SecurityCallback)
     */
    @Override
    public void authenticate(SecurityCallback callback) {
        throw new UnsupportedOperationException();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#logout()
     */
    @Override
    public boolean logout() throws SecurityFacadeException {
        return logoutInternally(getLocalToken(), getDefaultSecurityCallback());
    }

    private boolean logoutInternally(final Token token, SecurityCallback callback) throws SecurityFacadeException {
        if (token == null) {
            return false;
        }

        String url = new StringBuilder(50).append(AUTH_TOKEN).append(token.getTokenID()).toString();
        return execute(callback, url, FacadeUtilities.DELETE, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Boolean, SecurityFacadeException>() {
                    @Override
                    public Boolean execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        int respondeCode = connection.getResponseCode();
                        if (respondeCode == FacadeUtilities.DELETED || respondeCode == FacadeUtilities.OK) {
                            setTokenInternal(null);
                            return respondeCode == FacadeUtilities.DELETED;
                        } else {
                            return false;
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#logout(se.esss.ics.rbac.access.SecurityCallback)
     */
    @Override
    public void logout(SecurityCallback callback) {
        throw new UnsupportedOperationException();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getToken()
     */
    @Override
    public Token getToken() throws SecurityFacadeException {
        if (token == null && tokenID != null) {
            String url = new StringBuilder(50).append(AUTH_TOKEN).append(tokenID).toString();
            return execute(getDefaultSecurityCallback(), url, FacadeUtilities.GET, NO_PARAMETERS_MAP,
                    new ConnectionExecutionTask<Token, SecurityFacadeException>() {
                        @Override
                        public Token execute(HttpURLConnection connection) throws IOException, DataBindingException,
                                SecurityFacadeException {
                            if (connection.getResponseCode() == FacadeUtilities.OK) {
                                String answer = readAndSendAnswer(connection.getInputStream());
                                Token aToken = new Token(JAXB.unmarshal(new StringReader(answer), TokenInfo.class));
                                setTokenInternal(aToken);
                                return aToken;
                            } else {
                                readAndSendAnswer(connection.getErrorStream());
                                return null;
                            }
                        }
                    });
        }
        return token;
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#isTokenValid()
     */
    @Override
    public boolean isTokenValid() throws SecurityFacadeException {
        return isTokenValidInternally(getLocalToken());
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#isTokenValid(se.esss.ics.rbac.access.SecurityCallback)
     */
    @Override
    public void isTokenValid(SecurityCallback callback) {
        throw new UnsupportedOperationException();
    }

    /**
     * Checks if the provided token is valid and returns true if the service says it is valid or false if it says it is
     * not.
     * 
     * @param token the token to check
     * @return true if the token is valid or false otherwise
     * @throws SecurityFacadeException if there was an error in validation
     */
    private boolean isTokenValidInternally(Token token) throws SecurityFacadeException {
        if (token == null) {
            return false;
        }
        String url = new StringBuilder(60).append(AUTH_TOKEN).append(token.getTokenID()).append("/isvalid").toString();
        return execute(getDefaultSecurityCallback(), url, FacadeUtilities.GET, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Boolean, SecurityFacadeException>() {
                    @Override
                    public Boolean execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            readAndSendAnswer(connection.getInputStream());
                            return Boolean.TRUE;
                        } else if (connection.getResponseCode() == FacadeUtilities.ERROR
                                || connection.getResponseCode() == FacadeUtilities.BAD_REQUEST) {
                            readAndSendAnswer(connection.getErrorStream());
                            return false;
                        } else {
                            readAndSendAnswer(connection.getErrorStream());
                            return Boolean.FALSE;
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getLocalToken()
     */
    @Override
    public Token getLocalToken() {
        if (token != null) {
            return token;
        } else if (tokenID != null) {
            return new Token(tokenID);
        }
        return null;
    }

    private void setTokenInternal(Token token) {
        this.token = token;
        if (this.token == null) {
            this.tokenID = null;
        } else {
            this.tokenID = this.token.getTokenID();
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#setToken(char[])
     */
    @Override
    public void setToken(char[] tokenID) {
        this.tokenID = tokenID == null ? null : Arrays.copyOf(tokenID, tokenID.length);
        if (token != null && !Arrays.equals(token.getTokenID(), tokenID)) {
            token = null;
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#hasPermission(java.lang.String, java.lang.String)
     */
    @Override
    public boolean hasPermission(String resource, String permission) throws SecurityFacadeException,
            IllegalArgumentException {
        return hasPermissions(resource, new String[] { permission }).get(permission).booleanValue();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#hasPermissions(java.lang.String, java.lang.String[])
     */
    @Override
    public Map<String, Boolean> hasPermissions(String resource, String... permissions) throws SecurityFacadeException,
            IllegalArgumentException {
        return hasPermissionsInternally(getLocalToken(), resource, permissions, getDefaultSecurityCallback());
    }

    private Map<String, Boolean> hasPermissionsInternally(Token token, String resource, String[] permissions,
            SecurityCallback callback) throws SecurityFacadeException, IllegalArgumentException {
        StringBuilder urlBuilder = new StringBuilder(80 + permissions.length * 30).append(AUTH_TOKEN)
                .append(token.getTokenID()).append('/').append(resource).append('/');
        urlBuilder.append(permissions[0]);
        for (int i = 1; i < permissions.length; i++) {
            urlBuilder.append(',');
            urlBuilder.append(permissions[i]);
        }
        return execute(callback, urlBuilder.toString(), FacadeUtilities.GET, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Map<String, Boolean>, SecurityFacadeException>() {
                    @Override
                    public Map<String, Boolean> execute(HttpURLConnection connection) throws IOException,
                            DataBindingException, SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            String answer = readAndSendAnswer(connection.getInputStream());
                            return JAXB.unmarshal(new StringReader(answer), PermissionsInfo.class).getPermissions();
                        } else {
                            readAndSendAnswer(connection.getErrorStream());
                            return null;
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#hasPermissions(se.esss.ics.rbac.access.SecurityCallback,
     * java.lang.String, java.lang.String[])
     */
    @Override
    public void hasPermissions(SecurityCallback callback, final String resource, final String... permissions) {
        throw new UnsupportedOperationException();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getRolesForUser(java.lang.String)
     */
    @Override
    public String[] getRolesForUser(String username) throws SecurityFacadeException, IllegalArgumentException {
        return getRolesForUserInternally(username, getDefaultSecurityCallback());
    }

    private String[] getRolesForUserInternally(String username, SecurityCallback callback)
            throws SecurityFacadeException, IllegalArgumentException {
        String url = new StringBuilder(30).append("/auth/").append(username).append("/role").toString();

        return execute(callback, url, FacadeUtilities.GET, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<String[], SecurityFacadeException>() {
                    @Override
                    public String[] execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            String answer = readAndSendAnswer(connection.getInputStream());
                            RolesInfo rolesInfo = JAXB.unmarshal(new StringReader(answer), RolesInfo.class);
                            return rolesInfo.getRoleNames().toArray(new String[rolesInfo.getRoleNames().size()]);
                        } else {
                            readAndSendAnswer(connection.getErrorStream());
                            return null;
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getRolesForUser(se.esss.ics.rbac.access.SecurityCallback,
     * java.lang.String)
     */
    @Override
    public void getRolesForUser(SecurityCallback callback, final String username) {
        throw new UnsupportedOperationException();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#requestExclusiveAccess(java.lang.String, java.lang.String, int)
     */
    @Override
    public ExclusiveAccess requestExclusiveAccess(String resource, String permission, int durationInMinutes)
            throws SecurityFacadeException, IllegalArgumentException {
        return requestExclusiveAccessInternally(getLocalToken(), resource, permission, durationInMinutes,
                getDefaultSecurityCallback());
    }

    private ExclusiveAccess requestExclusiveAccessInternally(Token token, String resource, String permission,
            int durationInMinutes, SecurityCallback callback) throws SecurityFacadeException, IllegalArgumentException {
        String url = new StringBuilder(150).append(AUTH_TOKEN).append(token.getTokenID()).append('/').append(resource)
                .append('/').append(permission).append("/exclusive").toString();
        Map<String, String> parameters = new HashMap<>(1);
        if (durationInMinutes >= 1) {
            parameters.put(FacadeUtilities.HEADER_EXCLUSIVE_DURATION, String.valueOf(durationInMinutes * 60000));
        }
        return execute(callback, url, FacadeUtilities.POST, parameters,
                new ConnectionExecutionTask<ExclusiveAccess, SecurityFacadeException>() {
                    @Override
                    public ExclusiveAccess execute(HttpURLConnection connection) throws IOException,
                            DataBindingException, SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.CREATED) {
                            String answer = readAndSendAnswer(connection.getInputStream());
                            ExclusiveInfo info = JAXB.unmarshal(new StringReader(answer), ExclusiveInfo.class);
                            return new ExclusiveAccess(info);
                        } else {
                            readAndSendAnswer(connection.getErrorStream());
                            return null;
                        }
                    }
                });
    }

    /*
     * @see
     * se.esss.ics.rbac.access.ISecurityFacade#requestExclusiveAccess(se.esss.ics.rbac.access.SecurityCallback,
     * java.lang.String, java.lang.String, int)
     */
    @Override
    public void requestExclusiveAccess(SecurityCallback callback, final String resource, final String permission,
            final int durationInMinutes) {
        throw new UnsupportedOperationException();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#releaseExclusiveAccess(java.lang.String, java.lang.String)
     */
    @Override
    public boolean releaseExclusiveAccess(String resource, String permission) throws SecurityFacadeException,
            IllegalArgumentException {
        return releaseExclusiveAccessInternally(getLocalToken(), resource, permission, getDefaultSecurityCallback());
    }

    private boolean releaseExclusiveAccessInternally(Token token, String resource, String permission,
            SecurityCallback callback) throws SecurityFacadeException, IllegalArgumentException {
        String url = new StringBuilder(150).append(AUTH_TOKEN).append(token.getTokenID()).append('/').append(resource)
                .append('/').append(permission).append("/exclusive").toString();
        return execute(callback, url, FacadeUtilities.DELETE, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Boolean, SecurityFacadeException>() {
                    @Override
                    public Boolean execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.DELETED) {
                            readAndSendAnswer(connection.getInputStream());
                            return Boolean.TRUE;
                        } else if (connection.getResponseCode() == FacadeUtilities.OK) {
                            readAndSendAnswer(connection.getInputStream());
                            return Boolean.FALSE;
                        } else {
                            readAndSendAnswer(connection.getErrorStream());
                            return false;
                        }
                    }
                });
    }

    /*
     * @see
     * se.esss.ics.rbac.access.ISecurityFacade#releaseExclusiveAccess(se.esss.ics.rbac.access.SecurityCallback,
     * java.lang.String, java.lang.String)
     */
    @Override
    public void releaseExclusiveAccess(SecurityCallback callback, final String resource, final String permission) {
        throw new UnsupportedOperationException();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getRBACVersion()
     */
    @Override
    public String getRBACVersion() throws SecurityFacadeException {
        return execute(getDefaultSecurityCallback(), "/auth/version", FacadeUtilities.GET, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<String, SecurityFacadeException>() {
                    @Override
                    public String execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            return readAndSendAnswer(connection.getInputStream());
                        } else {
                            readAndSendAnswer(connection.getErrorStream());
                            return null;
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#renewToken()
     */
    @Override
    public Token renewToken() throws SecurityFacadeException {
        return renewTokenInternally(getLocalToken(), getDefaultSecurityCallback());
    }

    private Token renewTokenInternally(Token token, SecurityCallback callback) throws SecurityFacadeException {
        String url = new StringBuilder(60).append(AUTH_TOKEN).append(token.getTokenID()).append("/renew").toString();
        return execute(callback, url, FacadeUtilities.POST, NO_PARAMETERS_MAP,
                new ConnectionExecutionTask<Token, SecurityFacadeException>() {
                    @Override
                    public Token execute(HttpURLConnection connection) throws IOException, DataBindingException,
                            SecurityFacadeException {
                        if (connection.getResponseCode() == FacadeUtilities.OK) {
                            String answer = readAndSendAnswer(connection.getInputStream());
                            Token renewedToken = new Token(JAXB.unmarshal(new StringReader(answer), TokenInfo.class));
                            setTokenInternal(renewedToken);
                            return renewedToken;
                        } else {
                            readAndSendAnswer(connection.getErrorStream());
                            return null;
                        }
                    }
                });
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#renewToken(se.esss.ics.rbac.access.SecurityCallback)
     */
    @Override
    public void renewToken(SecurityCallback callback) {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a connection to the given URL using the given method and parameters and executes the task using the
     * created connection. If there was an error during execution the error will be wrapped into a
     * {@link SecurityFacadeException}.
     * 
     * @param callback the callback used for generating the connection
     * @param url the URL to connect to
     * @param method the HTTP request method
     * @param parameters the request parameters
     * @param executor the task
     * @return the task execution result
     * @throws SecurityFacadeException if there was an error during execution
     */
    private <T> T execute(SecurityCallback callback, String url, String method, Map<String, String> parameters,
            ConnectionExecutionTask<T, SecurityFacadeException> task) throws SecurityFacadeException {
        HttpURLConnection connection = null;
        try {
            connection = connectionFactory.getConnection(callback, url, method);
            for (Map.Entry<String, String> property : parameters.entrySet()) {
                if (property.getValue() == null) {
                    continue;
                }
                connection.setRequestProperty(property.getKey(), property.getValue());
            }
            return task.execute(connection);
        } catch (Exception e) {
           return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#setAutoLogoutTimeout(int)
     */
    @Override
    public void setAutoLogoutTimeout(int timeoutInMinutes) {
        throw new UnsupportedOperationException();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#setAutoLogout(se.esss.ics.rbac.access.AutoLogout)
     */
    @Override
    public void setAutoLogout(AutoLogout autoLogout) {
        throw new UnsupportedOperationException();
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getAutoLogoutTimeout()
     */
    @Override
    public int getAutoLogoutTimeout() {
        return 15;
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#getDefaultSecurityCallback()
     */
    @Override
    public SecurityCallback getDefaultSecurityCallback() {
        return defaultSecurityCallback;
    }
    
    /*
     * @see
     * se.esss.ics.rbac.access.ISecurityFacade#setDefaultSecurityCallback(se.esss.ics.rbac.access.SecurityCallback
     * )
     */
    @Override
    public void setDefaultSecurityCallback(SecurityCallback callback) {
        this.defaultSecurityCallback = callback;
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#addSecurityFacadeListener(se.esss.ics.rbac.access.
     * SecurityFacadeListener)
     */
    @Override
    public void addSecurityFacadeListener(SecurityFacadeListener listener) {
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#removeSecurityFacadeListener(se.esss.ics.rbac.access.
     * SecurityFacadeListener)
     */
    @Override
    public void removeSecurityFacadeListener(SecurityFacadeListener listener) {
    }

    /**
     * Returns the IP of the local host.
     * 
     * @return the local host IP
     */
    private String getLocalhostIP() {
        if (localhostIP == null) {
            try {
                localhostIP = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
            }
        }
        return localhostIP;
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#destroy()
     */
    @Override
    public void destroy() {
    }

    /*
     * @see se.esss.ics.rbac.access.ISecurityFacade#isDestroyed()
     */
    @Override
    public boolean isDestroyed() {
        return false;
    }
    
    /**
     * Sets the answer listener which receives the raw message obtained from the RESTFul methods.
     * 
     * @param answerListener the answerListener to set
     */
    public void setAnswerListener(XMLAnswerListener answerListener) {
        this.answerListener = answerListener;
    }
    
    private String readAndSendAnswer(InputStream stream) throws IOException {
        StringBuilder b = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream, FacadeUtilities.CHARSET))) {
            while(br.ready()) {
                b.append(br.readLine());
                b.append('\n');
            }
        }
        if (answerListener != null) {
            answerListener.answerReceived(b.toString());
        }
        return b.toString();
    }


    @Override
    public void initLocalServiceUsage() {
        
    }
}
