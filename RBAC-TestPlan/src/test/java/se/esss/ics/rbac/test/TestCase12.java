/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import javax.swing.JOptionPane;

import org.junit.Before;
import org.junit.Test;

import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.test.internal.Base;

/**
 * 
 * <code>TestCase12: Role Picking</code> tests if user can select a role when being authenticated.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase12 extends Base {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        System.setProperty(RBACProperties.KEY_SHOW_ROLE_SELECTOR, "true");
    }

    /**
     * Test that user can select a preferred role or all roles when being authenticated.
     */
    @Test
    public void rbac240and241() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token token = facade.authenticate();
        assertNotNull("Authentication was successful", token);
        assertEquals("There should be only one role selected", 1, token.getRoles().length);
        assertEquals("Selected role should be RBACAdministrator", "RBACAdministrator", token.getRoles()[0]);
        JOptionPane.showMessageDialog(null, "You have been logged in as " + token.getRoles()[0] + ".");
        facade.logout();
        JOptionPane.showMessageDialog(null, "You have been logged out.");
    }

    /**
     * Test that if user does not select a preferred role, all of his roles are chose.
     */
    @Test
    public void rbac245() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        Token token = facade.authenticate();
        assertNotNull("Authentication was successful", token);
        assertTrue("There should be more than one role selected", token.getRoles().length > 1);
        assertTrue("Role RBACAdministrator should be one of the selected roles", Arrays.asList(token.getRoles())
                .contains("RBACAdministrator"));
        JOptionPane.showMessageDialog(null, "You have been logged in as " + Arrays.asList(token.getRoles()) +".");
        
    }
}
