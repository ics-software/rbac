/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.InputEvent;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;

import org.junit.Test;

import se.esss.ics.rbac.access.AutoLogout;
import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.access.swing.SwingSecurityCallback;
import se.esss.ics.rbac.access.test.AbstractSecurityFacadeBase;
import se.esss.ics.rbac.test.internal.Base;
import se.esss.ics.rbac.test.internal.SwingSecurityFacadeAutoLogoutTest;

/**
 * 
 * <code>TestCase8: Token Expiration</code> verifies the token expiration and renewal policies, as well as automatic
 * logout functionality. This test case verifies that auto logout timeout less than 15 minutes is not allowed, using
 * regular approach. However, it is still possible to do that by forcefully setting the values using reflection. This
 * feature is leveraged by the tests in order to be able to executed them in reasonable time.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TestCase8 extends Base {

    private class Warning extends JDialog {
        private static final long serialVersionUID = 1L;
        private JPanel panel;
        private String text = "";
        private final String title = "Do not touch any input commands";

        Warning() {
            super();
            setUndecorated(true);
            setAlwaysOnTop(true);
            setResizable(false);
            setModal(false);
            setSize(680, 150);
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation(dim.width / 2 - 375, 150);
            panel = new JPanel() {
                private static final long serialVersionUID = 1L;
                boolean first = true;
                @Override
                protected void paintComponent(Graphics g) {
                    super.paintComponents(g);
                    g.setFont(g.getFont().deriveFont(40f).deriveFont(Font.BOLD));
                    if (first) {
                        first = false;
                        Warning.this.setSize(g.getFontMetrics(g.getFont()).stringWidth(title)+ 30,150);
                    }
                    g.setColor(Color.RED);
                    g.drawString(title, 20, 60);
                    g.drawString(text, 20, 100);
                };
            };
            setContentPane(panel);
        }

        public void setText(String text) {
            this.text = text;
            repaint();
        }
    }

    /**
     * Set up the auto logout to use reasonable timeouts.
     */
    private static void setUpAutoLogout() throws Exception {
        final ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setAutoLogoutTimeout(15);
        Field field = se.esss.ics.rbac.access.SecurityFacade.class.getDeclaredField("autoLogout");
        field.setAccessible(true);
        Field delegate = se.esss.ics.rbac.access.swing.SecurityFacade.class.getDeclaredField("delegate");
        delegate.setAccessible(true);
        AutoLogout logout = (AutoLogout) field.get(delegate.get(facade));
        Field f = AutoLogout.class.getDeclaredField("logoutTimeout");
        f.setAccessible(true);
        f.set(logout, Long.valueOf(5000));

        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }

            @Override
            public boolean autoLogoutConfirm(Token token, int timeoutInSeconds) {
                synchronized (facade) {
                    facade.notifyAll();
                }
                return super.autoLogoutConfirm(token, timeoutInSeconds);
            }
        });
    }

    /**
     * Test if the token is valid for 8 hours at creation time.
     */
    @Test
    public void rbac500() throws SecurityFacadeException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });
        Token token = facade.authenticate();
        long creationTime = token.getCreationDate().getTime();
        long expirationTime = token.getExpirationDate().getTime();
        long now = System.currentTimeMillis();
        // allow 5 seconds difference between now and token creation time
        assertTrue("Creation time (" + token.getCreationDate() + ") should be approximately now (" + new Date(now)
                + ")", Math.abs(now - creationTime) < 5000);
        assertEquals("Expiration time should be 480 minutes after creation time", 8 * 60,
                (expirationTime - creationTime) / 60000);
    }

    /**
     * Test the minimum boundary for the auto logout time, and if different actions reset the auto logout.
     */
    @Test
    public void rbac510() throws Exception {
        Warning warning = new Warning();
        warning.setVisible(true);
        AbstractSecurityFacadeBase.init();
        // these requirements are covered by junit tests implemented in the SwingSecurityFacadeAutoLogoutTest
        SwingSecurityFacadeAutoLogoutTest test = new SwingSecurityFacadeAutoLogoutTest();
        try {
            test.setUp();
            test.testForInvalidTimeout();
            test.tearDown();

            warning.setText("Testing delayed confirmation...");
            test.setUp();
            test.testAutoLogoutDelayedConfirm();
            test.tearDown();

            warning.setText("Testing delayed deny...");
            test.setUp();
            test.testAutoLogoutDelayedDeny();
            test.tearDown();

            warning.setText("Testing immediate confirmation...");
            test.setUp();
            test.testAutoLogoutImmediateConfirm();
            test.tearDown();

            warning.setText("Testing immediate deny...");
            test.setUp();
            test.testAutoLogoutImmediateDeny();
            test.tearDown();

            warning.setText("Testing with interactions ...");
            test.setUp();
            test.testAutoLogoutWhenUpdating();
            test.tearDown();

            warning.setText("Testing mouse clicks ...");
            test.setUp();
            test.testWithMouseClick();
            test.tearDown();

            warning.setText("Testing mouse movement ...");
            test.setUp();
            test.testWithMouseMovement();
            test.tearDown();

            warning.setText("Testing mouse wheel ...");
            test.setUp();
            test.testWithMouseWheelMove();
            test.tearDown();

            warning.setText("Testing key inputs ...");
            test.setUp();
            test.testWitKeyTyped();
            test.tearDown();
        } finally {
            test.tearDown();
            AbstractSecurityFacadeBase.destroy();
            warning.dispose();
        }
    }

    /**
     * Tests if auto logout confirmation dialog is shown and behaves properly.
     */
    @Test
    public void rbac520() throws Exception {
        Warning warning = new Warning();
        warning.setVisible(true);
        try {
            // Force setup the facade to use auto logout time of 1 minute so the test
            // can be completed in reasonable time (5 seconds).
            ISecurityFacade facade = SecurityFacade.getDefaultInstance();
            setUpAutoLogout();
            Token token = facade.authenticate();
            assertNotNull("Token was received", token);

            warning.setText("Waiting for confirmation dialog...");
            synchronized (facade) {
                facade.wait(11000);
            }
            warning.setText("Simulating mouse move...");

            // test if logout occurs when user confirms it
            simulateMouseMove(true);
            synchronized (this) {
                wait(3000);
            }
            warning.setText("Simulating mouse click...");
            synchronized (this) {
                wait(1000);
            }
            Robot robot = new Robot();
            robot.mousePress(InputEvent.BUTTON1_MASK);
            Thread.sleep(250);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            synchronized (this) {
                wait(3000);
            }
            warning.setText("Verifying logout...");
            Token tokenAfterLogout = facade.getToken();
            assertNull("Automatic logout should happen", tokenAfterLogout);

            // test if logout occurs when user cancels it
            warning.setText("Waiting for confirmation dialog...");
            token = facade.authenticate();
            assertNotNull("Log in successful", token);
            synchronized (facade) {
                facade.wait(11000);
            }
            warning.setText("Simulating mouse move...");
            simulateMouseMove(false);
            synchronized (this) {
                wait(3000);
            }
            warning.setText("Simulating mouse click...");
            synchronized (this) {
                wait(1000);
            }
            robot.mousePress(InputEvent.BUTTON1_MASK);
            Thread.sleep(250);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            synchronized (this) {
                wait(3000);
            }
            warning.setText("Verifying not being logged out...");
            tokenAfterLogout = facade.getToken();
            assertNotNull("Automatic logout should not happen", tokenAfterLogout);

            warning.setText("Waiting for confirmation dialog...");
            // test if logout occurs if user doesn't do anything
            synchronized (facade) {
                facade.wait(10000);
            }
            warning.setText("Waiting for time to expire...");
            synchronized (this) {
                wait(31000);
            }
            tokenAfterLogout = facade.getToken();
            assertNull("Automatic logout should happen", tokenAfterLogout);
        } finally {
            warning.dispose();
        }
    }

    private static void simulateMouseMove(boolean ok) throws Exception {
        Thread.sleep(1000);
        Robot robot = new Robot();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        robot.mouseMove(0, 0);
        double stepX = (dim.width / 2) / 200.;
        double stepY = (dim.height / 2) / 200.;
        for (int i = 0; i < 200; i++) {
            robot.mouseMove((int) (i * stepX), (int) (i * stepY));
            Thread.sleep(10);
        }
        
        Window[] w = Window.getWindows();
        JButton button = null;
        JRootPane root = null;
        Point loc = null;
        for (Window ww : w) {
            if (ww.getComponentCount() == 0) continue;
            root = (JRootPane)ww.getComponent(0);
            button = getButton(root, ok);
            loc = ww.getLocation();
            if (button != null) break;
        }
              
        if (button == null || root == null || loc == null) {
            throw new IllegalStateException("Could not find the Confirmation dialog.");
        }
        loc.x += button.getLocation().x + root.getLocation().x + button.getParent().getLocation().x;
        loc.y += button.getLocation().y + root.getLocation().y + button.getParent().getLocation().y;
                
        int offset = button.getSize().height/2;
        stepX = (loc.x + offset - dim.width / 2) / 50.;
        stepY = (loc.y + offset - dim.height / 2) / 50.;
        
        for (int i = 0; i < 50; i++) {
            robot.mouseMove(dim.width / 2 + (int) (i * stepX), dim.height / 2 + (int) (i * stepY));
            Thread.sleep(10);
        }
        robot.mouseMove(loc.x + offset*2, loc.y + offset);
        
    }
        
    private static JButton getButton(Component parent, boolean okButton) {
        if (parent instanceof JButton) {
            String text = ((JButton)parent).getText();
            String ok = okButton ? UIManager.getString("OptionPane.okButtonText",Locale.getDefault()) :
                UIManager.getString("OptionPane.cancelButtonText",Locale.getDefault());
            if (ok.equals(text)) return (JButton)parent;
        }
        if (parent instanceof Container) {
            Component[] cc = ((Container)parent).getComponents();
            for (Component c : cc) {
                JButton b = getButton(c, okButton);
                if (b != null) return b;
            }
        } 
        return null;
    }

    /**
     * Test if token is renewed when a action is triggered on the service.
     */
    @Test
    public void rbac530() throws SecurityFacadeException, InterruptedException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });

        Token token = facade.authenticate();

        synchronized (this) {
            // wait a while, before making a request
            wait(10000);
        }

        facade.hasPermission("RBACManagementStudio", "ManageRole");

        ISecurityFacade newFacade = new SecurityFacade();
        newFacade.setToken(token.getTokenID());
        Token updatedToken = newFacade.getToken();

        assertArrayEquals("Token IDs should be the same", token.getTokenID(), updatedToken.getTokenID());
        assertEquals("Creation time should be the same", token.getCreationDate(), updatedToken.getCreationDate());
        long difference = (updatedToken.getExpirationDate().getTime() - token.getExpirationDate().getTime()) / 1000;
        assertTrue("Expiration time should be approximately 10 seconds later, but was " + difference, difference >= 10
                && difference < 13);
    }

    /**
     * Test if token renewal on demand is possible.
     */
    @Test
    public void rbac540() throws SecurityFacadeException, InterruptedException {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        facade.setDefaultSecurityCallback(new SwingSecurityCallback() {
            @Override
            public Credentials getCredentials() {
                return new Credentials(username, password);
            }
        });

        Token token = facade.authenticate();

        synchronized (this) {
            // wait a while, before making a request
            wait(10000);
        }

        facade.renewToken();

        Token renewedToken = facade.getToken();

        assertArrayEquals("Token IDs should be the same", token.getTokenID(), renewedToken.getTokenID());
        assertEquals("Creation time should be the same", token.getCreationDate(), renewedToken.getCreationDate());
        long difference = (renewedToken.getExpirationDate().getTime() - token.getExpirationDate().getTime()) / 1000;
        assertTrue("Expiration time should be approximately 10 seconds later, but was " + difference, difference >= 10
                && difference < 13);
    }

}
