/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.test.internal;

import java.lang.reflect.Field;

import org.junit.After;
import org.junit.Before;

import se.esss.ics.rbac.test.UsernameAndPassword;
import se.esss.ics.rbac.access.FacadeUtilities;
import se.esss.ics.rbac.access.RBACProperties;
import se.esss.ics.rbac.access.SecurityFacade;

/**
 * 
 * <code>Base</code> is a base class that provides common setup and cleanup for all the test cases.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public abstract class Base implements UsernameAndPassword {

    @Before
    public void setUp() throws Exception {
        SecurityFacade.getDefaultInstance().logout();
        SecurityFacade.getDefaultInstance().setDefaultSecurityCallback(null);

        Field f = RBACProperties.class.getDeclaredField("instance");
        f.setAccessible(true);
        f.set(null, null);
    }

    @After
    public void tearDown() throws Exception {
        SecurityFacade.getDefaultInstance().logout();
        SecurityFacade.getDefaultInstance().setDefaultSecurityCallback(null);

        Field f = RBACProperties.class.getDeclaredField("instance");
        f.setAccessible(true);
        f.set(null, null);

        f = FacadeUtilities.class.getDeclaredField("publicKey");
        f.setAccessible(true);
        f.set(FacadeUtilities.getInstance(), null);

        System.clearProperty(RBACProperties.KEY_PRIMARY_SERVICES_URL);
        System.clearProperty(RBACProperties.KEY_PRIMARY_SSL_HOST);
        System.clearProperty(RBACProperties.KEY_PRIMARY_SSL_PORT);
        System.clearProperty(RBACProperties.KEY_SECONDARY_SERVICES_URL);
        System.clearProperty(RBACProperties.KEY_SECONDARY_SSL_HOST);
        System.clearProperty(RBACProperties.KEY_SECONDARY_SSL_PORT);
        System.clearProperty(RBACProperties.KEY_CERTIFICATE_STORE);
        System.clearProperty(RBACProperties.KEY_INACTIVITY_RESPONSE_GRACE_PERIOD);
        System.clearProperty(RBACProperties.KEY_INACTIVITY_TIMEOUT_DEFAULT);
        System.clearProperty(RBACProperties.KEY_PUBLICK_KEY_LOCATION);
        System.clearProperty(RBACProperties.KEY_SHOW_ROLE_SELECTOR);
        System.clearProperty(RBACProperties.KEY_VERIFY_SIGNATURE);
    }

}
