/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Arrays;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXB;

import org.junit.Test;
import org.mockito.Mockito;

import se.esss.ics.rbac.access.ConnectionException;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.SecurityFacadeListener;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.jaxb.RolesInfo;
import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * 
 * <code>SecurityFacadeVariousTest</code> tests various methods of {@link SecurityFacade}, which are not covered by
 * other tests.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class SecurityFacadeVariousTest extends AbstractSecurityFacadeBase {

    private static final String ERROR                                = "Error";
    private static final String EXCEPTION_SHOULD_OCCUR               = "Exception should occur";
    private static final String ID123                                = "id123";
    private static final String JOHNDOE                              = "johndoe";
    private static final String MESSAGE_SHOULD_MATCH                 = "Message should match";
    private static final String ROLE1                                = "Role1";
    private static final String ROLE2                                = "Role2";
    private static final String SERVICE_RESPONDED_UNEXPECTEDLY_ERROR = "Service responded unexpectedly: Error";
    private static final String TOKEN_ID                             = "tokenID";
    private static final String TOKEN_ID_SHOULD_MATCH                = "Token ID should match";
    private static final String TOKEN123                             = "token123";

    private RolesInfo rolesInfo = new RolesInfo(JOHNDOE, Arrays.asList(ROLE1, ROLE2));
    private HttpURLConnection rolesConnection;
    private HttpURLConnection versionConnection;
    private HttpURLConnection getTokenConnection;
    private HttpURLConnection renewTokenConnection;
    private HttpURLConnection validateTokenConnection;

    private void setUpForGetVersion() throws Exception {
        versionConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/version"), eq("GET"))).thenReturn(
                versionConnection);
        when(versionConnection.getResponseCode()).thenReturn(200);
        InputStream stream = new ByteArrayInputStream("ver 1.2.3".getBytes(CHARSET));
        when(versionConnection.getInputStream()).thenReturn(stream);
        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(versionConnection.getErrorStream()).thenReturn(stream2);
    }

    private void setUpForGetRoles() throws Exception {
        rolesConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/johndoe/role"), eq("GET"))).thenReturn(
                rolesConnection);
        when(rolesConnection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(rolesInfo, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(rolesConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(rolesConnection.getErrorStream()).thenReturn(stream2);
    }

    private void setUpForGetToken() throws Exception {
        setUpForAuthentication();
        getTokenConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID"), eq("GET"))).thenReturn(
                getTokenConnection);
        when(getTokenConnection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(info, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(getTokenConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(getTokenConnection.getErrorStream()).thenReturn(stream2);

    }

    private void setUpForTokenValidation() throws Exception {
        setUpForAuthentication();
        validateTokenConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/isvalid"), eq("GET")))
                .thenReturn(validateTokenConnection);
        when(validateTokenConnection.getResponseCode()).thenReturn(200);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(new byte[0]);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(validateTokenConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(validateTokenConnection.getErrorStream()).thenReturn(stream2);

    }

    private void setUpForRenewToken() throws Exception {
        setUpForAuthentication();
        renewTokenConnection = mock(HttpURLConnection.class);
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/token/tokenID/renew"), eq("POST")))
                .thenReturn(renewTokenConnection);
        when(renewTokenConnection.getResponseCode()).thenReturn(200);

        TokenInfo info = new TokenInfo(TOKEN_ID, JOHNDOE, "John", "Doe", System.currentTimeMillis(),
                System.currentTimeMillis() + 30000, "192.168.1.2", Arrays.asList(ROLE1, ROLE2), new byte[] { 1, 2,
                        3, 4 });

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JAXB.marshal(info, out);
        InputStream stream = new ByteArrayInputStream(out.toByteArray());
        when(renewTokenConnection.getInputStream()).thenReturn(stream);

        InputStream stream2 = new ByteArrayInputStream(ERROR.getBytes(CHARSET));
        when(renewTokenConnection.getErrorStream()).thenReturn(stream2);

    }

    /**
     * Test if default instance of security facade can be created.
     */
    @Test
    public void testSecurityFacadeConstructor() {
        ISecurityFacade facade = SecurityFacade.getDefaultInstance();
        assertNotNull("Facade should not be null", facade);
        assertEquals("The facade class should be the SecurityFacade", SecurityFacade.class, facade.getClass());
        assertSame("Default instance should always return the same instance", facade,
                SecurityFacade.getDefaultInstance());
        SecurityCallback callback = facade.getDefaultSecurityCallback();
        assertNotNull("Default security callback should not be null", callback);
        facade.setDefaultSecurityCallback(null);
        callback = facade.getDefaultSecurityCallback();
        assertNotNull("Default security callback should not be null", callback);

        ISecurityFacade facade2 = SecurityFacade.newInstance();
        assertEquals("Default and new instance should be of the same type", facade.getClass(), facade2.getClass());

    }

    /**
     * Test {@link SecurityFacade#setToken(char[])} and {@link SecurityFacade#setToken(char[], String)}.
     * 
     * @throws Exception on error
     */
    @Test
    public void testSetToken() throws Exception {
        char[] tokenID = ID123.toCharArray();
        facade.setToken(tokenID);
        Token token = facade.getLocalToken();
        assertArrayEquals("TokenID should match", tokenID, token.getTokenID());
        tokenID[0] = 'a';
        token = facade.getLocalToken();
        assertArrayEquals("TokenID in facade should be immutable", ID123.toCharArray(), token.getTokenID());

        facade.setToken(ID123.toCharArray());
        assertEquals("Logged in should be called", 1, loggedIn);
        assertEquals("Logged out should not be called", 0, loggedOut);
        assertArrayEquals(TOKEN_ID_SHOULD_MATCH, ID123.toCharArray(), loggedInToken.getTokenID());
        assertArrayEquals(TOKEN_ID_SHOULD_MATCH, ID123.toCharArray(), facade.getLocalToken().getTokenID());

        Token previousToken = loggedInToken;
        loggedIn = 0;
        loggedInToken = null;
        loggedOutToken = null;
        loggedOut = 0;

        facade.setToken(TOKEN123.toCharArray());
        assertEquals("Logged in should be called", 1, loggedIn);
        assertEquals("Logged out should be called", 1, loggedOut);
        assertArrayEquals("Logged out token should match", previousToken.getTokenID(), loggedOutToken.getTokenID());
        assertArrayEquals(TOKEN_ID_SHOULD_MATCH, TOKEN123.toCharArray(), loggedInToken.getTokenID());
        assertArrayEquals(TOKEN_ID_SHOULD_MATCH, TOKEN123.toCharArray(), facade.getLocalToken().getTokenID());

        loggedIn = 0;
        loggedInToken = null;
        loggedOutToken = null;
        loggedOut = 0;
        facade.setToken(TOKEN123.toCharArray());

        assertEquals("Logged in should not be called", 0, loggedIn);
        assertEquals("Logged out should not be called", 0, loggedOut);
    }

    /**
     * Test {@link SecurityFacade#getToken()}.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetToken() throws Exception {
        setUpForGetToken();
        facade.setToken(TOKEN_ID.toCharArray());

        Token token = facade.getToken(ip);
        assertEquals("Username should match", info.getUserID(), token.getUsername());
        assertEquals("First name should match", info.getFirstName(), token.getFirstName());
        assertEquals("Last name should match", info.getLastName(), token.getLastName());
        assertEquals("IP should match", info.getIp(), token.getIP());
        assertEquals(TOKEN_ID_SHOULD_MATCH, info.getTokenID(), new String(token.getTokenID()));
        assertEquals("Creation date should match", info.getCreationTime(), token.getCreationDate().getTime());
        assertEquals("Expiration date should match", info.getExpirationTime(), token.getExpirationDate().getTime());
        assertArrayEquals("Role names should match", info.getRoleNames().toArray(new String[2]), token.getRoles());
        assertArrayEquals("Signature should match", info.getSignature(), token.getRBACSignature());

        facade.logout(ip);
        facade.setToken(TOKEN_ID.toCharArray());

        when(getTokenConnection.getResponseCode()).thenReturn(400);
        try {
            facade.getToken(ip);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
        getTokenConnection.getErrorStream().reset();
        when(getTokenConnection.getInputStream()).thenReturn(null);
        try {
            facade.getToken(ip);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
    }

    /**
     * Test {@link SecurityFacade#isTokenValid()}.
     * 
     * @throws Exception on error
     */
    @Test
    public void testIsTokenValid() throws Exception {
        setUpForTokenValidation();
        facade.logout(ip);
        boolean valid = facade.isTokenValid(ip);
        assertFalse("Token should not be valid", valid);

        facade.authenticate();
        valid = facade.isTokenValid(ip);
        assertTrue("Token should be valid", valid);

        when(validateTokenConnection.getResponseCode()).thenReturn(403);
        valid = facade.isTokenValid(ip);
        assertFalse("Token should be valid", valid);

        when(validateTokenConnection.getResponseCode()).thenReturn(400);
        try {
            facade.isTokenValid(ip);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
        validateTokenConnection.getErrorStream().reset();
        when(validateTokenConnection.getInputStream()).thenReturn(null);
        try {
            facade.isTokenValid(ip);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
    }

    /**
     * Test {@link SecurityFacade#renewToken()}.
     * 
     * @throws Exception on error
     */
    @Test
    public void testRenewToken() throws Exception {
        try {
            facade.renewToken(ip);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals("Exception expected", "RBAC authentication token is missing.", e.getMessage());
        }

        setUpForAuthentication();
        Token token = facade.authenticate();
        setUpForRenewToken();

        Token newToken = facade.renewToken(ip);
        assertEquals("Username should match", token.getUsername(), newToken.getUsername());
        assertEquals("First name should match", token.getFirstName(), newToken.getFirstName());
        assertEquals("Last name should match", token.getLastName(), newToken.getLastName());
        assertEquals("IP should match", token.getIP(), newToken.getIP());
        assertTrue("Expiration date should be greater", token.getExpirationDate().getTime() < newToken
                .getExpirationDate().getTime());
        assertArrayEquals("Role names should match", info.getRoleNames().toArray(new String[2]), newToken.getRoles());

        when(renewTokenConnection.getResponseCode()).thenReturn(400);
        try {
            facade.renewToken(ip);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }

        renewTokenConnection.getErrorStream().reset();
        when(renewTokenConnection.getInputStream()).thenReturn(null);
        try {
            facade.renewToken(ip);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
    }

    /**
     * Test the method {@link SecurityFacade#getRolesForUser(String)}.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetRoles() throws Exception {
        setUpForGetRoles();
        String[] roles = facade.getRolesForUser(ip, JOHNDOE);

        assertNotNull("Roles should not be null", roles);
        assertArrayEquals("List of roles should match", new String[] { ROLE1, ROLE2 }, roles);

        when(rolesConnection.getResponseCode()).thenReturn(400);
        try {
            facade.getRolesForUser(ip, JOHNDOE);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
        rolesConnection.getErrorStream().reset();
        when(rolesConnection.getInputStream()).thenReturn(null);
        try {
            facade.getRolesForUser(ip, JOHNDOE);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }

        try {
            facade.getRolesForUser(ip, "");
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, "Parameter 'username' must not be null or empty.", e.getMessage());
        }

        try {
            facade.getRolesForUser(ip, null);
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (IllegalArgumentException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, "Parameter 'username' must not be null or empty.", e.getMessage());
        }
    }

    /**
     * Test the method {@link SecurityFacade#getRBACVersion()}.
     * 
     * @throws Exception on error
     */
    @Test
    public void testGetVersion() throws Exception {
        setUpForGetVersion();
        String version = facade.getRBACVersion();
        assertEquals("Version number should match", "ver 1.2.3", version);

        when(versionConnection.getResponseCode()).thenReturn(400);
        try {
            facade.getRBACVersion();
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
        versionConnection.getErrorStream().reset();
        when(versionConnection.getInputStream()).thenReturn(null);
        try {
            facade.getRBACVersion();
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, SERVICE_RESPONDED_UNEXPECTEDLY_ERROR, e.getMessage());
        }
    }

    /**
     * Test that facade is not messed up when listener throws exception.
     * 
     * @throws Exception on error
     */
    @Test
    public void testListenerException() throws Exception {
        SecurityFacadeListener l1 = mock(SecurityFacadeListener.class);
        Mockito.doThrow(new IllegalArgumentException("Test")).when(l1).loggedIn(any(Token.class));
        SecurityFacadeListener l2 = mock(SecurityFacadeListener.class);
        Mockito.doThrow(new IllegalArgumentException("Test")).when(l2).loggedOut(any(Token.class));
        facade.addSecurityFacadeListener(l1);
        facade.addSecurityFacadeListener(l2);
        setUpForAuthentication();
        Token t = facade.authenticate();
        assertNotNull("Authentication successful", t);

        Mockito.verify(l1, Mockito.only()).loggedIn(any(Token.class));
        Mockito.verify(l2, Mockito.never()).loggedOut(any(Token.class));

        boolean b = facade.logout(ip);
        assertTrue("Logout successful", b);
        Mockito.verify(l2, Mockito.times(1)).loggedOut(any(Token.class));
    }

    /**
     * Test that in the case when a connection to primary service fails an attempt is made to connect to the secondary
     * service.
     * 
     * @throws Exception on error
     */
    @Test
    public void testRedundancy() throws Exception {
        setUpForAuthentication();
        when(loginConnection.getResponseCode()).thenThrow(new IOException(ERROR));
        try {
            facade.authenticate();
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, "Service response error: Error", e.getMessage());
            // ignore
        }

        Mockito.verify(loginConnection, Mockito.times(2)).getResponseCode();
        Mockito.verify(factory, Mockito.times(2)).getConnection(any(SecurityCallback.class), eq("/auth/token/"),
                eq("POST"));
        Mockito.verify(factory, Mockito.times(1)).switchService();

        setUpForAuthentication();
        when(loginConnection.getResponseCode()).thenThrow(new DataBindingException(new IOException(ERROR)));
        try {
            facade.authenticate();
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, "Service response error: Error", e.getMessage());
            // ignore
        }
        Mockito.verify(factory, Mockito.times(3)).getConnection(any(SecurityCallback.class), eq("/auth/token/"),
                eq("POST"));
        Mockito.verify(factory, Mockito.times(1)).switchService();

        setUpForAuthentication();
        when(factory.getConnection(any(SecurityCallback.class), eq("/auth/token/"), eq("POST"))).thenThrow(
                new ConnectionException(ERROR));
        try {
            facade.authenticate();
            fail(EXCEPTION_SHOULD_OCCUR);
        } catch (SecurityFacadeException e) {
            assertEquals(MESSAGE_SHOULD_MATCH, "Error while creating a connection to service. Error", e.getMessage());
            // ignore
        }
        Mockito.verify(factory, Mockito.times(5)).getConnection(any(SecurityCallback.class), eq("/auth/token/"),
                eq("POST"));
        Mockito.verify(factory, Mockito.times(2)).switchService();
    }
}
