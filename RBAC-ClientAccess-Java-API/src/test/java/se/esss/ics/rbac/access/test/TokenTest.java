/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.jaxb.TokenInfo;

/**
 * 
 * <code>TokenTest</code> tests the {@link Token} construction and immutability.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public class TokenTest {

    /**
     * Test if the token returns proper values.
     * 
     * @throws Exception on error
     */
    @Test
    public void testTokenCreation() throws Exception {
        Constructor<Token> c = Token.class.getDeclaredConstructor(char[].class, String.class, String.class,
                String.class, String[].class, Date.class, Date.class, String.class, byte[].class);
        c.setAccessible(true);

        long time = System.currentTimeMillis();
        Token t = c.newInstance("tokenID".toCharArray(), "johndoe", "John", "Doe", new String[] { "Role 1", "Role 2" },
                new Date(time), new Date(time + 10000), "192.168.1.1", new byte[] { 1, 2, 3, 4 });

        assertArrayEquals("Token ID should match", "tokenID".toCharArray(), t.getTokenID());
        assertEquals("Username should match", "johndoe", t.getUsername());
        assertEquals("First name should match", "John", t.getFirstName());
        assertEquals("Last name should match", "Doe", t.getLastName());
        assertArrayEquals("Roles should match", new String[] { "Role 1", "Role 2" }, t.getRoles());
        assertEquals("Creation date should match", new Date(time), t.getCreationDate());
        assertEquals("Expiration date should match", new Date(time + 10000), t.getExpirationDate());
        assertEquals("IP should match", "192.168.1.1", t.getIP());
        assertArrayEquals("Signature should match", new byte[] { 1, 2, 3, 4 }, t.getRBACSignature());
    }

    /**
     * Tests if token is properly generated from the TokenInfo.
     * 
     * @throws Exception on error
     */
    @Test
    public void testTokenCreationFromInfo() throws Exception {

        Constructor<Token> c = Token.class.getDeclaredConstructor(TokenInfo.class);
        c.setAccessible(true);

        long time = System.currentTimeMillis();
        TokenInfo info = new TokenInfo("tokenID", "johndoe", "John", "Doe", time, time + 10000, "192.168.1.1",
                Arrays.asList(new String[] { "Role 1", "Role 2" }), new byte[] { 1, 2, 3, 4 });
        Token t = c.newInstance(info);

        assertArrayEquals("Token ID should match", "tokenID".toCharArray(), t.getTokenID());
        assertEquals("Username should match", "johndoe", t.getUsername());
        assertEquals("First name should match", "John", t.getFirstName());
        assertEquals("Last name should match", "Doe", t.getLastName());
        assertArrayEquals("Roles should match", new String[] { "Role 1", "Role 2" }, t.getRoles());
        assertEquals("Creation date should match", new Date(time), t.getCreationDate());
        assertEquals("Expiration date should match", new Date(time + 10000), t.getExpirationDate());
        assertEquals("IP should match", "192.168.1.1", t.getIP());
        assertArrayEquals("Signature should match", new byte[] { 1, 2, 3, 4 }, t.getRBACSignature());
    }

    /**
     * Test token construction with null values.
     * 
     * @throws Exception on error
     */
    @Test
    public void testNullValues() throws Exception {
        Constructor<Token> c = Token.class.getDeclaredConstructor(char[].class, String.class, String.class,
                String.class, String[].class, Date.class, Date.class, String.class, byte[].class);
        c.setAccessible(true);

        Token t = c.newInstance("tokenID".toCharArray(), null, null, null, null, null, null, null, null);

        assertArrayEquals("Token ID should match", "tokenID".toCharArray(), t.getTokenID());
        assertNull("Username should be null", t.getUsername());
        assertNull("First name should be null", t.getFirstName());
        assertNull("Last name should be null", t.getLastName());
        assertArrayEquals("Roles should be empty", new String[0], t.getRoles());
        assertNull("Creation date should be null", t.getCreationDate());
        assertNull("Expiration date should be null", t.getExpirationDate());
        assertNull("IP should be null", t.getIP());
        assertArrayEquals("Signature should be empty", new byte[0], t.getRBACSignature());

        try {
            c.newInstance(null, null, null, null, null, null, null, null, null);
            fail("Exception should occur, token ID cannot be null");
        } catch (InvocationTargetException e) {
            assertEquals("NullPointerException should occur, if token id is null", NullPointerException.class, e
                    .getCause().getClass());
        }
    }

    /**
     * Test if the token is immutable.
     * 
     * @throws Exception on error
     */
    @Test
    public void testImmutability() throws Exception {
        Constructor<Token> c = Token.class.getDeclaredConstructor(char[].class, String.class, String.class,
                String.class, String[].class, Date.class, Date.class, String.class, byte[].class);
        c.setAccessible(true);

        byte[] signature = new byte[] { 1, 2, 3, 4 };
        String[] roles = new String[] { "Role 1", "Role 2" };
        long time = System.currentTimeMillis();
        Date creationDate = new Date(time);
        Date expirationDate = new Date(time + 10000);
        char[] tokenID = "tokenID".toCharArray();
        Token t = c.newInstance(tokenID, "johndoe", "John", "Doe", roles, creationDate, expirationDate, "192.168.1.1",
                signature);

        signature[0] = 0;
        assertArrayEquals("Signature should be immutable", new byte[] { 1, 2, 3, 4 }, t.getRBACSignature());
        t.getRBACSignature()[0] = 0;
        assertArrayEquals("Signature should be immutable", new byte[] { 1, 2, 3, 4 }, t.getRBACSignature());

        roles[0] = "foo";
        assertArrayEquals("Roles should be immutable", new String[] { "Role 1", "Role 2" }, t.getRoles());
        t.getRoles()[0] = "foo";
        assertArrayEquals("Roles should be immutable", new String[] { "Role 1", "Role 2" }, t.getRoles());

        creationDate.setTime(System.currentTimeMillis() - 50000);
        assertEquals("Creation date should be immutable", new Date(time), t.getCreationDate());
        t.getCreationDate().setTime(System.currentTimeMillis() - 50000);
        assertEquals("Creation date should be immutable", new Date(time), t.getCreationDate());

        expirationDate.setTime(System.currentTimeMillis() - 50000);
        assertEquals("Expiration date should be immutable", new Date(time + 10000), t.getExpirationDate());
        t.getExpirationDate().setTime(System.currentTimeMillis() - 50000);
        assertEquals("Expiration date should be immutable", new Date(time + 10000), t.getExpirationDate());

        tokenID[0] = 'a';
        assertArrayEquals("Token ID should be immutable", "tokenID".toCharArray(), t.getTokenID());
        t.getTokenID()[0] = 'a';
        assertArrayEquals("Token ID should be immutable", "tokenID".toCharArray(), t.getTokenID());

    }

    /**
     * Test that equals and hash code methods are correct. Tokens are equal only if all its parameters are equal.
     * Hash code should always be different if one of the parameters is changed.
     * 
     * @throws Exception on error
     */
    @Test
    public void testTokenEquality() throws Exception {
        Constructor<Token> c = Token.class.getDeclaredConstructor(char[].class, String.class, String.class,
                String.class, String[].class, Date.class, Date.class, String.class, byte[].class);
        c.setAccessible(true);

        long time = System.currentTimeMillis();
        Token t1 = c.newInstance("tokenID".toCharArray(), "johndoe", "John", "Doe",
                new String[] { "Role 1", "Role 2" }, new Date(time), new Date(time + 10000), "192.168.1.1", new byte[] {
                        1, 2, 3, 4 });

        Token t2 = c.newInstance("tokenID".toCharArray(), "johndoe", "John", "Doe",
                new String[] { "Role 1", "Role 2" }, new Date(time), new Date(time + 10000), "192.168.1.1", new byte[] {
                        1, 2, 3, 4 });

        assertEquals("Tokens are equal", t1, t2);
        assertEquals("Hashcodes are identical", t1.hashCode(), t2.hashCode());

        Token t3 = c.newInstance("tokenID1".toCharArray(), "johndoe", "John", "Doe",
                new String[] { "Role 1", "Role 2" }, new Date(time), new Date(time + 10000), "192.168.1.1", new byte[] {
                        1, 2, 3, 4 });

        assertNotEquals("Tokens should be different", t2, t3);
        assertNotEquals("Hashcodes should be different", t2, t3);

        t3 = c.newInstance("tokenID".toCharArray(), "johndoe1", "John", "Doe", new String[] { "Role 1", "Role 2" },
                new Date(time), new Date(time + 10000), "192.168.1.1", new byte[] { 1, 2, 3, 4 });

        assertNotEquals("Tokens should be different", t2, t3);
        assertNotEquals("Hashcodes should be different", t2, t3);

        t3 = c.newInstance("tokenID".toCharArray(), "johndoe", "John1", "Doe", new String[] { "Role 1", "Role 2" },
                new Date(time), new Date(time + 10000), "192.168.1.1", new byte[] { 1, 2, 3, 4 });

        assertNotEquals("Tokens should be different", t2, t3);
        assertNotEquals("Hashcodes should be different", t2, t3);

        t3 = c.newInstance("tokenID".toCharArray(), "johndoe", "John", "Doe1", new String[] { "Role 1", "Role 2" },
                new Date(time), new Date(time + 10000), "192.168.1.1", new byte[] { 1, 2, 3, 4 });

        assertNotEquals("Tokens should be different", t2, t3);
        assertNotEquals("Hashcodes should be different", t2, t3);

        t3 = c.newInstance("tokenID".toCharArray(), "johndoe", "John", "Doe", new String[] { "Role 1", "Role 3" },
                new Date(time), new Date(time + 10000), "192.168.1.1", new byte[] { 1, 2, 3, 4 });

        assertNotEquals("Tokens should be different", t2, t3);
        assertNotEquals("Hashcodes should be different", t2, t3);

        t3 = c.newInstance("tokenID".toCharArray(), "johndoe", "John", "Doe", new String[] { "Role 1", "Role 2" },
                new Date(time + 1), new Date(time + 10000), "192.168.1.1", new byte[] { 1, 2, 3, 4 });

        assertNotEquals("Tokens should be different", t2, t3);
        assertNotEquals("Hashcodes should be different", t2, t3);

        t3 = c.newInstance("tokenID".toCharArray(), "johndoe", "John", "Doe", new String[] { "Role 1", "Role 2" },
                new Date(time), new Date(time + 10001), "192.168.1.1", new byte[] { 1, 2, 3, 4 });

        assertNotEquals("Tokens should be different", t2, t3);
        assertNotEquals("Hashcodes should be different", t2, t3);

        t3 = c.newInstance("tokenID".toCharArray(), "johndoe", "John", "Doe", new String[] { "Role 1", "Role 2" },
                new Date(time), new Date(time + 10000), "192.168.1.2", new byte[] { 1, 2, 3, 4 });

        assertNotEquals("Tokens should be different", t2, t3);
        assertNotEquals("Hashcodes should be different", t2, t3);

        t3 = c.newInstance("tokenID".toCharArray(), "johndoe", "John", "Doe", new String[] { "Role 1", "Role 2" },
                new Date(time), new Date(time + 10000), "192.168.1.1", new byte[] { 1, 2, 3, 5 });

        assertNotEquals("Tokens should be different", t2, t3);
        assertNotEquals("Hashcodes should be different", t2, t3);

        assertFalse("Token is always different from null", t2.equals(null));
        assertFalse("Token is always different from non token object", t2.equals(new Object()));
    }
}
