/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

/**
 * This exception is thrown if there is an error while using {@link SecurityFacade}.
 * 
 * @author <a href="mailto:jakob.battelino@cosylab.com">Jakob Battelino Prelog</a>
 */
public class SecurityFacadeException extends Exception {

    private static final long serialVersionUID = 3891534624198941582L;

    private final int responseCode;

    /**
     * Constructs a new exception with the specified detail message. The cause is not initialised, and may subsequently
     * be initialised by a call to {@link #initCause(Throwable)}.
     * 
     * @param message the detailed message of the exception
     * @param responseCode one of the server response codes 
     * 
     * @see FacadeUtilities#ERROR and similar
     */
    public SecurityFacadeException(String message, int responseCode) {
        super(message);
        this.responseCode = responseCode;
    }

    /**
     * Constructs a new exception with the specified detail message and cause. Note that the detail message associated
     * with cause is not automatically incorporated in this exception's detail message.
     * 
     * @param message detailed message of the exception
     * @param cause the cause of the exception
     * @param responseCode one of the server response codes 
     * 
     * @see FacadeUtilities#ERROR and similar
     */
    public SecurityFacadeException(String message, Throwable cause, int responseCode) {
        super(message, cause);
        this.responseCode = responseCode;
    }
    
    /**
     * @return the HTTP response code of the response that triggered this exception or 
     *          {@link FacadeUtilities#IRRELEVANT} for irrelevant
     */
    public int getResponseCode() {
        return responseCode;
    }
}
