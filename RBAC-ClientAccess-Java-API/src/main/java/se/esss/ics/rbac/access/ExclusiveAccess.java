/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.access;

import java.util.Date;
import java.util.Objects;

import se.esss.ics.rbac.jaxb.ExclusiveInfo;

/**
 * 
 * <code>ExclusiveAccess</code> is a wrapper for the data received when a user request exclusive access. It provides the
 * information, which permission was requested and the expiration date, which is when the exclusive access will be
 * automatically released.
 * 
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 * 
 */
public final class ExclusiveAccess {

    private String resource;
    private String permission;
    private long expirationDate;

    /**
     * Constructs a new exclusive access from the service transfer object.
     * 
     * @param info the info received from the service
     */
    public ExclusiveAccess(ExclusiveInfo info) {
        this(info.getResourceName(), info.getPermissionName(), info.getExpirationTime());
    }

    /**
     * Construct a new exclusive access.
     * 
     * @param resource the name of the resource that owns the permission
     * @param permission the name of the permission for which exclusive access was requested
     * @param expirationDate the expiration date as UTC
     */
    public ExclusiveAccess(String resource, String permission, long expirationDate) {
        this.resource = resource;
        this.permission = permission;
        this.expirationDate = expirationDate;
    }

    /**
     * Returns the name of the resource that owns the permission.
     * 
     * @return the resource name
     */
    public String getResource() {
        return resource;
    }

    /**
     * Returns the name of the permission, for which exclusive access was requested.
     * 
     * @return the permission name
     */
    public String getPermission() {
        return permission;
    }

    /**
     * The date when exclusive access expires. After this date, the user no longer has exclusive access.
     * 
     * @return the exclusive access expiration date
     */
    public Date getExpirationDate() {
        return new Date(expirationDate);
    }

    /*
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(resource, permission, expirationDate);
    }

    /*
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } 
        if (obj == null) {
            return false;
        } 
        if (getClass() != obj.getClass()) {
            return false;
        }
        ExclusiveAccess other = (ExclusiveAccess) obj;
        return Objects.equals(permission, other.permission) && Objects.equals(resource, other.resource)
                && Objects.equals(expirationDate, other.expirationDate);
    }

    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new StringBuilder(100).append("Exclusive Access: ").append(permission).append(" [").append(resource)
                .append("], expires: ").append(expirationDate).toString();
    }

}
